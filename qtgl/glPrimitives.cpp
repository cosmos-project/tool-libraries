#include "glPrimitives.h"

#include <iostream>
using namespace std;

const double pi = atan(1)*4;





Vertex::Vertex(){
}

Vertex::Vertex(float value_x, float value_y, float value_z){
    x = value_x;
    y = value_y;
    z = value_z;
}



Vector::Vector(){
    x = 0.;
    y = 0.;
    z = 0.;
}

Vector::Vector(float value_x, float value_y, float value_z){
    x = value_x;
    y = value_y;
    z = value_z;
}

void Vector::normalize(){
    // changes the source vector in place
    float len = length();
    x = x/len;
    y = y/len;
    z = z/len;
}

Vector Vector::normalized(){
    // returns normalized vector
    Vector vec;
    float len = length();
    vec.x = x/len;
    vec.y = y/len;
    vec.z = z/len;
    return vec;
}

float Vector::length(){
    // or norm
    return sqrt(x*x + y*y + z*z);
}

void Vector::reNormalize(float scale){
    // in place
    float len = length();
    x = x/len * scale;
    y = y/len * scale;
    z = z/len * scale;
}

//void Vector::reNormalizeOffset(float scale, Vertex offset){
//    v.x = v.x/norm(v) * scale + offset.x;
//    v.y = v.y/norm(v) * scale + offset.y;
//    v.z = v.z/norm(v) * scale + offset.z;
//}

//double OpenGLPrimitive::norm(Vertex v){
//    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
//}

//double OpenGLPrimitive::norm(Vector v){
//    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
//}


// pass Vertex by refence because we just want to change it
//void OpenGLPrimitive::normalize(Vertex& v){
//    v.x = v.x/norm(v);
//    v.y = v.y/norm(v);
//    v.z = v.z/norm(v);
////    return v;
//}

//// by reference
//void OpenGLPrimitive::normalize(Vector& v){
//    v.x = v.x/norm(v);
//    v.y = v.y/norm(v);
//    v.z = v.z/norm(v);
////    return v;
//}


















OpenGLPrimitive::OpenGLPrimitive()
{
}

void OpenGLPrimitive::setupVbo(){ //OpenGLPrimitive primitive
    // setup vertex vbo
    if (vertices.size() > 0){
        vbo.vertex.create();
        vbo.vertex.setUsagePattern(QOpenGLBuffer::StaticDraw);
        vbo.vertex.bind();
        vbo.vertex.allocate(&vertices[0], vertices.size()*sizeof(vertices[0]));
    } else {
        cout << "Error setting up VBO: no vertices data" << endl;
    }

    // setup color vbo
    if (colors.size() > 0){
        vbo.color.create();
        vbo.color.setUsagePattern(QOpenGLBuffer::StaticDraw);
        vbo.color.bind();
        vbo.color.allocate(&colors[0], colors.size()*sizeof(colors[0]));
    } else {
        cout << "Error setting up VBO: no color data" << endl;
    }
}






Cube::Cube(float size){
    setSize(size);
    setDefaultColors();
}

//Cube::Cube(float size, Color color){
//    setSize(size);
//    setColor(color);
//}


void Cube::setSize(float size){
    // cube
    // replace GLuint to uint32_t
    // 6 faces x 2 triangles per face x 3 vertices per triangle = 36 vertices * 3 coordinates = 108 entries
    // vertex_data[36][3]
    const GLfloat vertex_data[108] =  {
        // left face
        -size,-size,-size,
        -size,-size, size,
        -size, size, size,
        -size,-size,-size,
        -size, size, size,
        -size, size,-size,

        // back face
        size, size,-size,
        -size,-size,-size,
        -size, size,-size,
        size, size,-size,
        size,-size,-size,
        -size,-size,-size,

        // bottom face
        size,-size, size,
        -size,-size,-size,
        size,-size,-size,
        size,-size, size,
        -size,-size, size,
        -size,-size,-size,

        // front face
        -size, size, size,
        -size,-size, size,
        size,-size, size,
        size, size, size,
        -size, size, size,
        size,-size, size,

        // right face
        size, size, size,
        size,-size,-size,
        size, size,-size,
        size,-size,-size,
        size, size, size,
        size,-size, size,

        // top face
        size, size, size,
        size, size,-size,
        -size, size,-size,
        size, size, size,
        -size, size,-size,
        -size, size, size,

    };

    for (int i=0; i < 36; i++){
        vertices.push_back({vertex_data[3*i],
                            vertex_data[3*i+1],
                            vertex_data[3*i+2]});
    }
}


void Cube::setColor(Color color){
    for (int i=0; i < 36; i++){
        colors.push_back({color.r, color.g, color.b});
    }
}

void Cube::setDefaultColors(){
    // One color for each vertex. They were generated randomly.
    //static const GLfloat
    const GLfloat color_data[108] = {
        0.583f,  0.771f,  0.014f,
        0.609f,  0.115f,  0.436f,
        0.327f,  0.483f,  0.844f,
        0.822f,  0.569f,  0.201f,
        0.435f,  0.602f,  0.223f,
        0.310f,  0.747f,  0.185f,
        0.597f,  0.770f,  0.761f,
        0.559f,  0.436f,  0.730f,
        0.359f,  0.583f,  0.152f,
        0.483f,  0.596f,  0.789f,
        0.559f,  0.861f,  0.639f,
        0.195f,  0.548f,  0.859f,
        0.014f,  0.184f,  0.576f,
        0.771f,  0.328f,  0.970f,
        0.406f,  0.615f,  0.116f,
        0.676f,  0.977f,  0.133f,
        0.971f,  0.572f,  0.833f,
        0.140f,  0.616f,  0.489f,
        0.997f,  0.513f,  0.064f,
        0.945f,  0.719f,  0.592f,
        0.543f,  0.021f,  0.978f,
        0.279f,  0.317f,  0.505f,
        0.167f,  0.620f,  0.077f,
        0.347f,  0.857f,  0.137f,
        0.055f,  0.953f,  0.042f,
        0.714f,  0.505f,  0.345f,
        0.783f,  0.290f,  0.734f,
        0.722f,  0.645f,  0.174f,
        0.302f,  0.455f,  0.848f,
        0.225f,  0.587f,  0.040f,
        0.517f,  0.713f,  0.338f,
        0.053f,  0.959f,  0.120f,
        0.393f,  0.621f,  0.362f,
        0.673f,  0.211f,  0.457f,
        0.820f,  0.883f,  0.371f,
        0.982f,  0.099f,  0.879f
    };

    for (int i=0; i < 36; i++){
        colors.push_back({color_data[3*i],
                          color_data[3*i+1],
                          color_data[3*i+2]});
    }
}

//int Cube::numVertices(){
//    return sizeof(vertex_data)/sizeof(vertex_data[0]);
//}







Cylinder::Cylinder()
{
}

Cylinder::Cylinder(Vertex base_center,
                   Vector direction,
                   float r, float height, glm::vec3 color){

    // get perpendicular vector from cylinder direction (reference for triagles)
    //Vertex direction_normal = {-direction.y, direction.x, 0 };
    // using cross product is more general
    glm::vec3 crossResult = glm::cross(glm::vec3(direction.x, direction.y, direction.z),
                                       glm::vec3(direction.x+1.0f, direction.y+2.0f, direction.z+3.0f));
    Vector direction_normal = {crossResult.x, crossResult.y, crossResult.z};

    //    Vector direction_normal_normalized;
    //    direction_normal_normalized = direction_normal;
    //    normalize(direction_normal_normalized);


    // <<
    //    direction_normal.x = direction_normal_normalized.x*r;
    //    direction_normal.y = direction_normal_normalized.y*r;
    //    direction_normal.z = direction_normal_normalized.z*r;

    direction_normal.reNormalize(r);

    //    float test = direction_normal.length();

    // base vertices for cylinder
    Vertex a, b, c, d;

    float alpha = 0.0;
    float nSections = 30;
    float dalpha = 360.0f/nSections*pi/180.0f; // in rad

    // start drawing the cylinder on the direction_normal (with z=0)
    a.x = base_center.x + direction_normal.x;
    a.y = base_center.y + direction_normal.y;
    a.z = base_center.z + direction_normal.z;

    glm::mat4 rot;

    for (int i = 0; i<=nSections; i++){
        alpha = i*dalpha;

        rot = glm::rotate(glm::mat4(1.0f), (float)alpha,
                          glm::vec3(direction.x,
                                    direction.y,
                                    direction.z));

        glm::vec4 bb = rot*glm::vec4(direction_normal.x,
                                     direction_normal.y,
                                     direction_normal.z,
                                     0);

        b.x = base_center.x + bb.x;
        b.y = base_center.y + bb.y;
        b.z = base_center.z + bb.z;

        // top of cylinder
        //        normalize(direction);

        //        Vector direction_normalized;
        //        direction_normalized = direction;
        //        normalize(direction_normalized);


        //        c.x = b.x + normalize(direction).x*height;
        //        c.y = b.y + normalize(direction).y*height;
        //        c.z = b.z + normalize(direction).z*height;
        //        c.x = b.x + direction_normalized.x*height;
        //        c.y = b.y + direction_normalized.y*height;
        //        c.z = b.z + direction_normalized.z*height;
        c.x = b.x + direction.normalized().x*height;
        c.y = b.y + direction.normalized().y*height;
        c.z = b.z + direction.normalized().z*height;

        //        d.x = a.x + normalize(direction).x*height;
        //        d.y = a.y + normalize(direction).y*height;
        //        d.z = a.z + normalize(direction).z*height;
        //        d.x = a.x + direction_normalized.x*height;
        //        d.y = a.y + direction_normalized.y*height;
        //        d.z = a.z + direction_normalized.z*height;
        d.x = a.x + direction.normalized().x*height;
        d.y = a.y + direction.normalized().y*height;
        d.z = a.z + direction.normalized().z*height;

        vertices.push_back({a.x, a.y, a.z});
        vertices.push_back({b.x, b.y, b.z});
        vertices.push_back({c.x, c.y, c.z});

        vertices.push_back({c.x, c.y, c.z});
        vertices.push_back({d.x, d.y, d.z});
        vertices.push_back({a.x, a.y, a.z});


        colors.push_back(color);
        colors.push_back(color);
        colors.push_back(color);

        colors.push_back(color);
        colors.push_back(color);
        colors.push_back(color);

        // vertex point for next triangle is the same as b for the previous
        a.x = b.x;
        a.y = b.y;
        a.z = b.z;
    }
}

Cylinder::Cylinder(float r, float height, glm::vec3 color)
{
    setup(r, height, color);
}



void Cylinder::setup(float r, float height, glm::vec3 color){
    type = "triangles";

    //float height = 2.0f;
    //float r = 0.02f;
    float alpha = 0.0;
    float dalpha = 30.0f*pi/180.0f; // in rad

    // panel centered with z axis
    //    vertices.push_back({-dx/2.0f, 0.0f, radius/2.0f});
    //    vertices.push_back({ dx/2.0f, 0.0f, radius/2.0f});
    //    vertices.push_back({ dx/2.0f, height, radius/2.0f});

    //    vertices.push_back({-dx/2.0f, 0.0f, radius/2.0f});
    //    vertices.push_back({ dx/2.0f, height, radius/2.0f});
    //    vertices.push_back({-dx/2.0f, height, radius/2.0f});

    Vertex base_a, base_b;

    // panel #1 around X axis
    base_b.x = 0.0f;
    base_b.y = 0.0f;
    base_b.z = r;

    //base_b.x = r*sin(alpha + dalpha);
    //base_b.y = 0.0f;
    //base_b.z = r*cos(alpha + dalpha);

    for (int i = 0; i<36+1; i++){
        alpha = i*dalpha;

        // compute nex base
        base_a.x = base_b.x;
        base_a.y = base_b.y;
        base_a.z = base_b.z;

        // interesting wave :)
        //        base_b.x = base_b.x + (base_b.x/r)*cos(dalpha);
        //        base_b.y = 0.0f;
        //        base_b.z = base_b.z - (base_b.z/r)*sin(dalpha);
        base_b.x = 0.0f;
        base_b.y = r*sin(alpha + dalpha);
        base_b.z = r*cos(alpha + dalpha);

        // draw differential panel
        setupPanel(base_a, base_b, height);

        for (int i = 0; i<6; i++){
            // fil the 6 vertices with same color
            colors.push_back(color);
            //colors.push_back({ 1,1,0});
        }
    }

    //    colors.push_back({ 1,0,0});
    //    colors.push_back({ 1,0,0});
    //    colors.push_back({ 1,0,0});

    //    colors.push_back({ 0,1,0});
    //    colors.push_back({ 0,1,0});
    //    colors.push_back({ 0,1,0});
}

void Cylinder::setupPanel(Vertex base_a, Vertex base_b, float height){
    // panel setup around X
    vertices.push_back({base_a.x, base_a.y, base_a.z});
    vertices.push_back({base_b.x, base_b.y, base_b.z});
    vertices.push_back({base_b.x + height, base_b.y, base_b.z});

    vertices.push_back({base_b.x + height, base_b.y , base_b.z});
    vertices.push_back({base_a.x + height, base_a.y , base_a.z});
    vertices.push_back({base_a.x, base_a.y, base_a.z});
}



Cone::Cone(){

}

Cone::Cone(Vertex base_center, Vector direction, float r, float height, glm::vec3 color)
{

    //    base_center = {0,0,1};
    //    direction = {0,-1,0};

    // get perpendicular vector from cone direction (reference for triagles)
    //Vertex direction_normal = {-direction.y, direction.x, 0 };
    glm::vec3 crossResult = glm::cross(glm::vec3(direction.x, direction.y, direction.z),
                                       glm::vec3(direction.x+1.0f, direction.y+2.0f, direction.z+3.0f));
    Vector direction_normal = {crossResult.x, crossResult.y, crossResult.z};



    //    direction_normal.x = normalize(direction_normal).x*r;
    //    direction_normal.y = normalize(direction_normal).y*r;
    //    direction_normal.z = normalize(direction_normal).z*r;
    //    direction_normal.x = direction_normal.x*r;
    //    direction_normal.y = direction_normal.y*r;
    //    direction_normal.z = direction_normal.z*r;

    direction_normal.reNormalize(r);

    //    Vector test = direction_normal.normalized();
    //    float test1 = direction_normal.normalized().x;


    // base for cone
    Vertex a, b, c;

    float alpha = 0.0;
    float nSections = 30;
    float dalpha = 360.0f/nSections*pi/180.0f; // in rad

    // start drawing the cone on the direction_normal (with z=0)
    a.x = base_center.x + direction_normal.x;
    a.y = base_center.y + direction_normal.y;
    a.z = base_center.z + direction_normal.z;

    glm::mat4 rot;

    for (int i = 0; i<=nSections; i++){
        alpha = i*dalpha;

        rot = glm::rotate(glm::mat4(1.0f), (float)alpha,
                          glm::vec3(direction.x,
                                    direction.y,
                                    direction.z));

        glm::vec4 bb = rot*glm::vec4(direction_normal.x,
                                     direction_normal.y,
                                     direction_normal.z,
                                     0);

        b.x = base_center.x + bb.x;
        b.y = base_center.y + bb.y;
        b.z = base_center.z + bb.z;

        // center of cone
        //        Vector direction_normalized;
        //        direction_normalized = direction;
        //        normalize(direction_normalized);

        //        c.x = base_center.x + normalize(direction).x*height;
        //        c.y = base_center.y + normalize(direction).y*height;
        //        c.z = base_center.z + normalize(direction).z*height;
        //        c.x = base_center.x + direction_normalized.x*height;
        //        c.y = base_center.y + direction_normalized.y*height;
        //        c.z = base_center.z + direction_normalized.z*height;

        c.x = base_center.x + direction.normalized().x*height;
        c.y = base_center.y + direction.normalized().y*height;
        c.z = base_center.z + direction.normalized().z*height;

        vertices.push_back({a.x, a.y, a.z});
        vertices.push_back({b.x, b.y, b.z});
        vertices.push_back({c.x, c.y, c.z});

        colors.push_back(color);
        colors.push_back(color);
        colors.push_back(color);

        // vertex point for next triangle is the same as b for the previous
        a.x = b.x;
        a.y = b.y;
        a.z = b.z;
    }

}

Cone::Cone(float r, float height, float cone_height)
{
    float alpha = 0.0;
    float dalpha = 30.0f*pi/180.0f; // in rad

    // base for cone
    Vertex edge_a, edge_b;
    edge_a.x = height-cone_height;
    edge_a.y = 0;
    edge_a.z = r;
    for (int i = 0; i<36+1; i++){
        alpha = i*dalpha;

        edge_b.x = edge_a.x;
        edge_b.y = r*sin(alpha + dalpha);
        edge_b.z = r*cos(alpha + dalpha);

        vertices.push_back({edge_a.x,0,0});
        vertices.push_back({edge_a.x,edge_a.y,edge_a.z});
        vertices.push_back({edge_b.x,edge_b.y,edge_b.z});

        colors.push_back({1,0,0});
        colors.push_back({1,0,0});
        colors.push_back({1,0,0});
        // next base
        edge_a.x = edge_b.x;
        edge_a.y = edge_b.y;
        edge_a.z = edge_b.z;
    }

    // cone
    edge_a.x = height;
    edge_a.y = 0;
    edge_a.z = r;
    for (int i = 0; i<36+1; i++){
        alpha = i*dalpha;

        edge_b.x = edge_a.x;
        edge_b.y = r*sin(alpha + dalpha);
        edge_b.z = r*cos(alpha + dalpha);

        vertices.push_back({edge_a.x,0,0});
        vertices.push_back({edge_a.x-cone_height,edge_a.y,edge_a.z});
        vertices.push_back({edge_b.x-cone_height,edge_b.y,edge_b.z});

        colors.push_back({0,1,0});
        colors.push_back({0,1,0});
        colors.push_back({0,1,0});

        // next base
        edge_a.x = edge_b.x;
        edge_a.y = edge_b.y;
        edge_a.z = edge_b.z;
    }
}


glVector::glVector()
{
    // set properties to zero
    vector.x = 0;
    vector.y = 0;
    vector.z = 0;

    position.x = 0;
    position.y = 0;
    position.z = 0;

    lenght = 0;

    //    // add cylinder
    //    float height = 1;
    //    float r = 0.03;
    //    float ratio = 0.2;
    //    glm::vec3 cylinder_color = {1,0,0};
    //    Vertex cylinder_base_center = {0,0,0};
    //    Vertex cylinder_direction = {1,0,0};

    //    //Cylinder cylinder(r, height*(1-ratio), cylinder_color);
    //    Cylinder cylinder(cylinder_base_center, cylinder_direction, r, height*(1-ratio), cylinder_color);
    //    vertices = cylinder.vertices;
    //    colors = cylinder.colors;

    //    // add the cone
    //    Vertex cone_base_center = {height*(1-ratio),0,0};
    //    Vertex cone_direction = {1,0,0};
    //    glm::vec3 cone_color = {1,1,0};
    //    float cone_radius =  r*2.2;
    //    float cone_height = height*(ratio);

    //    Cone cone( cone_base_center, cone_direction, cone_radius, cone_height, cone_color);
    //    vertices.insert(vertices.end(), cone.vertices.begin(), cone.vertices.end());
    //    colors.insert(colors.end(), cone.colors.begin(), cone.colors.end());

}

glVector::glVector(Vertex a, Vertex b, float r, glm::vec3 color)
{
    setupAB(a, b, r, color);
}

void glVector::setupAB(Vertex a, Vertex b, float r, glm::vec3 color){

    Vector vector = {b.x-a.x, b.y-a.y, b.z-a.z};
    //    float vector_lenght = norm(vector);
    float vector_lenght = vector.length();

    // add cylinder
    //float height = 1;
    //float r = 0.03;
    float ratio = 0.2;
    //glm::vec3 cylinder_color = {1,0,0};
    //Vertex cylinder_base_center = {0,0,0};
    //Vertex cylinder_direction = {1,0,0};
    //cylinder_base_center = {0,0,0};
    //cylinder_direction = {1,0,0};

    Cylinder cylinder(a, vector, r, vector_lenght*(1-ratio), color);
    vertices = cylinder.vertices;
    colors = cylinder.colors;

    // add the cone
    // {height*(1-ratio),0,0};

    //    normalize(vector); // by reference
    //    Vector n = normalize(vector); // Vector copy
    //    vector.normalize();
    //    vector.normalize();

    //    Vertex cone_base_center = {a.x + normalize(vector).x*vector_lenght*(1-ratio),
    //                               a.y + normalize(vector).y*vector_lenght*(1-ratio),
    //                               a.z + normalize(vector).z*vector_lenght*(1-ratio)};
    Vertex cone_base_center = {a.x + vector.normalized().x*vector_lenght*(1-ratio),
                               a.y + vector.normalized().y*vector_lenght*(1-ratio),
                               a.z + vector.normalized().z*vector_lenght*(1-ratio)};

    //Vertex cone_direction = {1,0,0};
    color = {1,1,0};
    float cone_radius =  r*2.2;
    float cone_height = vector_lenght*(ratio);

    Cone cone( cone_base_center, vector, cone_radius, cone_height, color);
    vertices.insert(vertices.end(), cone.vertices.begin(), cone.vertices.end());
    colors.insert(colors.end(), cone.colors.begin(), cone.colors.end());

}

void glVector::setupVector(Vertex position, Vector vector, float r, glm::vec3 color){

    //Vertex vector = {b.x-a.x, b.y-a.y, b.z-a.z};
    //    float vector_lenght = norm(vector);
    float vector_lenght = vector.length();

    // add cylinder
    float ratio = 0.05;
    //glm::vec3 cylinder_color = {1,0,0};
    //Vertex cylinder_base_center = {0,0,0};
    //Vertex cylinder_direction = {1,0,0};
    //cylinder_base_center = {0,0,0};
    //cylinder_direction = {1,0,0};

    Cylinder cylinder(position, vector, r, vector_lenght*(1-ratio), color);
    vertices = cylinder.vertices;
    colors = cylinder.colors;

    // add the cone
    // {height*(1-ratio),0,0};

    //    Vector vector_normalized;
    //    vector_normalized = vector;
    //    normalize(vector_normalized);
    //    Vertex cone_base_center = {position.x + normalize(vector).x*vector_lenght*(1-ratio),
    //                               position.y + normalize(vector).y*vector_lenght*(1-ratio),
    //                               position.z + normalize(vector).z*vector_lenght*(1-ratio)};
    Vertex cone_base_center = {position.x + vector.normalized().x*vector_lenght*(1-ratio),
                               position.y + vector.normalized().y*vector_lenght*(1-ratio),
                               position.z + vector.normalized().z*vector_lenght*(1-ratio)};
    //Vertex cone_direction = {1,0,0};
    //color = {1,1,0};
    float cone_radius =  r*3.2;
    float cone_height = vector_lenght*(ratio);

    Cone cone( cone_base_center, vector, cone_radius, cone_height, color);
    vertices.insert(vertices.end(), cone.vertices.begin(), cone.vertices.end());
    colors.insert(colors.end(), cone.colors.begin(), cone.colors.end());

}



void glVector::setOrigin(Vertex origin){
    position.x = origin.x;
    position.y = origin.y;
    position.z = origin.z;
}

void glVector::setOrigin(float x, float y, float z){
    // alternative to
    //    glMagFieldVector.position.x = magField.col[0];
    //    glMagFieldVector.position.y = magField.col[1];
    //    glMagFieldVector.position.z = magField.col[2];

    position.x = x;
    position.y = y;
    position.z = z;
}

void glVector::setVector(Vector vec){
    vector.x = vec.x;
    vector.y = vec.y;
    vector.z = vec.z;
}

void glVector::setVector(float x, float y, float z){
    // alternative to
    //    glMagFieldVector.vector.x = magField.col[0];
    //    glMagFieldVector.vector.y = magField.col[1];
    //    glMagFieldVector.vector.z = magField.col[2];

    vector.x = x;
    vector.y = y;
    vector.z = z;
}

void glVector::setColor(float r, float g, float b){

    glm::vec3 color = {r,g,b};
    for (int i = 0; i< colors.size(); i++){
        colors.at(i) = color;
    }

}

void glVector::setLength(float len){
    // alternative to
    //    glMagFieldVector.lenght = l;

    lenght = len;
}


// new function
void glVector::setup(){

    // by using this function you must have defined the
    // vector, position and lenght of the vector
    // Example
    // glMagFieldVector.setLength(0);
    // glMagFieldVector.setOrigin(0,0,0);
    // glMagFieldVector.setVector(magField.col[0], magField.col[1], magField.col[2]);


    // add cylinder
    float ratio  = 0.2;
    float cylinder_radius = lenght/100.0;

    // default color
    glm::vec3 color = {1,1,0}; // yellow

    Cylinder cylinder(position, vector, cylinder_radius, lenght*(1-ratio), color);

    vertices = cylinder.vertices;
    colors = cylinder.colors;

    // add the cone
    Vertex cone_base_center = {position.x + vector.normalized().x*lenght*(1-ratio),
                               position.y + vector.normalized().y*lenght*(1-ratio),
                               position.z + vector.normalized().z*lenght*(1-ratio)};
    //Vertex cone_direction = {1,0,0};
    //color = {1,1,0};
    float cone_radius = cylinder_radius*3.2;
    float cone_height = lenght*(ratio);

    Cone cone( cone_base_center, vector, cone_radius, cone_height, color);
    vertices.insert(vertices.end(), cone.vertices.begin(), cone.vertices.end());
    colors.insert(colors.end(), cone.colors.begin(), cone.colors.end());
}


// new function
void glVector::setup(Vertex origin, Vector vec, float lenght, Color col){

    // by using this function you must have defined the
    // vector, position and lenght of the vector
    // Example
    // glMagFieldVector.setLength(0);
    // glMagFieldVector.setOrigin(0,0,0);
    // glMagFieldVector.setVector(magField.col[0], magField.col[1], magField.col[2]);

    setOrigin(origin);
    setVector(vec);
    setLength(lenght);

    glm::vec3 color = {col.r, col.g, col.b};

    // add cylinder
    float ratio  = 0.05;
    float cylinder_radius = lenght/200.0;

    Cylinder cylinder(position, vector, cylinder_radius, lenght*(1-ratio), color);

    vertices = cylinder.vertices;
    colors = cylinder.colors;

    // add the cone
    Vertex cone_base_center = {position.x + vector.normalized().x*lenght*(1-ratio),
                               position.y + vector.normalized().y*lenght*(1-ratio),
                               position.z + vector.normalized().z*lenght*(1-ratio)};
    //Vertex cone_direction = {1,0,0};
    //color = {1,1,0};
    float cone_radius = cylinder_radius*3.2;
    float cone_height = lenght*(ratio);

    Cone cone( cone_base_center, vector, cone_radius, cone_height, color);
    vertices.insert(vertices.end(), cone.vertices.begin(), cone.vertices.end());
    colors.insert(colors.end(), cone.colors.begin(), cone.colors.end());
}


void glVector::setup(Vertex origin, Vector vec, float lenght){
    // default color
    //    glm::vec3 color = {1,1,0}; // yellow
    Color color;
    color.r = 1;
    color.g = 1;
    color.b = 0;
    setup(origin, vec, lenght, color);
}


ReferenceFrame::ReferenceFrame(){

    //setReferenceFrame(1.0f);

    //setupVbo(vectorAxisX);
    //setupVbo(vectorAxisY);
    //setupVbo(vectorAxisZ);

}

void ReferenceFrame::setReferenceFrame(float size){

    // Default
    Vertex zero(0,0,0);
    Vector X(size,0,0);
    Vector Y(0,size,0);
    Vector Z(0,0,size);
    float length = size*0.005;

    //    vectorAxisX.setupVector({0,0,0}, {size,0,0}, size*0.005, {1,0,0});
    vectorAxisX.setupVector(zero, X, length, {1,0,0});
    vertices.insert(vertices.end(), vectorAxisX.vertices.begin(), vectorAxisX.vertices.end());
    colors.insert(colors.end(), vectorAxisX.colors.begin(), vectorAxisX.colors.end());

    //    vectorAxisY.setupVector({0,0,0}, {0,size,0}, size*0.005, {0,1,0});
    vectorAxisY.setupVector(zero, Y, length, {0,1,0});
    vertices.insert(vertices.end(), vectorAxisY.vertices.begin(), vectorAxisY.vertices.end());
    colors.insert(colors.end(), vectorAxisY.colors.begin(), vectorAxisY.colors.end());

    //    vectorAxisZ.setupVector({0,0,0}, {0,0,size}, size*0.005, {0,0,1});
    vectorAxisZ.setupVector(zero, Z, length, {0,0,1});
    vertices.insert(vertices.end(), vectorAxisZ.vertices.begin(), vectorAxisZ.vertices.end());
    colors.insert(colors.end(), vectorAxisZ.colors.begin(), vectorAxisZ.colors.end());

}


void ReferenceFrame::setReferenceFrame(Vector X, Vector Y, Vector Z, float lenght){

    Vertex zero(0,0,0);

    // ----
    vectorAxisX.setup(zero, X, lenght, {1,0,0});

    vertices.insert(vertices.end(), vectorAxisX.vertices.begin(), vectorAxisX.vertices.end());
    colors.insert(colors.end(), vectorAxisX.colors.begin(), vectorAxisX.colors.end());


    // ----
    vectorAxisY.setup(zero, Y, lenght, {0,1,0});

    vertices.insert(vertices.end(), vectorAxisY.vertices.begin(), vectorAxisY.vertices.end());
    colors.insert(colors.end(), vectorAxisY.colors.begin(), vectorAxisY.colors.end());


    // ----
    vectorAxisZ.setup(zero, Z, lenght, {0,0,1});

    vertices.insert(vertices.end(), vectorAxisZ.vertices.begin(), vectorAxisZ.vertices.end());
    colors.insert(colors.end(), vectorAxisZ.colors.begin(), vectorAxisZ.colors.end());

}


void ReferenceFrame::setReferenceFrame(Vector X, Vector Y, Vector Z,
                                       float lenght, Color color){

    Vertex zero(0,0,0);

    // ----
    vectorAxisX.setup(zero, X, lenght, color);

    vertices.insert(vertices.end(), vectorAxisX.vertices.begin(), vectorAxisX.vertices.end());
    colors.insert(colors.end(), vectorAxisX.colors.begin(), vectorAxisX.colors.end());


    // ----
    vectorAxisY.setup(zero, Y, lenght, color);

    vertices.insert(vertices.end(), vectorAxisY.vertices.begin(), vectorAxisY.vertices.end());
    colors.insert(colors.end(), vectorAxisY.colors.begin(), vectorAxisY.colors.end());


    // ----
    vectorAxisZ.setup(zero, Z, lenght, color);

    vertices.insert(vertices.end(), vectorAxisZ.vertices.begin(), vectorAxisZ.vertices.end());
    colors.insert(colors.end(), vectorAxisZ.colors.begin(), vectorAxisZ.colors.end());

}

//void ReferenceFrame::setReferenceFrame(Vector X, Vector Y, Vector Z,
//                                       float lenght){

//    Vertex zero(0,0,0);

//    // ----
//    vectorAxisX.setup(zero, X, lenght, {1,0,0});

//    vertices.insert(vertices.end(), vectorAxisX.vertices.begin(), vectorAxisX.vertices.end());
//    colors.insert(colors.end(), vectorAxisX.colors.begin(), vectorAxisX.colors.end());


//    // ----
//    vectorAxisY.setup(zero, Y, lenght, {0,1,0});

//    vertices.insert(vertices.end(), vectorAxisY.vertices.begin(), vectorAxisY.vertices.end());
//    colors.insert(colors.end(), vectorAxisY.colors.begin(), vectorAxisY.colors.end());


//    // ----
//    vectorAxisZ.setup(zero, Z, lenght, {0,0,1});

//    vertices.insert(vertices.end(), vectorAxisZ.vertices.begin(), vectorAxisZ.vertices.end());
//    colors.insert(colors.end(), vectorAxisZ.colors.begin(), vectorAxisZ.colors.end());

//}


void ReferenceFrame::setReferenceFrameInertial(float lenght){

    Vertex zero(0,0,0);
    Vector X(lenght,0,0);
    Vector Y(0,lenght,0);
    Vector Z(0,0,lenght);
    Color color;

    color = {1,1,1};

    // ----
    vectorAxisX.setup(zero, X, lenght, color);
    vertices.insert(vertices.end(), vectorAxisX.vertices.begin(), vectorAxisX.vertices.end());
    colors.insert(colors.end(), vectorAxisX.colors.begin(), vectorAxisX.colors.end());

    // ----
    vectorAxisY.setup(zero, Y, lenght, color);
    vertices.insert(vertices.end(), vectorAxisY.vertices.begin(), vectorAxisY.vertices.end());
    colors.insert(colors.end(), vectorAxisY.colors.begin(), vectorAxisY.colors.end());

    // ----
    vectorAxisZ.setup(zero, Z, lenght, color);
    vertices.insert(vertices.end(), vectorAxisZ.vertices.begin(), vectorAxisZ.vertices.end());
    colors.insert(colors.end(), vectorAxisZ.colors.begin(), vectorAxisZ.colors.end());

}



// Default constructor
Line::Line(){

    type = "line";

    // vertex #1
    vertices.push_back({0.0f, 0.0f, 0.0f});
    // vertex #2
    vertices.push_back({20.0f, 0.0f, 0.0f});

    // color vertex #1
    colors.push_back({1.0, 1.0, 1.0});
    // color vertex #2
    colors.push_back({1.0, 1.0, 1.0});

}

Line::Line(Vertex a, Vertex b){

    // vertex #1
    vertices.push_back({a.x, a.y, a.z});
    // vertex #2
    vertices.push_back({b.x, b.y, b.z});

    // color vertex #1
    colors.push_back({1.0, 1.0, 1.0});
    // color vertex #2
    colors.push_back({1.0, 1.0, 1.0});

}

void Line::updateVertices(Vertex a, Vertex b)
{
    // vertex #1
    vertices.at(0) = {a.x, a.y, a.z};
    // vertex #2
    vertices.at(1) = {b.x, b.y, b.z};
}


void Line::updateColors(Color a, Color b)
{
    // color for vertex #1
    colors.at(0) = {a.r, a.g, a.b};
    // color for vertex #2
    colors.at(1) = {b.r, b.g, b.b};
}

void Line::addVertex(Vertex v, Color c)
{
    vertices.push_back({v.x, v.y, v.z});
    colors.push_back({c.r, c.g, c.b});
}







Triangle::Triangle(){

    vertices.push_back({ -5.0f, -5.0f, 0.0f });
    vertices.push_back({ 5.0f, -5.0f, 0.0f });
    vertices.push_back({ 0.0f,  0.0f, 0.0f });

    colors.push_back({ 1.0f,  0.0f,  0.0f });
    colors.push_back({ 0.0f,  1.0f,  0.0f });
    colors.push_back({ 0.0f,  0.0f,  1.0f, });

}


//int Triangle::numVertices(){
//    return sizeof(vertex_data)/sizeof(vertex_data[0]);
//}

// notes
// putting vertex coordinates into vectors/arrays
// probably best is to use gml::vec

//std::vector<float> vertices, color;
//    // vertex #1
//    vertices.push_back(-width/2.0f); // X
//    vertices.push_back(0.0f); // Y
//    vertices.push_back(0.0f); // Z

//    // vertex #2
//    vertices.push_back(width/2.0f); // X
//    vertices.push_back(0.0f); // Y
//    vertices.push_back(0.0f); // Z

//    // vertex #3
//    vertices.push_back(width/2.0f); // X
//    vertices.push_back(height); // Y
//    vertices.push_back(0.0f); // Z

//    const GLfloat color_data[9*2] = {
//        1.0f,  0.0f,  0.0f,
//        1.0f,  0.0f,  0.0f,
//        1.0f,  0.0f,  0.0f,

//        0.0f,  1.0f,  0.0f,
//        0.0f,  1.0f,  0.0f,
//        0.0f,  1.0f,  0.0f,

//    };



