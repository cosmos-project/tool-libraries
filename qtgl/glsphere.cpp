#include "glsphere.h"
#include <cmath>

GLSphere::GLSphere(QObject *parent) :
    GLSphere(1.0, 32, 32, parent)
{
    //see below.
}

GLSphere::GLSphere(float radius, QObject *parent) :
    GLSphere(radius, 32, 32, parent)
{
    //see below.
}

GLSphere::GLSphere(float radius, uint32_t stacks, uint32_t slices, QObject *parent) :
    GLObject(parent)
{
    const float pi = 3.1415926535897932384626433832795f;
    const float _2pi = 2.0f * pi;

    for( size_t i = 0; i <= stacks; ++i )
    {
        // V texture coordinate.
        float V = i / (float)stacks;
        float phi = V * pi;

        for ( size_t j = 0; j <= slices; ++j )
        {
            // U texture coordinate.
            float U = j / (float)slices;
            float theta = U * _2pi;

            float X = cos(theta) * sin(phi);
            float Y = sin(theta) * sin(phi);
            float Z = cos(phi); // principal axis

            vertices.push_back({X*radius, Y*radius, Z*radius});
            normals.push_back({X, Y, Z});
            textureCoords.push_back({U, V});
        }

    }

    // Now generate the index buffer
    // iterate trhough each face of the sphere
    for( size_t i = 0; i < slices * stacks + slices; ++i )
    {
        // first triangle, upper
        vertIndexList.push_back( i );
        vertIndexList.push_back( i + slices );
        vertIndexList.push_back( i + slices + 1  );


        // second triangle, lower
        vertIndexList.push_back( i + slices + 1  );
        vertIndexList.push_back( i + 1 );
        vertIndexList.push_back( i );
    }
    setColor(1.0, 1.0, 1.0, 1.0); //no tinting by default
}
