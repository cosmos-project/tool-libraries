#ifndef GLCONE_H
#define GLCONE_H

#include <QObject>
#include "globject.h"

class GLCone : public GLObject
{
    Q_OBJECT
public:
    explicit GLCone(QObject *parent = 0);
    explicit GLCone(float radius, float height, QObject *parent = 0);
    explicit GLCone(float radius, float height, uint32_t stacks, QObject *parent = 0);

signals:

public slots:
};

#endif // GLCONE_H
