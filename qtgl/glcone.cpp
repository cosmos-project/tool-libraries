#include "glcone.h"
#include <cmath>

GLCone::GLCone(QObject *parent) :
    GLCone(0.25, 1.0, 30, parent)
{
    //see below.
}

GLCone::GLCone(float radius, float height, QObject *parent) :
    GLCone(radius, height, 30, parent)
{
    //see below.
}

GLCone::GLCone(float radius, float height, uint32_t stacks, QObject *parent) : GLObject(parent)
{
    const float pi = 3.1415926535897932384626433832795f;
    const float _2pi = 2.0f * pi;

    vertices.push_back({0.f, 0.f, 0.f});
    normals.push_back({0.f, 0.f, -1.f});

    for (size_t i = 0; i<stacks; i++) {
        float theta = (i * _2pi)/stacks;
        float X = cos(theta);
        float Y = sin(theta);
        float X_prime = cos(theta+(pi/stacks));
        float Y_prime = sin(theta+(pi/stacks));
        vertices.push_back({0.f, 0.f, height});
        float magnitude = sqrt(radius*radius + 2*height*height);
        normals.push_back({height*X_prime/magnitude, height*Y_prime/magnitude, radius/magnitude});
        vertices.push_back({radius*X, radius*Y, 0.f});
        normals.push_back({height*X/magnitude, height*Y/magnitude, radius/magnitude});
        vertices.push_back({radius*X, radius*Y, 0.f});
        normals.push_back({0.f, 0.f, -1.f});
        vertIndexList.push_back(1 + (3*i)%(stacks*3));
        vertIndexList.push_back(1 + (3*i+1)%(stacks*3));
        vertIndexList.push_back(1 + (3*i+4)%(stacks*3));
        vertIndexList.push_back(1 + (3*i+5)%(stacks*3));
        vertIndexList.push_back(1 + (3*i+2)%(stacks*3));
        vertIndexList.push_back(0);
    }
    setColor(1.0, 1.0, 1.0, 1.0); //no tinting by default
}

