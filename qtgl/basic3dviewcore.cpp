#include "basic3dviewcore.h"
#include <cmath>
#include "glcone.h"

#include <QPainter>
#include <QPaintEngine>

Basic3DViewCore::Basic3DViewCore(QObject *parent) :
    QObject(parent)
{
    lightColor = ambientColor = QColor("white");
}

Basic3DViewCore::~Basic3DViewCore() {

}

QColor Basic3DViewCore::bkgColor() const {
    return QColor(255*_r, 255*_g, 255*_b, 255*_a);
}

/*! Sets the background color of the view */
void Basic3DViewCore::setBkgColor(float r, float g, float b, float a) {
    if (r==_r&&g==_g&&b==_b&&a==_a) return;
    _r = r;
    _g = g;
    _b = b;
    _a = a;
    if (initialized) glClearColor(_r, _g, _b, _a);
    emit bkgColorChanged();
}

/*! Sets the background color of the view */
void Basic3DViewCore::setBkgColor(QColor newColor) {
    if (newColor!=QColor(255*_r, 255*_g, 255*_b, 255*_a)&&newColor.isValid()) {
        _r = newColor.redF();
        _g = newColor.greenF();
        _b = newColor.blueF();
        _a = newColor.alphaF();
        if (initialized) glClearColor(_r, _g, _b, _a);
        emit bkgColorChanged();
        emit redraw();
    }
}


void Basic3DViewCore::initializeGL() {
    initializeOpenGLFunctions();
    initialized = true;
    glClearColor(_r, _g, _b, _a);
    glEnable(GL_DEPTH_TEST); // Enable depth buffer
    glEnable(GL_CULL_FACE); // Enable back face culling
    glDisable(GL_BLEND); // Turn off blending (we don't really need it for now)
    for (auto i = shaders.begin(); i != shaders.end(); i++) {
        shaderList.push_back(new QOpenGLShaderProgram());
        shaderList.back()->addShaderFromSourceFile(QOpenGLShader::Vertex, (*i).first);
        shaderList.back()->addShaderFromSourceFile(QOpenGLShader::Fragment, (*i).second);
    }
    for (auto i = textures.begin(); i != textures.end(); i++) {
        textureList.push_back(new QOpenGLTexture(QImage(*i)));
    }
    calculatevMatrix(); //get an initial camera position
    calculatepMatrix(); //set initial camera projection
    initializeObjects();
}

/*! Initializes all child GLObjects so that they may be displayed in the view */
void Basic3DViewCore::initializeObjects() {
    QObject* thisObj = (QObject*)this;
    recursiveGLOInit(thisObj);
}

/*! Initializes a given GLObject and all its children.  Should be called whenever
 * a new object is added after the first frame is drawn. */
void Basic3DViewCore::initializeObject(GLObject* obj) {
    if (obj->textureIndex() != -1) obj->_textureReference = textureList[obj->textureIndex()];
    if (obj->shaderIndex() != -1) obj->_shaderReference = shaderList[obj->shaderIndex()];
    obj->initializeVAO();
    QObject* thisObj = (QObject*)obj;
    recursiveGLOInit(thisObj);
}
void Basic3DViewCore::recursiveGLOInit(QObject*& obj) {
    QObjectList childList = obj->children();
    GLObject* glo;
    for (auto i = childList.begin(); i!=childList.end(); i++) {
        if ((glo=qobject_cast<GLObject*>(*i))) {
            if (glo->textureIndex() != -1) glo->_textureReference = textureList[glo->textureIndex()];
            if (glo->shaderIndex() != -1) glo->_shaderReference = shaderList[glo->shaderIndex()];
            glo->initializeVAO();
            recursiveGLOInit(*i);
        }
    }
}

void Basic3DViewCore::resize(int w, int h) {
    // Calculate aspect ratio
    _aspect = qreal(w) / qreal(h ? h : 1);
    calculatepMatrix();
    emit aspectChanged();
}

void Basic3DViewCore::paintGL() {
    glClearColor(_r, _g, _b, _a);
    glEnable(GL_DEPTH_TEST); // Enable depth buffer
    glEnable(GL_CULL_FACE); // Enable back face culling
    glDisable(GL_BLEND); // Turn off blending (we don't really need it for now)
    glDepthMask(GL_TRUE);
    glStencilMask(GL_TRUE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    QObject* thisObj = (QObject*)this;
    recursiveGLODraw(thisObj, vMatrix);
}
void Basic3DViewCore::recursiveGLODraw(QObject*& obj, const QMatrix4x4 &MV) {
    QObjectList childList = obj->children();
    GLObject* glo;
    QMatrix4x4 childMV;
    QMatrix3x3 childNormal;
    for (auto i = childList.begin(); i != childList.end(); i++) {
        if ((glo=qobject_cast<GLObject*>(*i))) { //For every child that is a GLObject:
            //Calculate the new MV matrix
            childMV = MV * glo->mMatrix;
            childNormal = childMV.normalMatrix();
            //Draw if draw==true, and the object has a shader and vertices
            if (glo->drawn() && glo->_shaderReference!=NULL && !glo->vertices.empty()) {
                glo->vao.bind();
                glo->_shaderReference->bind();
                glo->indexBuffer.bind();
                if (glo->_textureReference!=NULL) glo->_textureReference->bind();
                glo->_shaderReference->setUniformValue("matrix_MVP", pMatrix * childMV);
                glo->_shaderReference->setUniformValue("matrix_MV", childMV);
                glo->_shaderReference->setUniformValue("matrix_Normal", childNormal);
                glo->_shaderReference->setUniformValue("sceneLightDirection", lightDirection);
                glo->_shaderReference->setUniformValue("sceneLightDirection_camspace", QVector3D(QMatrix4x4(vMatrix.normalMatrix())*QVector4D(lightDirection)));
                glo->_shaderReference->setUniformValue("sceneLightColor", lightColor);
                glo->_shaderReference->setUniformValue("sceneLightIntensity", lightIntensity);
                glo->_shaderReference->setUniformValue("ambientColor", ambientColor);
                glo->_shaderReference->setUniformValue("ambientIntensity", ambientIntensity);
                glo->_shaderReference->setUniformValue("objectLightDirection", glo->objectLightDirection);
                glo->_shaderReference->setUniformValue("objectLightDirection_camspace", QVector3D(QMatrix4x4(vMatrix.normalMatrix())*QVector4D(glo->objectLightDirection)));
                glo->_shaderReference->setUniformValue("objectLightColor", glo->objectLightColor);
                glo->_shaderReference->setUniformValue("objectLightIntensity", glo->objectLightIntensity);
                if (glo->vertIndexList.empty()) glDrawArrays(glo->drawMode, 0, glo->vertices.size());
                else glDrawElements(glo->drawMode, glo->vertIndexList.size(), GL_UNSIGNED_INT, NULL);
                glo->_shaderReference->release();
                if (glo->_textureReference!=NULL) glo->_textureReference->release();
                glo->vao.release();
            }
            //Draw the object's children
            recursiveGLODraw(*i, childMV);
        }
    }
}

/*! Gets the camera distance. */
float Basic3DViewCore::cameraDistance() const {
    return (float)pow(2.0, zoomFactor);
}

/*! Sets the camera distance without changing alpha or beta. */
void Basic3DViewCore::setCameraDistance(float distance) {
    float newFactor = log2(distance);
    if (zoomFactor!=newFactor) {
        zoomFactor = newFactor;
        calculatevMatrix();
        emit cameraDistanceChanged();
        emit redraw();
    }
}

void Basic3DViewCore::setCameraAlpha(float alpha) {
    if (_alpha!=alpha) {
        _alpha = alpha;
        while (_alpha < 0) _alpha += 360;
        while (_alpha >= 360) _alpha -= 360;
        calculatevMatrix();
        emit cameraAlphaChanged();
        emit redraw();
    }
}

void Basic3DViewCore::setCameraBeta(float beta) {
    if (_beta != beta) {
        _beta = beta;
        if (_beta < -90) _beta = -90;
        if (_beta > 90) _beta = 90;
        calculatevMatrix();
        emit cameraBetaChanged();
        emit redraw();
    }
}

void Basic3DViewCore::setNearClipPlane(float nearClipPlane) {
    if (nearClip != nearClipPlane) {
        nearClip = nearClipPlane;
        calculatepMatrix();
        emit nearClipPlaneChanged();
        emit redraw();
    }
}

void Basic3DViewCore::setFarClipPlane(float farClipPlane) {
    if (farClip != farClipPlane) {
        farClip = farClipPlane;
        calculatepMatrix();
        emit farClipPlaneChanged();
        emit redraw();
    }
}

/*! Sets the camera's field of view. */
void Basic3DViewCore::setFov(float fov) {
    if (_fov != fov) {
        _fov = fov;
        calculatepMatrix();
        emit fovChanged();
        emit redraw();
    }
}

/*! Sets the camera position. */
void Basic3DViewCore::setCameraPos(float distance, float alpha, float beta) {
    zoomFactor = log2(distance);
    _alpha = alpha;
    while (_alpha < 0) _alpha += 360;
    while (_alpha >= 360) _alpha -= 360;
    _beta = beta;
    if (_beta < -90) _beta = -90;
    if (_beta > 90) _beta = 90;
    calculatevMatrix();
    emit cameraDistanceChanged();
    emit cameraAlphaChanged();
    emit cameraBetaChanged();
    emit redraw();
}

/*! Sets the camera's clip planes. */
void Basic3DViewCore::setClipPlanes(float nearclip, float farclip) {
    nearClip = nearclip;
    farClip = farclip;
    calculatepMatrix();
    emit nearClipPlaneChanged();
    emit farClipPlaneChanged();
    emit redraw();
}

void Basic3DViewCore::calculatevMatrix() {
    QMatrix4x4 cameraTransformation;
    cameraTransformation.rotate(_alpha, 0, 0, 1);
    cameraTransformation.rotate(_beta, 0, 1, 0);
    QVector3D cameraPosition = cameraTransformation * QVector3D(pow(2.0, zoomFactor), 0, 0);
    QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 0, 1);
    vMatrix.setToIdentity();
    vMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);
}

void Basic3DViewCore::calculatepMatrix() {
    // Reset projection
    pMatrix.setToIdentity();
    // Set perspective projection
    pMatrix.perspective(_fov, _aspect, nearClip, farClip);
}

void Basic3DViewCore::mousePress(QPoint mousePos) {
    //save the current mouse position
    mouseLastPos = mousePos;
}
void Basic3DViewCore::mouseMove(QPoint mousePos) {
    //update _alpha & _beta
    _alpha -= (mousePos.x() - mouseLastPos.x())/5.;
    while (_alpha < 0) _alpha += 360;
    while (_alpha >= 360) _alpha -= 360;
    _beta -= (mousePos.y() - mouseLastPos.y())/5.;
    if (_beta < -90) _beta = -90;
    if (_beta > 90) _beta = 90;

    //recalculate the vMatrix
    calculatevMatrix();
    if (mousePos.x() != mouseLastPos.x()) emit cameraAlphaChanged(); //alpha was changed.
    if (mousePos.y() != mouseLastPos.y()) emit cameraBetaChanged(); //beta was changed.

    //save the current mouse position
    mouseLastPos = mousePos;
}
void Basic3DViewCore::wheelMove(int delta) {
    //update zoomFactor
    zoomFactor += delta/1000.;

    //recalculate the vMatrix
    calculatevMatrix();

    if (delta!=0) emit cameraDistanceChanged();
}
