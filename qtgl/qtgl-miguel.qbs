import qbs

Product {
    //    type: "staticlibrary"
    name: "qtgl"
    files: [ "*.cpp", "*.h" ]

    Depends { name: "cpp" }
    cpp.cxxLanguageVersion : "c++11"
    cpp.includePaths: [ "."]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: [ "."]
    }

}
