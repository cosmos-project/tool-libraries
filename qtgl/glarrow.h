#ifndef GLARROW_H
#define GLARROW_H

#include <QObject>
#include "globject.h"

class GLArrow : public GLObject
{
    Q_OBJECT

    // for QML
    Q_PROPERTY(QVector3D vector READ vector WRITE setVector NOTIFY vectorChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(int shaderIndex READ shaderIndex WRITE setShaderIndex NOTIFY shaderIndexChanged)


public:
    explicit GLArrow(QObject *parent = 0);

    void setVector(QVector3D vector);
    void setColor(QColor color);
    void setColor(float r, float g, float b, float a);
    void setShaderIndex(int idx);

private:
    GLObject* body;
    GLObject* point;

// ----------------------------------------------
// QML setup
private:

    // for QML
    QVector3D _vect = QVector3D(0,0,0);
    QColor _color = QColor(0,0,0);
    int _shaderIndex = 0;

public:

    // this only returns the property (for QML)
    QVector3D vector() const { return _vect; }
    QColor color() const { return _color; }
    int shaderIndex() const { return _shaderIndex; }

signals:
    void vectorChanged();
    void colorChanged();
    void shaderIndexChanged();
};

#endif // GLARROW_H
