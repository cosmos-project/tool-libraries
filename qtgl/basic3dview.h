#ifndef BASIC3DVIEW_H
#define BASIC3DVIEW_H

#include <QOpenGLWidget>
#include "basic3dviewcore.h"

class Basic3DView : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit Basic3DView(QWidget *parent = 0);

    Basic3DViewCore* view() const { return _view; }

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

private:
    Basic3DViewCore* _view;

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

signals:

public slots:
};

#endif // BASIC3DVIEW_H
