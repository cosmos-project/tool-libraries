#include "glarrow.h"
#include "glcone.h"
#include "glcylinder.h"

GLArrow::GLArrow(QObject *parent) : GLObject(parent)
{
    _vect = QVector3D(0.0, 0.0, 1.0); //not that it really matters
    body = new GLCylinder(0.01, 0.93, 8, this);
    point = new GLCone(0.03, 0.1, 15, body);
    point->mMatrix.translate(0.0, 0.0, 0.9);
}

void GLArrow::setVector(QVector3D vector)
{
    body->mMatrix.setToIdentity();
    body->mMatrix.scale(vector.length()*QVector3D(1.0, 1.0, 1.0));
    QVector3D cross = QVector3D::crossProduct(QVector3D(0, 0, 1), vector);
    QQuaternion quat(vector.length()+vector.z(), cross);
    quat.normalize();
    body->mMatrix.rotate(quat);
}

void GLArrow::setColor(QColor color)
{
    setColor(color.redF(), color.greenF(), color.blueF(), color.alphaF());
}

void GLArrow::setColor(float r, float g, float b, float a)
{
    body->setColor(r, g, b, a);
    point->setColor(r, g, b, a);
}

void GLArrow::setShaderIndex(int idx)
{
    body->setShaderIndex(idx);
    point->setShaderIndex(idx);
}

