#include "glModel3D.h"

// TODOs:
// - create a failsafe if file does not exist
// - overload function to load qt resource
GLModel3D::GLModel3D()
{

}

GLModel3D::GLModel3D(string inputfile)
{

}

void GLModel3D::loadFromFile(string inputfile)
{
    // Read our .obj file
    //bool res = loadOBJ(":/obj/cube.obj", model3dVertices, model3dUvs, model3dNormals);
    //bool res = loadOBJ(":/obj/cube_blender.obj", model3dVertices, model3dUvs, model3dNormals);
    //bool res = loadOBJ(":/obj/cube_blender.obj", model3dVertices, model3dUvs, model3dNormals);

    //bool res = loadOBJ(":/obj/cylinder.obj", model3dVertices, model3dUvs, model3dNormals);

    cout << "load object: " << inputfile << endl;
    string err = tinyobj::LoadObj(shapes, materials, inputfile.c_str());
    if (!err.empty()) {
        cerr << err << endl;
        exit(1);
    }

    cout << "# of shapes    : " << shapes.size() << endl;
    cout << "# of materials : " << materials.size() << endl;

    // debug info for .obj
    if (0){
        //cout << "# of shapes    : " << shapes.size() << endl;
        //cout << "# of materials : " << materials.size() << endl;
        for (size_t i = 0; i < shapes.size(); i++) {
            printf("shape[%ld].name = %s\n", i, shapes[i].name.c_str());
            printf("Size of shape[%ld].indices: %ld\n", i, shapes[i].mesh.indices.size());
            printf("Size of shape[%ld].material_ids: %ld\n", i, shapes[i].mesh.material_ids.size());
            assert((shapes[i].mesh.indices.size() % 3) == 0);
            for (size_t f = 0; f < shapes[i].mesh.indices.size() / 3; f++) {
                printf("  idx[%ld] = %d, %d, %d. mat_id = %d\n", f, shapes[i].mesh.indices[3*f+0], shapes[i].mesh.indices[3*f+1], shapes[i].mesh.indices[3*f+2], shapes[i].mesh.material_ids[f]);
            }

            printf("shape[%ld].vertices: %ld\n", i, shapes[i].mesh.positions.size());
            assert((shapes[i].mesh.positions.size() % 3) == 0);
            for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++) {
                printf("  v[%ld] = (%f, %f, %f)\n", v,
                       shapes[i].mesh.positions[3*v+0],
                        shapes[i].mesh.positions[3*v+1],
                        shapes[i].mesh.positions[3*v+2]);
            }
        }
    }

    // debug
    if (0){

        for (size_t i = 0; i < materials.size(); i++) {
            printf("material[%ld].name = %s\n", i, materials[i].name.c_str());
            printf("  material.Ka = (%f, %f ,%f)\n", materials[i].ambient[0], materials[i].ambient[1], materials[i].ambient[2]);
            printf("  material.Kd = (%f, %f ,%f)\n", materials[i].diffuse[0], materials[i].diffuse[1], materials[i].diffuse[2]);
            printf("  material.Ks = (%f, %f ,%f)\n", materials[i].specular[0], materials[i].specular[1], materials[i].specular[2]);
            printf("  material.Tr = (%f, %f ,%f)\n", materials[i].transmittance[0], materials[i].transmittance[1], materials[i].transmittance[2]);
            printf("  material.Ke = (%f, %f ,%f)\n", materials[i].emission[0], materials[i].emission[1], materials[i].emission[2]);
            printf("  material.Ns = %f\n", materials[i].shininess);
            printf("  material.Ni = %f\n", materials[i].ior);
            printf("  material.dissolve = %f\n", materials[i].dissolve);
            printf("  material.illum = %d\n", materials[i].illum);
            printf("  material.map_Ka = %s\n", materials[i].ambient_texname.c_str());
            printf("  material.map_Kd = %s\n", materials[i].diffuse_texname.c_str());
            printf("  material.map_Ks = %s\n", materials[i].specular_texname.c_str());
            printf("  material.map_Ns = %s\n", materials[i].normal_texname.c_str());
            map<string, string>::const_iterator it(materials[i].unknown_parameter.begin());
            map<string, string>::const_iterator itEnd(materials[i].unknown_parameter.end());
            for (; it != itEnd; it++) {
                printf("  material.%s = %s\n", it->first.c_str(), it->second.c_str());
            }
            cout << endl;
        }
    }


    // Combine the data into sequential triangles
    // For each vertex of each triangle
    cout << "buildign attributes buffers" << endl;
    for (int shapeN = 0; shapeN < shapes.size(); shapeN++) {
        for( unsigned int i=0; i<shapes[shapeN].mesh.indices.size(); i++ ){

            // Get the indices of its attributes
            unsigned int id = shapes[shapeN].mesh.indices[i];

            //unsigned int uvIndex = uvIndices[i];
            //unsigned int normalIndex = normalIndices[i];

            // Put the attributes in buffers
            if (shapes[shapeN].mesh.positions.size() > 0){
                vertices.push_back({shapes[shapeN].mesh.positions[ 3*id+0 ],
                                           shapes[shapeN].mesh.positions[ 3*id+1 ],
                                           shapes[shapeN].mesh.positions[ 3*id+2 ]});
            }

            if (shapes[shapeN].mesh.normals.size() > 0){
                normals.push_back({shapes[shapeN].mesh.normals[ 3*id+0 ],
                                          shapes[shapeN].mesh.normals[ 3*id+1 ],
                                          shapes[shapeN].mesh.normals[ 3*id+2 ]});
            }

            if (materials.size() > 0){
                colors.push_back({materials[0].diffuse[0],
                                         materials[0].diffuse[1],
                                         materials[0].diffuse[2]});
            } else {
                colors.push_back({1,0,0});
            }

            //out_uvs     .push_back(uv);

        }
    }
    cout << "end" << endl;

    // add colors
    //    for (int i=0; i<model3dVertices.size()/3; i++){
    //        model3dColors.push_back({0.4,0,0});
    //        model3dColors.push_back({0.3,0.6,0.6});
    //        model3dColors.push_back({0.1,0.9,0.3});
    //    }
}
