#include "globject.h"
#include <QDebug>

GLObject::GLObject(QObject *parent) : QObject(parent)
{
    //indexBuffer must be of the IndexBuffer type (all others can be deafults)
    QOpenGLBuffer idxBuff(QOpenGLBuffer::IndexBuffer);
    indexBuffer = idxBuff;
    mMatrix.setToIdentity();
    objectLightDirection = QVector3D(0, 0, 1); //b/c the default (0, 0, 0) is unnormalizable, knocks out fragment shader entirely.
}

void GLObject::setUpBuffers() {
    if (vertices.size()>0) {
        vertexBuffer.create();
        vertexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        vertexBuffer.bind();
        vertexBuffer.allocate(&vertices[0], vertices.size()*sizeof(vertices[0]));
        _shaderReference->enableAttributeArray("vertexPosition");
        _shaderReference->setAttributeArray("vertexPosition", GL_FLOAT, 0, 3);
        vertexBuffer.release();
    }

    if (colors.size()>0) {
        colorBuffer.create();
        colorBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        colorBuffer.bind();
        colorBuffer.allocate(&colors[0], colors.size()*sizeof(colors[0]));
        _shaderReference->enableAttributeArray("vertexColor");
        _shaderReference->setAttributeArray("vertexColor", GL_FLOAT, 0, 4);
        colorBuffer.release();
    }

    if (normals.size()>0) {
        normalBuffer.create();
        normalBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        normalBuffer.bind();
        normalBuffer.allocate(&normals[0], normals.size()*sizeof(normals[0]));
        _shaderReference->enableAttributeArray("vertexNormal");
        _shaderReference->setAttributeArray("vertexNormal", GL_FLOAT, 0, 3);
        normalBuffer.release();
    }

    if (vertIndexList.size()>0) {
        indexBuffer.create();
        indexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBuffer.bind();
        indexBuffer.allocate(&vertIndexList[0], vertIndexList.size()*sizeof(vertIndexList[0]));
        indexBuffer.release(); //probably isn't scooped up by the VAO and has to be bound every time the scene is drawn.
    }

    if (textureCoords.size()>0) {
        texCoordsBuffer.create();
        texCoordsBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        texCoordsBuffer.bind();
        texCoordsBuffer.allocate(&textureCoords[0], textureCoords.size()*sizeof(textureCoords[0]));
        _shaderReference->enableAttributeArray("a_texcoord");
        _shaderReference->setAttributeArray("a_texcoord", GL_FLOAT, 0, 2);
        texCoordsBuffer.release();
    }
}

void GLObject::initializeVAO() {
    if (_shaderReference==NULL) return;
    vao.create();
    vao.bind();
    _shaderReference->bind(); //also needs to be bound before drawing, isn't scooped up by VAO.
    if (_textureReference!=NULL) {
        _textureReference->bind();
        _shaderReference->setUniformValue("texture", 0); //use texture unit 0
        //(hopefully we won't need to worry about multiple textures per shader any time soon)
    }
    setUpBuffers();
    _shaderReference->release();
    if (_textureReference!=NULL) _textureReference->release();
    vao.release();
}

void GLObject::setColor(QColor color) {
    setColor(color.redF(), color.greenF(), color.blueF(), color.alphaF());
}
void GLObject::setColor(float r, float g, float b, float a) {
    colors.clear();
    for (size_t i=0; i<vertices.size(); i++) colors.push_back({r, g, b, a});
}

void GLObject::setDrawn(bool drawn) {
    if (_drawn != drawn) {
        _drawn = drawn;
        emit drawnChanged();
    }
}

