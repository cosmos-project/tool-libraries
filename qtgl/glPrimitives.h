#ifndef OpenGLPrimitive_H
#define OpenGLPrimitive_H

// later it would be good to remove the Qt dependency from here
// hum ... I think I'm leaving this with Qt dependency because after
// all this is a graphical application and it would require some other
// lib like GLFW or SDL to make it work, sounds fair?

//#include <QGLWidget> // for GLfloat type
#include <QOpenGLWidget>
#include <QOpenGLBuffer>

#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>

// add GLM
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
//#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtx/matrix_operation.hpp>


// ------------------------------------------------------------------
class VBO{
public:
    QOpenGLBuffer vertex;
    QOpenGLBuffer color;
};


// ------------------------------------------------------------------
class Vector {

public:
    Vector();
    Vector(float value_x, float value_y, float value_z);

    void   normalize();    // changes the source vector in place,
    Vector normalized(); // returns normalized vector
    float  length();
    void   reNormalize(float scale);
    float x, y, z;

private:
    //float x, y, z;
};

// ------------------------------------------------------------------
class Vertex {
public:
    float x, y, z;
    Vertex();
    Vertex(float x, float y, float z);
};


// ------------------------------------------------------------------
struct Color {
    float r, g, b;
};

// ------------------------------------------------------------------
// OpenGLPrimitive
class OpenGLPrimitive
{

public:
    OpenGLPrimitive();
    //void Cube();
    VBO vbo;
    std::vector<glm::vec3> vertices, colors;
    QString type;
    void setupVbo(); //OpenGLPrimitive primitive
//    OpenGLPrimitive primitive;

public:
////    double norm(Vertex v);
//    double norm(Vector v);
////    void normalize(Vertex &v);
//    void normalize(Vector &v);
//    void reNormalize(Vector &v, float scale);
//    void reNormalizeOffset(Vector& v, float scale, Vertex offset);

};



class glVector : public OpenGLPrimitive
{

public:
    glVector();
    glVector(Vertex a, Vertex b, float r, glm::vec3 color);
    void setupAB(Vertex a, Vertex b, float r, glm::vec3 color);
    void setupVector(Vertex position, Vector vector, float r, glm::vec3 color);

    void setup();
    void setup(Vertex origin, Vector vec, float lenght);
    void setup(Vertex origin, Vector vec, float lenght, Color col);

    void setLength(float len);

    void setOrigin(Vertex origin);
    void setOrigin(float x, float y, float z);

    void setVector(Vector vec);
    void setVector(float x, float y, float z);

    void setColor(float r, float g, float b);


    // pyshical properties of the vector
    Vector vector;
    Vertex position;
    float lenght;
};



class Triangle : public OpenGLPrimitive{

public:
    Triangle();
    //int numVertices();

    //    const GLfloat vertex_data[27] = {
    //        -5.0f, -5.0f, 0.0f,
    //        5.0f, -5.0f, 0.0f,
    //        0.0f,  0.0f, 0.0f,

    //        //        1.0f, 0.0f, 0.0f,
    //        //        3.0f, 0.0f, 0.0f,
    //        //        2.0f, 1.0f, 0.0f,

    //        //        3.0f, 0.0f, 0.0f,
    //        //        4.0f, 0.0f, 0.0f,
    //        //        3.0f, 1.0f, 0.0f,
    //    };

    //    const GLfloat color_data[27] = {
    //        1.0f,  0.0f,  0.0f,
    //        0.0f,  1.0f,  0.0f,
    //        0.0f,  0.0f,  1.0f,

    //        //        0.822f,  0.569f,  0.201f,
    //        //        0.435f,  0.602f,  0.223f,
    //        //        0.310f,  0.747f,  0.185f,

    //        //        0.597f,  0.770f,  0.761f,
    //        //        0.559f,  0.436f,  0.730f,
    //        //        0.359f,  0.583f,  0.152f,
    //    };

};





class Cylinder : public OpenGLPrimitive
{

public:
    Cylinder();
    Cylinder(float r, float height, glm::vec3 color);
    Cylinder(Vertex base_center, Vector direction, float radius, float height, glm::vec3 color);


    void setup(float r, float height, glm::vec3 color);
    void setupPanel(Vertex base_a, Vertex base_b, float height);
};



class Cone : public OpenGLPrimitive
{
public:
    Cone();
    Cone(float r, float height, float cone_height);
    Cone(Vertex base_center, Vector direction, float base_radius, float height, glm::vec3 color);
};




class ReferenceFrame : public OpenGLPrimitive
{
    glVector vectorAxisX, vectorAxisY, vectorAxisZ ;
    //float size = 1; // default size
public:
    ReferenceFrame();
    void setReferenceFrame(float size);
    void setReferenceFrame(Vector X, Vector Y, Vector Z, float lenght);
    void setReferenceFrame(Vector X, Vector Y, Vector Z, float lenght, Color color);
    void setReferenceFrameInertial(float lenght);
};


class Line : public OpenGLPrimitive
{

public:
    Line();
    Line(Vertex, Vertex);
    void updateVertices(Vertex a, Vertex b);
    void updateColors(Color a, Color b);
    void addVertex(Vertex v, Color c);

    //    std::vector<glm::vec3> vertices, color;

    //    //
    //    QOpenGLBuffer mVboVertex;
    //    QOpenGLBuffer mVboColor;
};


class Cube : public OpenGLPrimitive
{

public:
    // default constructor, if no argument is given then default size is 1
    Cube(float size = 1.0f);
//    Cube(float size = 1.0f, Color color = {1.0,1.0,0.0});

    void setSize(float size);
    void setDefaultColors();
    void setColor(Color color);

    //int numVertices();
    //std::vector<double> vd {1.2,3};
};

#endif // OpenGLPrimitive_H
