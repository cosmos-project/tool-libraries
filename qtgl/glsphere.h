#ifndef GLSPHERE_H
#define GLSPHERE_H

#include <QObject>
#include "globject.h"

class GLSphere : public GLObject
{
    Q_OBJECT
public:
    explicit GLSphere(QObject* parent = 0);
    explicit GLSphere(float radius, QObject* parent = 0);
    explicit GLSphere(float radius, uint32_t stacks, uint32_t slices, QObject* parent = 0);

};

#endif // GLSPHERE_H
