#ifndef GLMODEL3D_H
#define GLMODEL3D_H

// include C++ stuff
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

// include Qt stuff
#include <QVector3D>

// thirdparty libraries
#include "tinyobjloader/tiny_obj_loader.h"

// include GLM
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtx/transform.hpp>
//#include <glm/gtc/quaternion.hpp>
//#include <glm/gtx/quaternion.hpp>
//#include <glm/gtc/matrix_transform.hpp>

class GLModel3D
{
private:
    // model from tinyobject
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;
public:
    GLModel3D();
    GLModel3D(string inputfile);

    vector<QVector3D> vertices; // TODO: change to vector<Vector3d>
    vector<glm::vec2> uvs;
    vector<glm::vec3> normals;
    vector<glm::vec3> colors;
    void loadFromFile(string inputfile);
};

#endif // GLMODEL3D_H
