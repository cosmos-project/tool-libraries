#ifndef BASIC3DVIEWCORE_H
#define BASIC3DVIEWCORE_H

#include <QObject>

// QtOpenGl
#include <QOpenGLContext>
#include <QSurfaceFormat>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

// Other Qt libs
//#include <QMouseEvent>
#include <QColor>
#include <QList>
#include <QPair>

#include "globject.h"

class Basic3DViewCore : public QObject, QOpenGLFunctions
{
    Q_OBJECT

    Q_PROPERTY(float cameraDistance READ cameraDistance WRITE setCameraDistance NOTIFY cameraDistanceChanged)
    Q_PROPERTY(QColor bkgColor READ bkgColor WRITE setBkgColor NOTIFY bkgColorChanged)
    Q_PROPERTY(float cameraAlpha READ cameraAlpha WRITE setCameraAlpha NOTIFY cameraAlphaChanged)
    Q_PROPERTY(float cameraBeta READ cameraBeta WRITE setCameraBeta NOTIFY cameraBetaChanged)
    Q_PROPERTY(float nearClipPlane READ nearClipPlane WRITE setNearClipPlane NOTIFY nearClipPlaneChanged)
    Q_PROPERTY(float farClipPlane READ farClipPlane WRITE setFarClipPlane NOTIFY farClipPlaneChanged)
    Q_PROPERTY(float fov READ fov WRITE setFov NOTIFY fovChanged)
    Q_PROPERTY(float aspect READ aspect NOTIFY aspectChanged)
    //Eventually everything should be a property, but for now I'm moving on.

public:
    explicit Basic3DViewCore (QObject *parent = 0);
    ~Basic3DViewCore();
    QColor bkgColor() const;
    void setBkgColor(QColor newColor);
    void setBkgColor(float r, float g, float b, float a);

    float cameraDistance() const;
    void setCameraDistance(float distance);

    float cameraAlpha() const { return _alpha; }
    void setCameraAlpha(float alpha);

    float cameraBeta() const { return _beta; }
    void setCameraBeta(float beta);

    float nearClipPlane() const { return nearClip; }
    void setNearClipPlane(float nearClipPlane);

    float farClipPlane() const { return farClip; }
    void setFarClipPlane(float farClipPlane);

    float fov() const { return _fov; }
    void setFov(float fov);

    float aspect() const { return _aspect; }

    void setCameraPos(float distance, float alpha, float beta);
    void setClipPlanes(float near nearclip, float far farclip);

    void initializeObjects(); //Initialize every object
    void initializeObject(GLObject* obj); //Initialize this object and all its children

    QVector3D lightDirection;
    QColor lightColor;
    QColor ambientColor;
    float lightIntensity = 1.0;
    float ambientIntensity = 0.3;
    QList<QString> textures;
    QList<QPair<QString, QString>> shaders; //defines the available shaders by vertex (.vsh) and fragment (.fsh) filenames (in that order)

    void initializeGL();
    void resize(int w, int h);
    void paintGL();

    void mousePress(QPoint mousePos);
    void mouseMove(QPoint mousePos);
    void wheelMove(int delta);

protected:

    QList<QOpenGLTexture*> textureList; //list of textures used in the scene.
    QList<QOpenGLShaderProgram*> shaderList; //list of shaders used in the scene.

private:
    QMatrix4x4 pMatrix; // projection
    QMatrix4x4 vMatrix; // view
    //  MVP = pMatrix * vMatrix * mMatrix

    bool initialized = false;

    void recursiveGLOInit(QObject*& obj);
    void recursiveGLODraw(QObject*& obj, const QMatrix4x4 &MV);


    //scene properties
    float _r=0.1;
    float _g=0.1;
    float _b=0.3;
    float _a=1.0;

    //camera properties
    qreal nearClip = 0.05;
    qreal farClip = 50.0;
    qreal _aspect = 1.0;
    qreal _fov = 45.0;
    float _alpha = 0;
    float _beta = 0;
    float zoomFactor = 2;

    // mouse interaction
    QPoint mouseLastPos;
    void calculatevMatrix();
    void calculatepMatrix();

signals:
    void cameraDistanceChanged();
    void bkgColorChanged();
    void cameraAlphaChanged();
    void cameraBetaChanged();
    void nearClipPlaneChanged();
    void farClipPlaneChanged();
    void fovChanged();
    void aspectChanged();

    void redraw(); //should trigger a re-render (automatically emitted after all of the above signals (except aspectChanged(), where it isn't needed))

};

#endif // BASIC3DVIEWCORE_H
