#include "basic3dviewqtquick.h"

#include <QtGui/QOpenGLFramebufferObject>

#include <QtQuick/QQuickWindow>
#include <QSGTransformNode>
//#include <qsgsimpletexturenode.h>

class Basic3DViewRenderer : public QQuickFramebufferObject::Renderer
{
public:
    Basic3DViewRenderer(Basic3DViewCore* view)
    {
        _view = view;
        _view->initializeGL();
    }

    void render()
    {
        _view->paintGL();
        update();
    }

    QOpenGLFramebufferObject* createFramebufferObject(const QSize &size)
    {
        QOpenGLFramebufferObjectFormat format;
        format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
        format.setSamples(0); //anti-aliasing thing, we don't really need it for what we do.
        format.setInternalTextureFormat(GL_RGB8);
        _view->resize(size.width(), size.height());
        return new QOpenGLFramebufferObject(size, format);
    }

    Basic3DViewCore* _view;
};

Basic3DViewQtQuick::Basic3DViewQtQuick(QQuickItem *parent) :
    QQuickFramebufferObject(parent)
{
    _view = new Basic3DViewCore();
    setAcceptedMouseButtons(Qt::AllButtons);
}

QQuickFramebufferObject::Renderer* Basic3DViewQtQuick::createRenderer() const {
    auto rdr = new Basic3DViewRenderer(_view);
    connect(_view, SIGNAL(redraw()), this, SLOT(update()));
    return rdr;
}

//Fixes the mysterious image flipping, but at the terrible cost of margins no longer working for some reason.
QSGNode* Basic3DViewQtQuick::updatePaintNode(QSGNode *const inOutNode, UpdatePaintNodeData *const inOutData) {
    const int width = this->width();
    const int height = this->height();
    QMatrix4x4 flipY;
    flipY.translate(width*0.5, height*0.5);
    flipY.scale(1.0, -1.0);
    flipY.translate(-width*0.5, -height*0.5);
    inOutData->transformNode->setMatrix(flipY);
    return QQuickFramebufferObject::updatePaintNode(inOutNode, inOutData);
}

void Basic3DViewQtQuick::mousePressEvent(QMouseEvent *event) {
    _view->mousePress(event->pos());
}

void Basic3DViewQtQuick::mouseMoveEvent(QMouseEvent *event) {
    _view->mouseMove(event->pos());
}

void Basic3DViewQtQuick::wheelEvent(QWheelEvent *event) {
    int delta = event->angleDelta().y();
    if (abs(delta)>abs(event->angleDelta().x())) {
        _view->wheelMove(delta);
    }
}
