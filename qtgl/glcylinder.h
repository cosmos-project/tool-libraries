#ifndef GLCYLINDER_H
#define GLCYLINDER_H

#include <QObject>
#include "globject.h"

class GLCylinder : public GLObject
{
    Q_OBJECT
public:
    explicit GLCylinder(QObject *parent = 0);
    explicit GLCylinder(float radius, float height, QObject *parent = 0);
    explicit GLCylinder(float radius, float height, uint32_t stacks, QObject *parent = 0);

};

#endif // GLCYLINDER_H
