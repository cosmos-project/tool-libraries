uniform mat4 matrix_MVP; // model, view, projection matrix

//in vec3 vertexPosition;
attribute vec4 vertexPosition;

//in vec4 vertexColor;
attribute vec4 vertexColor;

//varying lowp vec3 vertexColorVar;
varying vec4 vertexColorVar;

// for textures
attribute vec2 a_texcoord;
varying vec2 v_texcoord;

void main(){

    // Calculate vertex position in screen space
    //gl_Position = vertexPosition;
    gl_Position = matrix_MVP * vertexPosition; // 4-dim
    //gl_Position = matrix_MVP * vec4(vertexPosition,1); // 3-dim

    // pass vertex color
    vertexColorVar = vertexColor;

}
