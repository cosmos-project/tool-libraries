//attribute highp vec4 vertexPosition;
//attribute lowp vec4 vertexColor;
//varying lowp vec4 vertexColorVar;

uniform mat4 matrix_MVP;

attribute vec4 vertexPosition;
attribute vec2 a_texcoord;
varying vec2 v_texcoord;

void main(){

    //vertexColorVar = vertexColor;
    //gl_Position = vertexPosition;

    // Calculate vertex position in screen space
    gl_Position = matrix_MVP * vertexPosition;

    // Pass texture coordinate to fragment shader
    // Value will be automatically interpolated to fragments inside polygon faces
    v_texcoord = a_texcoord;

}
