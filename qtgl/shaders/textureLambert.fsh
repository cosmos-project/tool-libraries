uniform vec3 lightDirection;
uniform vec4 sceneLightColor;
uniform float sceneLightIntensity;
uniform vec4 ambientColor;
uniform float ambientIntensity;
uniform sampler2D texture;

varying vec2 v_texcoord;
varying vec3 N;
varying vec4 col;

void main(void)
{
   vec4 Idiff = sceneLightColor * (sceneLightIntensity * max(dot(normalize(N),lightDirection), 0.0));
   Idiff = Idiff + ambientColor * ambientIntensity;
   Idiff = Idiff * col * texture2D(texture, v_texcoord);

   gl_FragColor = clamp(Idiff, 0.0, 1.0);
}
