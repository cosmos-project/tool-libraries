uniform mat4 matrix_MVP; //model, view, projection matrix

attribute vec4 vertexPosition;
attribute vec4 vertexNormal;
attribute vec4 vertexColor;

varying vec3 N;
varying vec4 col;


void main(void)
{
   gl_Position = matrix_MVP * vertexPosition;
   N = vec3(vertexNormal);
   col = vertexColor;
}
