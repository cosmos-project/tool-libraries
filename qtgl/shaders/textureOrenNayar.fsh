uniform vec3 objectLightDirection_camspace; //the direction to the sun is calculated on a per-object basis
uniform vec4 sceneLightColor; //using scene light for all lighting properties (for convenience)
uniform float sceneLightIntensity;
uniform vec4 ambientColor;
uniform float ambientIntensity;
uniform sampler2D texture;

varying vec2 v_texcoord;
varying vec3 N;
varying vec3 v;
varying vec4 col;

//========Parameters========
const float roughness = 0.9;
const float terminatorSharpness = 11.0;
//==========================

const float roughnessSquared = roughness*roughness;
const float A = 1.0 - 0.5*(roughnessSquared/(roughnessSquared + 0.57));
const float B = 0.45 * (roughnessSquared/(roughnessSquared + 0.09));

//Based on implementation found here: http://ruh.li/GraphicsOrenNayar.html

void main(void)
{
    vec3 eyeDir = normalize(v);
    vec3 norm = normalize(N);
    float NdotL = dot(norm, objectLightDirection_camspace);
    float NdotV = dot(norm, eyeDir);

    float angleVN = acos(NdotV);
    float angleLN = acos(NdotL);

    float alpha = max(angleVN, angleLN);
    float beta = min(angleVN, angleLN);
    //The dot product of the aimuthal components of eyeDir and N, i.e. cos(phi_light - phi_eye)
    float gamma = dot(eyeDir - norm * NdotV, objectLightDirection_camspace - norm * NdotL);

    float C = min(terminatorSharpness, sin(alpha)*tan(beta)); //C kept below sharpness limit to soften terminator

    vec4 L1 = sceneLightColor * (sceneLightIntensity * max(NdotL, 0.0) * (A + B*max(0.0, gamma)*C));
    L1 = L1 + ambientColor * ambientIntensity;
    L1 = L1 * col * texture2D(texture, v_texcoord);

    gl_FragColor = clamp(L1, 0.0, 1.0);
}
