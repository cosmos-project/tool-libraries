const vec3 lightDirection = vec3(1.0/1.73, 1.0/1.73, 1.0/1.73); //remember to normalize!
const vec4 lightColor = vec4(1.0, 1.0, 1.0, 1.0);
const float lightIntensity = 0.6;
const vec4 ambientColor = vec4(1.0, 1.0, 1.0, 1.0);
const float ambientIntensity = 0.4;

varying vec3 N;
varying vec4 col;

void main(void)
{
   vec4 Idiff = lightColor * (lightIntensity * max(dot(normalize(N),lightDirection), 0.0));
   Idiff = Idiff + ambientColor * ambientIntensity;
   Idiff = Idiff * col;

   gl_FragColor = clamp(Idiff, 0.0, 1.0);
}
