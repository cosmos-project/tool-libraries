uniform sampler2D texture;
varying vec2 v_texcoord;

//varying lowp vec4 vertexColorVar;

void main()
{

    // Output color = red
    // color = vec3(1,0,0);
    //gl_FragColor = vertexColorVar;
    //gl_FragColor = col;
//    gl_FragColor = vec4(1,1,0,0);

    // Set fragment color from texture
    gl_FragColor = texture2D(texture, v_texcoord);

}
