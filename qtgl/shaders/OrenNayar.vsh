uniform mat4 matrix_MVP; //model, view, projection matrix
uniform mat4 matrix_MV; //model and view matrix
uniform mat3 matrix_Normal; //rotation into camera frame

attribute vec4 vertexPosition;
attribute vec4 vertexNormal;
attribute vec4 vertexColor;

varying vec3 N;
varying vec3 v;
varying vec4 col;


void main(void)
{
    v = vec3(matrix_MV * vertexPosition);
    gl_Position = matrix_MVP * vertexPosition;
    N = matrix_Normal * vec3(vertexNormal);
    col = vertexColor;
}
