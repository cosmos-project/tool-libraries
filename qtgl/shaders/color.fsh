//varying lowp vec3 vertexColorVar;
varying lowp vec4 vertexColorVar;

uniform sampler2D texture;
varying vec2 v_texcoord;

void main()
{

    gl_FragColor = vertexColorVar;
    //gl_FragColor = vec4(1,1,0,0);
    //gl_FragColor = texture2D(texture, v_texcoord);
}
