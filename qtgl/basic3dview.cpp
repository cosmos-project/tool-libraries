#include "basic3dview.h"
#include <QMouseEvent>

Basic3DView::Basic3DView(QWidget *parent) : QOpenGLWidget(parent)
{
    _view = new Basic3DViewCore(this);
    connect(_view, SIGNAL(redraw()), this, SLOT(update()));
}

void Basic3DView::initializeGL() {
   _view->initializeGL();
}

void Basic3DView::resizeGL(int w, int h) {
    _view->resize(w, h);
    update();
}

void Basic3DView::paintGL() {
    _view->paintGL();
}

void Basic3DView::mousePressEvent(QMouseEvent *event) {
    _view->mousePress(event->pos());
}

void Basic3DView::mouseMoveEvent(QMouseEvent *event) {
    _view->mouseMove(event->pos());
    update();
}

void Basic3DView::wheelEvent(QWheelEvent *event) {
    int delta = event->angleDelta().y();
    if (abs(delta)>abs(event->angleDelta().x())) {
        _view->wheelMove(delta);
        update();
    }
}
