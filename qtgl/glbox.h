#ifndef GLBOX_H
#define GLBOX_H

#include <QObject>
#include "globject.h"

class GLBox : public GLObject
{
    Q_OBJECT
public:
    explicit GLBox(QObject* parent = 0);
    explicit GLBox(float size, QObject* parent = 0);
    explicit GLBox(float width, float height, float length, QObject* parent = 0);

};

#endif // GLBOX_H
