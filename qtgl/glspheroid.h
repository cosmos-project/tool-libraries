#ifndef GLSPHEROID_H
#define GLSPHEROID_H

#include <QObject>
#include "globject.h"
#include <QColor>

class GLSpheroid : public GLObject
{
    Q_OBJECT
public:
    explicit GLSpheroid(QObject* parent = 0);
    explicit GLSpheroid(float eqRadius, float aspect, QObject* parent = 0);
    explicit GLSpheroid(float eqRadius, float aspect, uint32_t stacks, uint32_t slices, QObject* parent = 0);

};

#endif // GLSPHEROID_H
