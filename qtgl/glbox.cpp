#include "glbox.h"

GLBox::GLBox(QObject *parent) :
    GLBox(1.0, 1.0, 1.0, parent)
{
    //see below.
}

GLBox::GLBox(float size, QObject *parent) :
    GLBox(size, size, size, parent)
{
    //see below.
}

GLBox::GLBox(float width, float height, float length, QObject *parent) :
    GLObject(parent)
{
    /* Technically only 8 vertexes are needed, but to ensure
     * the cube still has sharp edges when smooth shading is
     * enabled each face must not share its verticies with
     * any other, so 24 vertices total are used. */
    //front face corners
    vertices.push_back({width/2, height/2, length/2}); //top-right
    vertices.push_back({-width/2, height/2, length/2}); //top-left
    vertices.push_back({-width/2, -height/2, length/2}); //bottom-left
    vertices.push_back({width/2, -height/2, length/2}); //bottom-right
    normals.push_back({0.f, 0.f, 1.f});
    normals.push_back({0.f, 0.f, 1.f});
    normals.push_back({0.f, 0.f, 1.f});
    normals.push_back({0.f, 0.f, 1.f});
    //back face corners
    vertices.push_back({width/2, height/2, -length/2}); //top-right
    vertices.push_back({width/2, -height/2, -length/2}); //bottom-right
    vertices.push_back({-width/2, -height/2, -length/2}); //bottom-left
    vertices.push_back({-width/2, height/2, -length/2}); //top-left
    normals.push_back({0.f, 0.f, -1.f});
    normals.push_back({0.f, 0.f, -1.f});
    normals.push_back({0.f, 0.f, -1.f});
    normals.push_back({0.f, 0.f, -1.f});
    //top face corners
    vertices.push_back({width/2, height/2, length/2}); //front-right
    vertices.push_back({width/2, height/2, -length/2}); //back-right
    vertices.push_back({-width/2, height/2, -length/2}); //back-left
    vertices.push_back({-width/2, height/2, length/2}); //front-left
    normals.push_back({0.f, 1.f, 0.f});
    normals.push_back({0.f, 1.f, 0.f});
    normals.push_back({0.f, 1.f, 0.f});
    normals.push_back({0.f, 1.f, 0.f});
    //bottom face corners
    vertices.push_back({width/2, -height/2, length/2}); //front-right
    vertices.push_back({-width/2, -height/2, length/2}); //front-left
    vertices.push_back({-width/2, -height/2, -length/2}); //back-left
    vertices.push_back({width/2, -height/2, -length/2}); //back-right
    normals.push_back({0.f, -1.f, 0.f});
    normals.push_back({0.f, -1.f, 0.f});
    normals.push_back({0.f, -1.f, 0.f});
    normals.push_back({0.f, -1.f, 0.f});
    //left face corners
    vertices.push_back({-width/2, height/2, length/2}); //top-front
    vertices.push_back({-width/2, height/2, -length/2}); //top-back
    vertices.push_back({-width/2, -height/2, -length/2}); //bottom-back
    vertices.push_back({-width/2, -height/2, length/2}); //bottom-front
    normals.push_back({-1.f, 0.f, 0.f});
    normals.push_back({-1.f, 0.f, 0.f});
    normals.push_back({-1.f, 0.f, 0.f});
    normals.push_back({-1.f, 0.f, 0.f});
    //right face corners
    vertices.push_back({width/2, height/2, length/2}); //top-front
    vertices.push_back({width/2, -height/2, length/2}); //bottom-front
    vertices.push_back({width/2, -height/2, -length/2}); //bottom-back
    vertices.push_back({width/2, height/2, -length/2}); //top-back
    normals.push_back({1.f, 0.f, 0.f});
    normals.push_back({1.f, 0.f, 0.f});
    normals.push_back({1.f, 0.f, 0.f});
    normals.push_back({1.f, 0.f, 0.f});

    /* Specifying vertex draw order */
    //Front face
    vertIndexList.push_back(0);
    vertIndexList.push_back(1);
    vertIndexList.push_back(2);

    vertIndexList.push_back(2);
    vertIndexList.push_back(3);
    vertIndexList.push_back(0);

    //Back face
    vertIndexList.push_back(4);
    vertIndexList.push_back(5);
    vertIndexList.push_back(6);

    vertIndexList.push_back(6);
    vertIndexList.push_back(7);
    vertIndexList.push_back(4);

    //Top face
    vertIndexList.push_back(8);
    vertIndexList.push_back(9);
    vertIndexList.push_back(10);

    vertIndexList.push_back(10);
    vertIndexList.push_back(11);
    vertIndexList.push_back(8);

    //Bottom face
    vertIndexList.push_back(12);
    vertIndexList.push_back(13);
    vertIndexList.push_back(14);

    vertIndexList.push_back(14);
    vertIndexList.push_back(15);
    vertIndexList.push_back(12);

    //Left face
    vertIndexList.push_back(16);
    vertIndexList.push_back(17);
    vertIndexList.push_back(18);

    vertIndexList.push_back(18);
    vertIndexList.push_back(19);
    vertIndexList.push_back(16);

    //Right face
    vertIndexList.push_back(20);
    vertIndexList.push_back(21);
    vertIndexList.push_back(22);

    vertIndexList.push_back(22);
    vertIndexList.push_back(23);
    vertIndexList.push_back(20);
    setColor(1.0, 1.0, 1.0, 1.0); //no tinting by default
}
