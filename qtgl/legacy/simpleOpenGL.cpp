#include "simpleOpenGL.h"


SimpleOpenGL::SimpleOpenGL(QWidget *parent) :
    QOpenGLWidget(parent)
//    QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{

}

SimpleOpenGL::~SimpleOpenGL()
{
}



void SimpleOpenGL::initializeGL()
{

    initializeOpenGLFunctions();

    // Dark blue background
    glClearColor(0.2f, 0.1f, 0.3f, 0.0f);

//    qglClearColor(Qt::black);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);




}

void SimpleOpenGL::paintGL(){

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    //glClearColor(0.1f, 0.1f, 0.3f, 0.0f);

}

void SimpleOpenGL::resizeGL(int w, int h){
    glViewport(0, 0, w, h);

}
