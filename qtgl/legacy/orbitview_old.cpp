//#include "sphere.h"
#include "orbitview.h"
#include "kepler.hpp"
#include "timelib.h"

// compute the ECEF frame from a delta time
//
void Sphere::computeFrameEcefDelta(double dt){
    // compute ECEF
    Vector3f geocentricFrameX;
    Vector3f geocentricFrameY;
    Vector3f geocentricFrameZ;

    geocentricFrameX << 1., 0., 0.;
    geocentricFrameY << 0., 1., 0.;
    geocentricFrameZ << 0., 0., 1.;

    Vector X, Y, Z;

    // compute the angle that has elapsed from dt
    // if the ECEF wabbles this will not be very accurate
    // but it's an approximation
    double earth_omega = 2.*M_PI/(24.*60.*60.);
    double angle = earth_omega*dt;

    // compute the new vectors using matlib
    rvector test;

    // compute the new vectors using Eigen
    Vector3d vec;

    Matrix3f m;
    m = AngleAxisf((float)angle, Vector3f::UnitZ());

    Vector3f newFrameX;
    Vector3f newFrameY;
    Vector3f newFrameZ;

    newFrameX = m*geocentricFrameX;
    newFrameY = m*geocentricFrameY;
    newFrameZ = m*geocentricFrameZ;

    X.x = newFrameX[0];
    X.y = newFrameX[1];
    X.z = newFrameX[2];

    Y.x = newFrameY[0];
    Y.y = newFrameY[1];
    Y.z = newFrameY[2];

    Z.x = newFrameZ[0];
    Z.y = newFrameZ[1];
    Z.z = newFrameZ[2];

    //qDebug() << X.x << " " << X.y << " " << X.z;

    frameECEF.setReferenceFrame(X,Y,Z,sphereRadius*2.1);

    // compare geocentric X vector with inertial X [1,0,0]
    // dot product is proportional to the cosine of the angle,
    // the determinant is proprortional to its sine
    // => angle = atan2(sin, cos)

    float dot = 1.0*X.x + 0.0*X.y;      // dot product
    float det = 1.0*X.y - 0.0*X.x;      // determinant
    angleRotateSphere = atan2(det, dot)*180./M_PI;  // atan2(y, x) or atan2(sin, cos)
    angleRotateSphere = 180 + angleRotateSphere; // 180 deg offset because of the texture
}

Vector3d Sphere::convertMagField2ECEF(){

    float x = magField.col[1]; // E
    float y = magField.col[0]; // N
    float z = -magField.col[2]; // -D

    //    locstruc loc;
    //    loc.pos.geod.s = magFieldPosGeodetic;
    //    pos_geod2geoc(&loc);

    //    magFieldPosGeocentric.x = loc.pos.geoc.s.col[0];
    //    magFieldPosGeocentric.y = loc.pos.geoc.s.col[1];
    //    magFieldPosGeocentric.z = loc.pos.geoc.s.col[2];

    Matrix3d mx;
    float lat = magFieldPosGeodetic.lat;
    float lon = magFieldPosGeodetic.lon;
    mx << -sin(lon), -sin(lat) * cos(lon), cos(lat) * cos(lon),
            cos(lon), -sin(lat) * sin(lon), cos(lat) * sin(lon),
            0, cos(lat), sin(lat);

    Vector3d enu, geo;
    enu << x,y,z;
    geo << magFieldPosGeocentric.x, magFieldPosGeocentric.y, magFieldPosGeocentric.z;

    Vector3d b_ecef = mx * enu ; // + geo
    return b_ecef;
}



Sphere::Sphere(QWidget *parent) :
    QOpenGLWidget(parent),
    vboIndices(QOpenGLBuffer::IndexBuffer),
    texture(0),
    //eye(5,5,2),
    center(0,0,0),
    unitX(1,0,0),
    unitZ(0,0,1),
    m_frameCount(0),
    mjdStart(57135.9528441667),
    animationStep(0),
    animationtStepOffset(0)
{

    sphereRadius = 6000e3;
    eyeDistance = sphereRadius*5;
    //eye = QVector3D(30,30,2);

    //rotation = QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), 45);

    //    eye = QVector3D(5,0,0);
    //vMatrix.lookAt(eye,center,unitZ);
    //    MVP.translate(0.0, 0.0, -10.0);
    //    MVP.rotate(rotation);

    //    QVector3D test;
    //    test.normalize(); // in place, changes the source vector
    //    QVector3D::normalized(); // returns normalized vector

    //    glm::vec3 test1;
    //    glm::normalize(test1);

    //    Eigen::Vector3f test2;
    //    test2.norm();

    //--------------------------------------------------------------
    QTimer *timer = new QTimer(this);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(10);


    // timer for animation
    timerAnimation = new QTimer(this);
    connect(timerAnimation, SIGNAL(timeout()), this, SLOT(updateAnimationStep()));

    //
    // set simulation Time
    simTimeMjd = gregorianToModJulianDate(2015,5,1, 0,10,0);
    simTimeDt = 0;

    //    string resourcesPath = "C:/COSMOS/resources";
#ifdef COSMOS_MAC_OS
    string resourcesPath = "/Applications/COSMOS/resources";
    setEnvCosmosResources(resourcesPath);
#endif

    // magfield
    magFieldPosGeodetic.h = 500e3;
    magFieldPosGeodetic.lat = 0;
    magFieldPosGeodetic.lon = 0;

    locstruc loc;
    loc.pos.geod.s = magFieldPosGeodetic;
    pos_geod2geoc(&loc);
    magFieldPosGeocentric.x = loc.pos.geoc.s.col[0];
    magFieldPosGeocentric.y = loc.pos.geoc.s.col[1];
    magFieldPosGeocentric.z = loc.pos.geoc.s.col[2];


    int iretn = geomag_front(magFieldPosGeodetic, mjd2year(simTimeMjd),&magField0);

    if (iretn == 0){
        //cout << "mag field 0 [nT]: "
         //    << magField0.col[0]*1e9 << ", "
          //   << magField0.col[1]*1e9 << ", "
           //  << magField0.col[2]*1e9 << endl;
    } else {
        //cout << "mag field error" << endl;
    }

    //    // concert NED to ECEF
    //    float slat = sin(magFieldPosGeodetic.lat); //theta
    //    float clat = cos(magFieldPosGeodetic.lat); //theta
    //    float slon = sin(magFieldPosGeodetic.lon); //phi
    //    float clon = cos(magFieldPosGeodetic.lon); //phi

    //    Matrix3d mx;
    //    mx << -slat*clon, -slon, -clat*clon,
    //          -slat*slon, clon, -clat*slon,
    //                clat, 0, -slat;

    //    Vector3d ned;
    //    ned << magField0.col[0], magField0.col[1], magField0.col[1];
    //    Vector3d b_ecef = mx * ned;
    Vector3d b_ecef = convertMagField2ECEF();
    //cout << b_ecef << endl;


    //    // compute all the mag vectors
    //    int i = 0;
    //    for (int lat=-90; lat <= 90; lat = lat+5){

    //        for (int lon=0; lon <= 360; lon = lon+15){

    //            // magfield geodetic position
    //            magFieldPosGeodetic.h = 500e3;

    //            magFieldPosGeodetic.lat = lat*M_PI/180.;
    //            magFieldPosGeodetic.lon = lon*M_PI/180.;

    //            locstruc loc;
    //            loc.pos.geod.s = magFieldPosGeodetic;
    //            pos_geod2geoc(&loc);
    //            magFieldPosGeocentric.x = loc.pos.geoc.s.col[0];
    //            magFieldPosGeocentric.y = loc.pos.geoc.s.col[1];
    //            magFieldPosGeocentric.z = loc.pos.geoc.s.col[2];

    //            int iretn = geomag_front(magFieldPosGeodetic, mjd2year(simTimeMjd), &magField);

    //            Vector3d b_ecef = convertMagField2ECEF();

    //            i++;

    //            glMagFieldVector.setLength(sphereRadius/8.);
    //            glMagFieldVector.setOrigin(magFieldPosGeocentric);
    //            //        glMagFieldVector.setVectorXYZ(magField.col[0], magField.col[1], magField.col[2]);
    //            glMagFieldVector.setVector(b_ecef[0], b_ecef[1], b_ecef[2]);
    //            glMagFieldVector.setup();
    //            //            glMagFieldVector.setupVbo();

    //            glMagFieldVectorS.push_back(glMagFieldVector);
    //            //        drawVector();
    //        }

    //    }

    //    computeFrameECEF(simTimeMjd);

    //    // Time
    //    time_t t_utc = time(nullptr);
    //    struct tm * time_utc;
    //    time_utc = gmtime(&t_utc);
    //    cout << "UTC: "
    //         <<  1900 + time_utc->tm_year << "-"
    //          <<  time_utc->tm_mon << "-"
    //           <<  time_utc->tm_mday << " "
    //            <<  time_utc->tm_hour << ":"
    //             <<  time_utc->tm_min << ":"
    //              <<  time_utc->tm_sec << ""
    //               << endl;


    //    time_t t_local = time(nullptr);
    //    struct tm * time_local;
    //    time_local = localtime (&t_local);
    //    printf ( "The current date/time is: %s", asctime (time_local) );
    //    cout << "Local: "
    //         <<  1900 + time_local->tm_year << "-"
    //          <<  time_local->tm_mon + 1 << "-"
    //           <<  time_local->tm_mday << " "
    //            <<  time_local->tm_hour << ":"
    //             <<  time_local->tm_min << ":"
    //              <<  time_local->tm_sec << ""
    //               << endl;

    //    char buffer [80];
    //    strftime (buffer,80,"Now it's %I:%M%p. Zone: %z %Z",time_local);
    //    cout << buffer << _timezone << endl;

    //    _timezone

    //    cout << "UTC: " << put_time(gmtime(&t),"%c %Z") << endl;
    //    cout << "local: " << put_time(localtime(&t),"%c %Z") << endl;

    //    mjdStart = simTimeMjd;
    //    mjdStart = 57135.9528441667;


    // compute orbit trajectory
    Vector3d v_r; // position vector
    Vector3d v_v; // velocity vector

    // launch conditions for 1 May 2015 00:00:00.000 (from STK)
    // position and velocity vectors in ICRF
    v_r << 3697.8265113729863000, 5687.9983496950472000, -568.7505552110469600;
    v_v << 0.1932431121646113, -0.8874281806205852, -7.6186484283010820;

    // launch conditions for 1 May 2015 00:00:00.000 (from STK)
    // position and velocity vectors in ECEF -> pseudo intertial
    v_r << -6520.3856599118853000, -1875.5706668704402000, -563.4969816368675300;
    v_v << 0.2900946508525095, 1.2803388570418119, -7.6183105593596760;

    CartesianVector cv_initial;
    CartesianVector cv_final;

    cv_initial.r = v_r;
    cv_initial.v = v_v;

    Kepler kepler;

    orbit_t.reserve(120000);
    orbit_x.reserve(120000);
    orbit_y.reserve(120000);
    orbit_z.reserve(120000);

    orbit_t.append(0);
    orbit_x.append(cv_initial.r[0]) ; //
    orbit_y.append(cv_initial.r[1]) ; //
    orbit_z.append(cv_initial.r[2]) ; //

    for (int i=0; i<70000; i++){
//    for (int i=0; i<6000; i++){
        kepler.KeplerCOE(cv_initial,i,cv_final);

        //        orbit_t[i] = i;
        orbit_t.append(i+1);

        //        orbit_x[i] = cv_final.r[0]; //
        //        orbit_y[i] = cv_final.r[1]; //
        //        orbit_z[i] = cv_final.r[2]; //
        orbit_x.append(cv_final.r[0]) ; //
        orbit_y.append(cv_final.r[1]) ; //
        orbit_z.append(cv_final.r[2]) ; //

        //        orbit_distance[i] = cv_final.r.norm();
        //        orbit_speed[i] = cv_final.v.norm();

        //        cv_initial.r = cv_final.r;
        //        cv_initial.v = cv_final.v;

    }
    //cout << cv_final.r << endl << endl;
    //cout << cv_final.v << endl;



} // end of constructor

Sphere::~Sphere()
{
}


void Sphere::initializeGL()
{

    initializeOpenGLFunctions();

    // Dark blue background
    glClearColor(0.1f, 0.1f, 0.3f, 0.0f);

    initShaders(&shTexture,
                ":/source/gl/shaders/texture.vsh",
                ":/source/gl/shaders/texture.fsh");
    initShaders(&shColor,
                ":/source/gl/shaders/color.vsh",
                ":/source/gl/shaders/color.fsh");

    initTextures();

    // Vertex Array Object
    //m_vao = new QOpenGLVertexArrayObject(this);
    vao.create();

    // define geometry and create VBOs
    makeSphere();

    //InitializeVertexBuffer();

    //printOpenGLinfo();

    // Inertial Frame
    // this is now pseudoInertialFrame
    frameECI.setReferenceFrameInertial(sphereRadius*2);
    frameECI.setupVbo();

    // Earth Centered Earth Fixed frame
    //    computeFrameECEF(simTimeMjd);
    //    computeFrameECEF(mjdStart);
    //    frameECEF.setupVbo();
    fastForward = 0.;

    // Mag Vector
    glMagFieldVector.setLength(sphereRadius/2.);
    glMagFieldVector.setOrigin(magFieldPosGeocentric);
    glMagFieldVector.setVector(magField.col[0], magField.col[1], magField.col[2]);
    glMagFieldVector.setup();
    glMagFieldVector.setupVbo();

    // Orbit Line
    // trajectory line for cubesat1
    //    float orbitRadius = sphereRadius*2;
    //    Vertex vert = {orbitRadius, 0, 0};
    //    orbitLine.addVertex( vert , {1.0f, 0.0f, 0.0f});

    Vertex vert = {float(orbit_x.at(0)*1e3),
                   float(orbit_y.at(0)*1e3),
                   float(orbit_z.at(0)*1e3)};
    orbitLine.addVertex( vert , {1.0f, 0.0f, 0.0f});

    for (int i=0; i<orbit_x.size(); i++){
        //    for (int i=0; i<360*1.1; i++){
        //        float x = (float)orbitRadius*cos(i*3.14/180.0);
        //        float y = (float)orbitRadius*sin(i*3.14/180.0);
        //        float z = 0;

        float x = orbit_x.at(i)*1e3;
        float y = orbit_y.at(i)*1e3;
        float z = orbit_z.at(i)*1e3;

        vert = {x,y,z};
        orbitLine.addVertex( vert , {1.0f, 1.0f, 0.0f});
        orbitLine.addVertex( vert , {1.0f, 1.0f, 0.0f});
    }
    orbitLine.setupVbo();


    // satellite object

    //    satelliteCube = new Cube(1e3);
    satelliteCube.setSize(100e3);
    satelliteCube.setColor({0.5,0,0.5});
    satelliteCube.setupVbo();


}

void Sphere::paintGL(){
    vao.bind();
    //    glClearColor(0.0f, 0.0f, 0.0f, 0.0f );

    glEnable(GL_DEPTH_TEST); // Enable depth buffer
    glEnable(GL_CULL_FACE); // Enable back face culling

    // Clear the screen
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    //    glClear( GL_DEPTH_BUFFER_BIT );

    // compute MVP
    QMatrix4x4 cameraTransformation;
    cameraTransformation.rotate(alpha, 0, 0, 1);
    cameraTransformation.rotate(beta, 0, 1, 0);
    QVector3D cameraPosition = cameraTransformation * QVector3D(eyeDistance, 0, 0);
    QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 0, 1);

    vMatrix.setToIdentity();
    vMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);


    //    qDebug() << "cameraPosition = " << cameraPosition;
    //    qDebug() << "cameraUpDirection = " << cameraUpDirection;

    MVP = pMatrix * vMatrix * mMatrix;


    //    qDebug() << "pMatrix = " << pMatrix;
    //    qDebug() << "vMatrix = " << vMatrix;
    //    qDebug() << "mMatrix = " << mMatrix;

    // ----------------------
    // Draw 3D Earth
    // Set modelview-projection matrix

    mMatrix_sphere.setToIdentity();
    mMatrix_sphere.rotate(angleRotateSphere,unitZ); // 180 deg offset
    MVP_sphere = pMatrix * vMatrix * mMatrix_sphere;

    shTexture.bind();
    shTexture.setUniformValue("matrix_MVP", MVP_sphere); //pMatrix * MVP

    texture->bind(); // Tell OpenGL which VBO to use
    shTexture.setUniformValue("texture", 0); // Use texture unit 0

    vboTexture.bind(); // Tell OpenGL which VBO to use
    shTexture.enableAttributeArray("a_texcoord");
    shTexture.setAttributeArray("a_texcoord", GL_FLOAT, 0, 2);
    vboTexture.release();

    vboPosition.bind(); // Tell OpenGL which VBO to use
    shTexture.enableAttributeArray("vertexPosition");
    shTexture.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    vboPosition.release();


    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices.bufferId());
    vboIndices.bind();

    // draw it finally!
    glDrawElements( GL_TRIANGLES, indices.count(), GL_UNSIGNED_INT, NULL );

    vboIndices.release();
    //    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE ); // only draw lines

    shTexture.disableAttributeArray("a_texcoord");
    shTexture.disableAttributeArray("vertexPosition");

    shTexture.release();


    // ----------------------
    // Draw Cartesian Frame ECI (Earth Centered Inertial)
    //draw(&m_shaderProgramSimple, frameInertial);
    shColor.bind();
    shColor.setUniformValue("matrix_MVP", MVP);

    frameECI.vbo.vertex.bind();
    shColor.enableAttributeArray("vertexPosition");
    shColor.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    frameECI.vbo.vertex.release();

    frameECI.vbo.color.bind();
    shColor.enableAttributeArray("vertexColor");
    shColor.setAttributeBuffer("vertexColor", GL_FLOAT, 0, 3);
    frameECI.vbo.color.release();

    glDrawArrays(GL_TRIANGLES, 0, frameECI.vertices.size());

    shColor.disableAttributeArray("vertexColor");
    shColor.disableAttributeArray("vertexPosition");

    shColor.release();


    // ----------------------
    // Draw Cartesian Frame ECEF

    //    fastForward += 0.0011;
    //    computeFrameECEF(simTimeMjd+fastForward);
    //    computeFrameECEF(currentmjd());
    computeFrameEcefDelta(simTimeDt);
    frameECEF.setupVbo();

    shColor.bind();
    shColor.setUniformValue("matrix_MVP", MVP);

    frameECEF.vbo.vertex.bind();
    shColor.enableAttributeArray("vertexPosition");
    shColor.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    frameECEF.vbo.vertex.release();

    frameECEF.vbo.color.bind();
    shColor.enableAttributeArray("vertexColor");
    shColor.setAttributeBuffer("vertexColor", GL_FLOAT, 0, 3);
    frameECEF.vbo.color.release();

    glDrawArrays(GL_TRIANGLES, 0, frameECEF.vertices.size());

    shColor.disableAttributeArray("vertexColor");
    shColor.disableAttributeArray("vertexPosition");

    shColor.release();

    // delete previous data
    frameECEF.vertices.clear();
    frameECEF.colors.clear();
    //    frameECEF.vbo.vertex.release();
    //    frameECEF.vbo.color.release();

    // ----------------------
    // Draw Mag Vector


    // magfield geodetic position
    magFieldPosGeodetic.h = 500e3;
    magFieldPosGeodetic.lat = 0*M_PI/180.;
    magFieldPosGeodetic.lon = 0*M_PI/180.;

    locstruc loc;
    loc.pos.geod.s = magFieldPosGeodetic;
    pos_geod2geoc(&loc);
    magFieldPosGeocentric.x = loc.pos.geoc.s.col[0];
    magFieldPosGeocentric.y = loc.pos.geoc.s.col[1];
    magFieldPosGeocentric.z = loc.pos.geoc.s.col[2];

    int iretn = geomag_front(magFieldPosGeodetic, mjd2year(currentmjd()), &magField);

    Vector3d b_ecef = convertMagField2ECEF();

    glMagFieldVector.setLength(sphereRadius/8.);
    glMagFieldVector.setOrigin(magFieldPosGeocentric);
    //        glMagFieldVector.setVectorXYZ(magField.col[0], magField.col[1], magField.col[2]);
    glMagFieldVector.setVector(b_ecef[0], b_ecef[1], b_ecef[2]);
    glMagFieldVector.setup();
    glMagFieldVector.setupVbo();

    drawMagVector(glMagFieldVector);

    //    // ----------------------
    //    // Draw Mag field of the Earth
    //    int i = 0;
    //    for (int lat=-90; lat <= 90; lat = lat+5){

    //        for (int lon=0; lon <= 360; lon = lon+15){

    //            drawMagVector(glMagFieldVectorS.at(i));
    //            i ++;
    //        }

    //    }


    // ----------------------
    // Draw Orbit Line

    shColor.bind();
    shColor.setUniformValue("matrix_MVP", MVP);

    orbitLine.vbo.vertex.bind();
    shColor.enableAttributeArray("vertexPosition");
    shColor.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    orbitLine.vbo.vertex.release();

    orbitLine.vbo.color.bind();
    shColor.enableAttributeArray("vertexColor");
    shColor.setAttributeBuffer("vertexColor", GL_FLOAT, 0, 3);
    orbitLine.vbo.color.release();

    glDrawArrays(GL_LINES, 0, orbitLine.vertices.size());

    shColor.disableAttributeArray("vertexColor");
    shColor.disableAttributeArray("vertexPosition");

    shColor.release();


    // ----------------------
    // Draw Satellite

    mMatrixSatellite.setToIdentity();
//    checkAnimationStep(); // to see if it' within range
//    mMatrixSatellite.rotate(angleRotateSphere,unitZ); // 180 deg offset
    mMatrixSatellite.translate(orbit_x.at(animationStep)*1e3,
                               orbit_y.at(animationStep)*1e3,
                               orbit_z.at(animationStep)*1e3);

    MVP_Satellite = pMatrix * vMatrix * mMatrixSatellite;

    shColor.bind();
    shColor.setUniformValue("matrix_MVP", MVP_Satellite);

    satelliteCube.vbo.vertex.bind();
    shColor.enableAttributeArray("vertexPosition");
    shColor.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    satelliteCube.vbo.vertex.release();

    satelliteCube.vbo.color.bind();
    shColor.enableAttributeArray("vertexColor");
    shColor.setAttributeBuffer("vertexColor", GL_FLOAT, 0, 3);
    satelliteCube.vbo.color.release();

    glDrawArrays(GL_TRIANGLES, 0, satelliteCube.vertices.size());

    shColor.disableAttributeArray("vertexColor");
    shColor.disableAttributeArray("vertexPosition");

    shColor.release();




    // ----------------------
    // Release VAO
    vao.release();


}

void Sphere::drawMagVector(glVector vec)
{
    //    shColor.bind();
    //    shColor.setUniformValue("matrix_MVP", MVP);

    //    glMagFieldVector.vbo.vertex.bind();
    //    shColor.enableAttributeArray("vertexPosition");
    //    shColor.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    //    glMagFieldVector.vbo.vertex.release();

    //    glMagFieldVector.vbo.color.bind();
    //    shColor.enableAttributeArray("vertexColor");
    //    shColor.setAttributeBuffer("vertexColor", GL_FLOAT, 0, 3);
    //    glMagFieldVector.vbo.color.release();

    //    glDrawArrays(GL_TRIANGLES, 0, glMagFieldVector.vertices.size());

    //    shColor.disableAttributeArray("vertexColor");
    //    shColor.disableAttributeArray("vertexPosition");

    //    shColor.release();

    vec.setupVbo();

    shColor.bind();
    shColor.setUniformValue("matrix_MVP", MVP);

    vec.vbo.vertex.bind();
    shColor.enableAttributeArray("vertexPosition");
    shColor.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    vec.vbo.vertex.release();

    vec.vbo.color.bind();
    shColor.enableAttributeArray("vertexColor");
    shColor.setAttributeBuffer("vertexColor", GL_FLOAT, 0, 3);
    vec.vbo.color.release();

    glDrawArrays(GL_TRIANGLES, 0, vec.vertices.size());

    shColor.disableAttributeArray("vertexColor");
    shColor.disableAttributeArray("vertexPosition");

    shColor.release();


}

void Sphere::paintEvent(QPaintEvent * event){
    //void Sphere::paintGL(){
    //cout << "hi" << endl;

    // count frames per second
    double fps = 0;
    if (m_frameCount == 0) {
        m_time.start();
    } else {
        //        fps = m_time.elapsed() / float(m_frameCount);
        fps = m_frameCount / (float(m_time.elapsed()) / 1000.0f);
        //        printf("FPS is %f ms\n",fps);
    }
    m_frameCount++;


    makeCurrent();


    paintGL();

    glDisable (GL_DEPTH_TEST);

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    //    p.begin(this);
    //        glClearColor(0.0,0.0,0.0,0.0);
    //        glClear(GL_COLOR_BUFFER_BIT); // << If I don't do this it clears out my viewer white
    //        p.setBackgroundMode(Qt::TransparentMode); // < was a test, doesn't seem to do anything
    //        p.setBackground(QColor(0,0,0,0));
    p.setPen(Qt::yellow);
    float fontSize = 10;
    p.setFont(QFont("Arial", fontSize));
    float startTextY = 20;
    float fontOffsetY = fontSize+5;

    //    // Time
    //    time_t t = time(nullptr);
    //    cout << "UTC: " << put_time(gmtime(&t),"%c %Z") << endl;
    //    cout << "UTC: " << put_time(gmtime(&t),"%c %Z") << endl;

//    p.drawText(10,startTextY+fontSize*0,"UTC: " + QString::fromStdString(mjdToGregorian(currentmjd()))
//               + " [mjd]: " + QString::number(currentmjd(),'f',6));

    // top left
    p.drawText(10,startTextY+fontOffsetY*0,"Current Time");
    p.drawText(10,startTextY+fontOffsetY*1,"UTC: " + QString::fromStdString(mjdToGregorian(currentmjd() )));
    p.drawText(10,startTextY+fontOffsetY*2,"MJD: " + QString::number(currentmjd(),'f',6));

    p.drawText(10,startTextY+fontOffsetY*5,"Simulation Time");
    p.drawText(10,startTextY+fontOffsetY*6,"UTC: " + QString::fromStdString(mjdToGregorian(simTimeMjd)));
    p.drawText(10,startTextY+fontOffsetY*7,"MJD: " + QString::number(simTimeMjd,'f',6));

    p.drawText(10,startTextY+fontOffsetY*10,"Mag x [nT]: " + QString::number(magField0.col[0]*1e9));
    p.drawText(10,startTextY+fontOffsetY*11,"Mag y [nT]: " + QString::number(magField0.col[1]*1e9));
    p.drawText(10,startTextY+fontOffsetY*12,"Mag z [nT]: " + QString::number(magField0.col[2]*1e9));

    //    p.drawText(rect(), Qt::AlignLeft, "Mag x: " + QString::number(magField.col[0]));
    //    p.drawText(rect(), Qt::AlignLeft, "Mag y: " + QString::number(magField.col[0]));
    p.drawText(rect(), Qt::AlignRight, "fps: " + QString::number(fps,'f',2));
    p.drawText(this->width()-200, 30, "eye dist [km]: " + QString::number(eyeDistance/1e3,'f',0));


    QRectF rectangle(10.0, 20.0, 80.0, 60.0);
    p.drawEllipse(rectangle);
    p.end();

    doneCurrent();


    //    update();

}

void Sphere::resizeGL(int w, int h){
    //glViewport(0, 0, w, h);

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    //const qreal zNear = 3.0, zFar = 7.0, fov = 45.0;
    const qreal zNear = 100e3, zFar = 1000000e3, fov = 45.0;

    // Reset projection
    pMatrix.setToIdentity();

    // Set perspective projection
    pMatrix.perspective(fov, aspect, zNear, zFar);

}



void Sphere::initShaders(QOpenGLShaderProgram *shaderProgram,
                         QString vertexShaderFile,
                         QString fragmentShaderFile)
{

    // Compile vertex shader
    if (!shaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile)){
        //QMessageBox::warning(this, "QOpenGLShader::Vertex", "QOpenGLShader::Vertex" + program.log());
        qCritical() << QObject::tr("Could not compile vertex shader. Log: ") << shaderProgram->log();
        close();
    }

    // Compile fragment shader
    if (!shaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile)){
        //QMessageBox::warning(this, "QOpenGLShader::Fragment", "QOpenGLShader::Fragment" + program.log());
        qCritical() << QObject::tr("Could not compile fragment shader. Log: ") << shaderProgram->log();
        close();
    }

    // Link shader pipeline
    if (!shaderProgram->link()){
        qCritical() << QObject::tr("Could not link shader program. Log: ") << shaderProgram->log();
        close();
    }

    // Bind shader pipeline for use
    // equivalent to use glUseProgram(programID);
    if (!shaderProgram->bind())
        close();

}


void Sphere::initTextures()
{
    texture = new QOpenGLTexture(QImage(":/resources/images/earth/EarthBasic.bmp"));

    //    // Set nearest filtering mode for texture minification
    //    texture->setMinificationFilter(QOpenGLTexture::Nearest);

    //    // Set bilinear filtering mode for texture magnification
    //    texture->setMagnificationFilter(QOpenGLTexture::Linear);

    //    // Wrap texture coordinates by repeating
    //    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
    //    texture->setWrapMode(QOpenGLTexture::Repeat);
}

void Sphere::makeSphere(){

    int stacks = 32;
    int slices = 32;
    //float radius = 2;

    const float pi = 3.1415926535897932384626433832795f;
    const float _2pi = 2.0f * pi;

    for( int i = 0; i <= stacks; ++i )
    {
        // V texture coordinate.
        float V = i / (float)stacks;
        float phi = V * pi;

        for ( int j = 0; j <= slices; ++j )
        {
            // U texture coordinate.
            float U = j / (float)slices;
            float theta = U * _2pi;

            float X = cos(theta) * sin(phi);
            float Y = sin(theta) * sin(phi);
            float Z = cos(phi); // principal axis

            positions.push_back( QVector3D( X, Y, Z) * sphereRadius );
            normals.push_back( QVector3D(X, Y, Z) );
            textureCoords.push_back( QVector2D(U, V) );
        }

    }

    // Now generate the index buffer
    // iterate trhough each face of the sphere
    for( int i = 0; i < slices * stacks + slices; ++i )
    {
        // first triangle, upper
        indices.push_back( i );
        indices.push_back( i + slices );
        indices.push_back( i + slices + 1  );


        // second triangle, lower
        indices.push_back( i + slices + 1  );
        indices.push_back( i + 1 );
        indices.push_back( i );

    }

    // setup VBO for position
    vboPosition.create();
    vboPosition.bind();
    //vboPosition.allocate(vertData.constData(), vertData.count() * sizeof(QVector3D));
    vboPosition.allocate(positions.constData(), positions.count() * sizeof(QVector3D));
    vboPosition.release();

    // setup VBO for normals
    vboNormals.create();
    vboNormals.bind();
    vboNormals.allocate(normals.constData(), normals.count() * sizeof(QVector3D));
    vboNormals.release();

    // setup VBO for texture
    vboTexture.create();
    vboTexture.bind();
    vboTexture.allocate(textureCoords.constData(), textureCoords.count() * sizeof(QVector2D));
    vboTexture.release();

    // setup VBO for indices
    vboIndices.create();
    vboIndices.bind();
    vboIndices.allocate(indices.constData(), indices.count() * sizeof(GLfloat));
    vboIndices.release();
}


void Sphere::mousePressEvent(QMouseEvent *event)
{
    //    qDebug() << event->pos();

    //store the mouse pointer’s initial position to be able to
    //track the movement
    mouseLastPos = event->pos();
    //mouseLastPos.setX(event->pos().x());
    //mouseLastPos.setY(event->pos().y());

    //event->accept();

}

void Sphere::mouseMoveEvent(QMouseEvent *event)
{
    //    qDebug() << event->pos();

    QPoint mousePos(event->pos().x(), event->pos().y());

    float delta_x = mousePos.x() - mouseLastPos.x();
    float delta_y = mousePos.y() - mouseLastPos.y();

    // ---------------------------------------------------
    // from Qt OpenGL tutorial, pg 23
    // source: ftp://ftp.informatik.hu-berlin.de/pub/Linux/Qt/QT/developerguides/qtopengltutorial/OpenGLTutorial.pdf


    alpha -= delta_x/5.;
    while (alpha < 0) {
        alpha += 360;
    }
    while (alpha >= 360) {
        alpha -= 360;
    }

    beta -= delta_y/5.;
    if (beta < -90) {
        beta = -90;
    }
    if (beta > 90) {
        beta = 90;
    }

    //    qDebug() << "alpha: " << alpha << "beta: " << beta;

    mouseLastPos = mousePos;

    update();


    //    // trackball motion
    //    // ref: https://www.opengl.org/wiki/Object_Mouse_Trackball

    //    float r = zoomFactor;
    //    float cR = 0;

    //    float x1 = view_x;
    //    float y1 = view_y;
    //    float z1 = 0;

    //    cR = x1*x1 + y1*y1;
    //    if (cR <= r*r/2.0f){
    //        z1 = sqrt(r*r - cR);
    //    } else {
    //        z1 = (r*r/2.0f)/sqrt(cR);
    //    }

    //    float x2 = view_x + (delta_x)/100;
    //    float y2 = view_y + (delta_y)/100;
    //    float z2 = 0;

    //    cR = x2*x2 + y2*y2;
    //    if (cR <= r*r/2.0f){
    //        z2 = sqrt(r*r - (cR));
    //    } else {
    //        z2 = (r*r/2.0f)/sqrt(cR);
    //    }

    //    QVector3D v1(x1,y1,z1);
    //    QVector3D v2(x2,y2,z2);

    //    // >> normalize vector
    //    QVector3D N =  QVector3D::crossProduct(v1,v2);
    //    float dot = QVector3D::dotProduct(v1,v2);
    //    float theta = acos(dot/(v1.length()*v2.length()));
    //    //float theta1 = acos(dot);

    //    N = QVector3D(0,0,-1);
    //    float theta_x =  -(delta_x)/100;
    //    float theta_y =  -(delta_y)/100;
    //    theta = theta_x + theta_y;

    //    QVector3D mouseAxisX(0,0,-1);
    //    QVector3D viewFrom(view_x,view_y,view_z);
    //    QVector3D mouseAxisY = QVector3D::crossProduct(mouseAxisX,viewFrom);
    //    mouseAxisY.normalize();

    //    QVector3D moveX = mouseAxisX*theta_x;
    //    QVector3D moveY = mouseAxisY*theta_y;

    //    QVector3D Nnew = moveX + moveY;


    //    //QQuaternion q = QQuaternion::fromAxisAndAngle(Nnew, -1.4f);

    //    // new

    //    // convert pixel to view position in unitary shpere
    //    QPointF viewLastPos(2.0 * float(mouseLastPos.x()) / width() - 1.0,
    //                        1.0 - 2.0 * float(mouseLastPos.y()) / height());

    //    QPointF viewPos(2.0 * float(mousePos.x()) / width() - 1.0,
    //                    1.0 - 2.0 * float(mousePos.y()) / height());

    //    //    QVector3D lastPos3D = QVector3D(viewLastPos.x(), viewLastPos.y(), 0.0f);
    //    QVector3D lastPos3D = QVector3D(0.0f, viewLastPos.x(), viewLastPos.y());
    //    float sqrZ = 1.0 - QVector3D::dotProduct(lastPos3D, lastPos3D);
    //    if (sqrZ > 0)
    //        //        lastPos3D.setZ(sqrt(sqrZ));
    //        lastPos3D.setX(sqrt(sqrZ));
    //    else
    //        lastPos3D.normalize();

    //    //    QVector3D currentPos3D = QVector3D(viewPos.x(), viewPos.y(), 0.0f);
    //    QVector3D currentPos3D = QVector3D(0.0f, viewPos.x(), viewPos.y());
    //    sqrZ = 1.0 - QVector3D::dotProduct(currentPos3D, currentPos3D);
    //    if (sqrZ > 0)
    //        //currentPos3D.setZ(sqrt(sqrZ));
    //        currentPos3D.setX(sqrt(sqrZ));
    //    else
    //        currentPos3D.normalize();

    //    QVector3D axis;
    //    axis = QVector3D::crossProduct(lastPos3D, currentPos3D);
    //    QQuaternion q = QQuaternion::fromAxisAndAngle(axis, 2);
    //    q.normalize();

    //MVP.rotate(q);

    //    qDebug() << MVP;
    //    MVP.setToIdentity();
    //    eye = QVector3D(5,0,0);
    //    MVP.lookAt(eye, center, unitZ);

    //    mouseLastPos = mouseLastPos;

    //    MVP.setToIdentity();
    //    qDebug() << MVP;
    //    qDebug() << "last pos: " << mouseLastPos << " | curr pos: " << mousePos;
    //    qDebug() << "last pos3D: " << lastPos3D << " | curr pos 3D: " << currentPos3D;

    //    QQuaternion q_test = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), 45);
    //    QMatrix4x4 m_test;
    //    m_test.rotate(q_test);
    //    qDebug() << m_test;

    //    qDebug() << MVP;

    //    QMatrix3x3 m;
    //    q.
    //    viewFrom.;
    //    glm::vec3 v = glm::toMat3(q)*glm::vec3(view_x,view_y,view_z);
    //    view_x = v.x;
    //    view_y = v.y;
    //    view_z = v.z;


}

void Sphere::wheelEvent(QWheelEvent *event)
{

    int delta = event->delta();
    if (event->orientation() == Qt::Vertical) {
        if (delta < 0) {
            eyeDistance *= 1.1;
        } else if (delta > 0) {
            eyeDistance *= 0.9;
        }
        update();
    }
    //    event->accept();

}


void Sphere::updateAnimationStep()
{
    if (animationStep < orbit_x.size() - animationtStepOffset){
        animationStep = animationStep + animationtStepOffset ;
    } else {
        animationStep = orbit_x.size() - 1;
        AnimationStop();
    }

}

void Sphere::AnimationStart(){
    timerAnimation->start(10);
}

void Sphere::AnimationStop(){
    timerAnimation->stop();
}


//void Sphere::AnimationStepCheck(){
//    if (animationStep >= orbit_x.size()){
//        animationStep = orbit_x.size() - 1;
//        AnimationStop();
//    }
//}



