#ifndef Sphere_H
#define Sphere_H
#include "configCosmos.h"


// COSMOS libs

#include "geomag.h"
#include "timelib.h"
#include "datalib.h"

// QtOpenGl
#include <QOpenGLWidget>
#include <QOpenGLContext>
#include <QSurfaceFormat>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

// Other Qt libs
#include <QWindow>
#include <QPainter>
#include <QMouseEvent>
#include <QTimer>
#include <QTime>


#include "Eigen/Core"
#include "Eigen/Dense"
//using Eigen::Matrix3d;
//using Eigen::Vector3d;
using namespace Eigen;


// C++ libs
#include <iostream>
#define _USE_MATH_DEFINES // for M_PI on cmath
#include <math.h>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <chrono>
using namespace std;


#include "gl/glPrimitives.h"
#include "convertlib.h"

class Sphere : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT // must include this if you use Qt signals/slots

public:
    Sphere(QWidget *parent= 0);
    ~Sphere();

    void printOpenGLinfo();


protected:
    // -- main 3 opengl functions
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    //
    void paintEvent(QPaintEvent *event);

    void initShaders(QOpenGLShaderProgram *shaderProgram,
                     QString vertexShaderFile,
                     QString fragmentShaderFile);
    void initTextures();

    void makeSphere();

    //void InitializeVertexBuffer();


private:
    QOpenGLShaderProgram shTexture;
    QOpenGLShaderProgram shColor;

    GLuint vertexbuffer;

    QOpenGLVertexArrayObject vao;
    QOpenGLBuffer vboPosition;
    QOpenGLBuffer vboColor;
    QOpenGLBuffer vboNormals;
    QOpenGLBuffer vboTexture;
    QOpenGLBuffer vboIndices;

    GLuint m_posAttr;
    GLuint m_colAttr;

    QOpenGLTexture *texture;


    QQuaternion rotation;

    QVector<QVector3D> positions;
    QVector<QVector3D> normals;
    QVector<QVector2D> textureCoords;
    QVector<GLuint> indices;

    // mouse interaction
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);


    QPoint mouseLastPos;
    float zoomFactor = 5;

    // initial perspective
    float view_x = 5;
    float view_y = 0;
    float view_z = 0;

    // sphere
    float sphereRadius = 1;

    // model view projection matrix
    QMatrix4x4 MVP;
    QMatrix4x4 MVP_Satellite;

    QMatrix4x4 mMatrix; // model
    QMatrix4x4 pMatrix; // projection
    QMatrix4x4 vMatrix; // view
    //QMatrix4x4 projection;

    QMatrix4x4 MVP_sphere;
    QMatrix4x4 mMatrix_sphere; // model
    QMatrix4x4 mMatrixSatellite; // model maxtrix for satellite
    float angleRotateSphere;

    QVector3D eye;
    QVector3D center;

    QVector3D unitX;
    QVector3D unitZ;

    int counter = 0;

    QTimer *timer;

    // Reference Frame
    ReferenceFrame frameECI;  // Earth Centered Inertial
    ReferenceFrame frameECEF; // Earth Fixed Earth Centered
    double mjdStart;

    // Vectors

    // Orbit Line
    Line orbitLine;

    // Satellite Cube
    Cube satelliteCube;


    // mouse operation
    float alpha = 0;
    float beta = 0;
    float eyeDistance = 5;

    // 2D text
    QPainter painter;

    // Magfield
    rvector magField, magField0;
    gvector magFieldPosGeodetic;
    Vertex  magFieldPosGeocentric;
    Vector3d convertMagField2ECEF();
    glVector glMagFieldVector;
    vector<glVector> glMagFieldVectorS;

    // OpenGL calls to draw mag vector
    void drawMagVector(glVector vec);

    // fps
    QTime m_time;
    int m_frameCount;

    void computeFrameEcefDelta(double dt);
    float fastForward;

    double simTimeMjd;
    double simTimeDt;

// -----------------------------
// animation controls
private:
    QTimer *timerAnimation;
public:
    // animation controls
    int animationStep;
    int animationtStepOffset;
    void AnimationStart();
    void AnimationStop();

private slots:
    void updateAnimationStep();

public:
    // Orbit
    QVector<double> orbit_t;
    QVector<double> orbit_x, orbit_y, orbit_z; // initialize with entries 0..100
    QVector<double> orbit_distance, orbit_speed;

};

#endif // Sphere_H
