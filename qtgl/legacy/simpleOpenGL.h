#ifndef SimpleOpenGL_H
#define SimpleOpenGL_H

#include <QWindow>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
//#include <QGLWidget>

class SimpleOpenGL : public QOpenGLWidget, protected QOpenGLFunctions
//class SimpleOpenGL : public QGLWidget
{
    Q_OBJECT // must include this if you use Qt signals/slots

public:
    SimpleOpenGL(QWidget *parent= 0);
    ~SimpleOpenGL();

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

};

#endif // SimpleOpenGL_H
