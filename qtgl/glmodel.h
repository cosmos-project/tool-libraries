#ifndef GLMODEL_H
#define GLMODEL_H

#include <string>
#include <iostream>
//#include "tiny_obj_loader.h"
#include "tinyobjloader/tiny_obj_loader.h"

#include "globject.h"

class GLModel : public GLObject
{
    Q_OBJECT
public:
    explicit GLModel(QObject *parent = 0);

    void loadFromFile(std::string inputfile);

private:
    // model from tinyobject
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

signals:

public slots:
};

#endif // GLMODEL_H
