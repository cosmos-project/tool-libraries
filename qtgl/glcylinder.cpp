#include "glcylinder.h"
#include <cmath>

GLCylinder::GLCylinder(QObject *parent) :
    GLCylinder(0.2, 1.0, 15, parent)
{
    //see below.
}

GLCylinder::GLCylinder(float radius, float height, QObject *parent) :
    GLCylinder(radius, height, 15, parent)
{
    //see below.
}

GLCylinder::GLCylinder(float radius, float height, uint32_t stacks, QObject *parent) :
    GLObject(parent)
{
    const float pi = 3.1415926535897932384626433832795f;
    const float _2pi = 2.0f * pi;

    vertices.push_back({0.f, 0.f, 0.f});
    normals.push_back({0.f, 0.f, -1.f});
    vertices.push_back({0.f, 0.f, height});
    normals.push_back({0.f, 0.f, 1.f});

    for (size_t i = 0; i<stacks; i++) {
        float theta = (i * _2pi)/stacks;
        float X = cos(theta);
        float Y = sin(theta);
        vertices.push_back({radius*X, radius*Y, 0.f});
        normals.push_back({radius*X, radius*Y, 0.f});
        vertices.push_back({radius*X, radius*Y, height});
        normals.push_back({radius*X, radius*Y, 0.f});
        vertices.push_back({radius*X, radius*Y, 0.f});
        normals.push_back({0.f, 0.f, -1.f});
        vertices.push_back({radius*X, radius*Y, height});
        normals.push_back({0.f, 0.f, 1.f});

        vertIndexList.push_back(2 + (i*4 + 1)%(stacks*4));
        vertIndexList.push_back(2 + (i*4)%(stacks*4));
        vertIndexList.push_back(2 + ((i+1)*4)%(stacks*4));

        vertIndexList.push_back(2 + ((i+1)*4)%(stacks*4));
        vertIndexList.push_back(2 + ((i+1)*4 + 1)%(stacks*4));
        vertIndexList.push_back(2 + (i*4 + 1)%(stacks*4));

        vertIndexList.push_back(2 + (i*4 + 2)%(stacks*4));
        vertIndexList.push_back(0);
        vertIndexList.push_back(2 + ((i+1)*4 + 2)%(stacks*4));

        vertIndexList.push_back(2 + (i*4 + 3)%(stacks*4));
        vertIndexList.push_back(2 + ((i+1)*4 + 3)%(stacks*4));
        vertIndexList.push_back(1);
    }
    setColor(1.0, 1.0, 1.0, 1.0); //no tinting by default
}
