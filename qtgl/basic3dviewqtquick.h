#ifndef BASIC3DVIEWQTQUICK_H
#define BASIC3DVIEWQTQUICK_H

#include "basic3dviewcore.h"

#include <QtQuick/QQuickFramebufferObject>

class Basic3DViewQtQuick : public QQuickFramebufferObject
{
    Q_OBJECT

    Q_PROPERTY(Basic3DViewCore* view READ view)

public:
    explicit Basic3DViewQtQuick(QQuickItem *parent = 0);

    Basic3DViewCore* view() const { return _view; }

    Renderer* createRenderer() const;

    //Fixes the mysterious image flipping, but at the terrible cost of margins no longer working for some reason.
    QSGNode* updatePaintNode(QSGNode *const inOutNode, UpdatePaintNodeData *const inOutData);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    Basic3DViewCore* _view;
};

#endif // BASIC3DVIEWQTQUICK_H
