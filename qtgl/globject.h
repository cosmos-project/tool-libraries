#ifndef GLOBJECT_H
#define GLOBJECT_H

#include <QObject>
#include <QColor>

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>

#include <vector>

class GLObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool drawn READ drawn WRITE setDrawn NOTIFY drawnChanged)

public:
    explicit GLObject(QObject *parent = 0);
    //some of this stuff might be made private eventually

    //Independent object properties
    QMatrix4x4 mMatrix;
    QVector3D objectLightDirection;
    QColor objectLightColor;
    float objectLightIntensity = 0.0;
    std::vector<QVector3D> vertices, normals;
    std::vector<QVector4D> colors;
    std::vector<uint32_t> vertIndexList; //default order of 'vertices' used if empty
    GLenum drawMode = GL_TRIANGLES;
    std::vector<QVector2D> textureCoords;
    bool drawn() const { return _drawn; }
    void setDrawn(bool drawn);

    virtual void setTextureIndex(int idx) { _textureIndex = idx; }
    int textureIndex() { return _textureIndex; } //index of the texture on the view that this object uses (-1 = no texture)
    virtual void setShaderIndex(int idx) { _shaderIndex = idx; }
    int shaderIndex() {return _shaderIndex; } //index of the shader on the view that this object uses (-1 = no shader)
    QOpenGLTexture* _textureReference = NULL; //pointer to the texture this object should use (will be set automatically by Basic3DView)
    QOpenGLShaderProgram* _shaderReference = NULL; //pointer to the shader program this object should use (will be set automatically by Basic3DView)

    //Contingent properties
    //OpenGL Buffers:
    QOpenGLBuffer vertexBuffer;
    QOpenGLBuffer colorBuffer;
    QOpenGLBuffer normalBuffer;
    QOpenGLBuffer indexBuffer;
    QOpenGLBuffer texCoordsBuffer;

    QOpenGLVertexArrayObject vao;

    void setUpBuffers();
    void initializeVAO();

    virtual void setColor(QColor color);
    virtual void setColor(float r, float g, float b, float a);

private:
    int _textureIndex = -1; //index of the texture on the view that this object uses (-1 = no texture)
    int _shaderIndex = -1; //index of the shader on the view that this object uses (-1 = no shader)
    bool _drawn = true;

signals:
    void drawnChanged();

public slots:
};

#endif // GLOBJECT_H
