#include "support/configCosmos.h"
#include "pane.h"
#include "support/jsonlib.h"

#include <qglobal.h>
#include <QtWidgets>

#include <QtScript/QtScript>
#include <QtScriptTools/QtScriptTools>

#include "qtsupport/cosmosdata.h"

extern COSMOSData *COSMOS;

Q_DECLARE_METATYPE(Pane*)

QHash<QString, QString> paneFiles;

QScriptValue createPane(QScriptContext *context, QScriptEngine *engine) {
    QString key = context->callee().property("functionName").toString(); //a function name property must be added when this function is set up!
    QScriptValue parentQSV;
    bool floating = false;
    if (context->argumentCount()<2) {
        parentQSV = engine->globalObject();
        if (context->argumentCount()==1) floating = context->argument(0).toBool();
    } else {
        parentQSV = context->argument(1);
        floating = context->argument(0).toBool();
    }
    if (!parentQSV.isQObject()) return context->throwError("Specified parent is not a QObject.");
    Pane *pan;
    if (key=="Pane") pan = new Pane(floating, qobject_cast<QWidget*>(parentQSV.toQObject())); //Generic Pane
    else pan = new Pane(paneFiles[key], floating, qobject_cast<QWidget*>(parentQSV.toQObject())); //Script controlled Pane
    pan->show();
    return engine->toScriptValue(pan);
}

QScriptValue Pane_toScriptValue(QScriptEngine *engine, Pane* const &pan) {
    QScriptValue obj = engine->toScriptValue((QWidget*)pan);
    return obj;
}

void Pane_fromScriptValue(const QScriptValue &obj, Pane* &pan) {
    pan = (Pane*)obj.toQObject();
}

QScriptValue getNodeData(QScriptContext *context, QScriptEngine *engine) {
    if(context->argumentCount()>0) {
        QString namestring;
        if ((namestring = context->argument(0).toString())!=NULL) {
            QByteArray bytes = namestring.toLocal8Bit();
            std::string tempPointer = bytes.data();
            return engine->toScriptValue(json_get_double(tempPointer,   *COSMOS->meta, *COSMOS->data));
            //^Support for more complex structures should be added in the future
        } else return context->throwError("Argument was not a string");
    } else return context->throwError("No namespace name provided.");
}

Pane::Pane(QString scriptFile, bool floating, QWidget *parent) :
    QWidget(parent)
{
    scriptPath = scriptFile;
    int i = scriptFile.length()-1;
    //?? what is this doing?
    while (i>=0) { //Take all
        if (scriptFile[i]==QChar('/')) {
            paneTypeName = scriptFile.mid(i+1).remove(QRegExp("[^A-Za-z]"));
            paneTypeName.truncate(paneTypeName.length()-2); //get rid of the 'js' from the extension
            scriptFile.truncate(i+1);
            resourcePath = scriptFile;
            break;
        } else i--;
    }
    QDir res(resourcePath);
    engine = new COSMOSScriptEngine();
    //QScriptEngineDebugger debugger;
    //debugger.attachTo(engine); //Uncomment to debug (remember, this will affect every pane)
    engine->implementFullBindings(); //Doing this first allows 'toScriptValue' to use the special methods I made for QObject wrapping.
    QScriptValue thisQSV = engine->toScriptValue((QWidget*)this);
    QScriptValueIterator gbl(engine->globalObject());
    while(gbl.hasNext()) { //copy all the global properties onto the new global object
        gbl.next();
        thisQSV.setProperty(gbl.name(), gbl.value());
    }
    engine->setGlobalObject(thisQSV); //make this widget the global object
    engine->globalObject().setProperty("FloatingPane", floating, QScriptValue::ReadOnly | QScriptValue::Undeletable);
    engine->globalObject().setProperty("Resources", engine->toScriptValue(resourcePath), QScriptValue::ReadOnly | QScriptValue::Undeletable);
    engine->globalObject().setProperty("getNodeData", engine->newFunction(getNodeData), QScriptValue::ReadOnly | QScriptValue::Undeletable);
    qScriptRegisterMetaType<Pane*>(engine, Pane_toScriptValue, Pane_fromScriptValue);
    //Generic Pane constructor
    QScriptValue tempFunct = engine->newFunction(createPane);
    tempFunct.setProperty("functionName", QScriptValue("Pane"));
    engine->globalObject().setProperty("Pane", tempFunct);
    //---
    QFile fileobject(scriptPath);
    if (!fileobject.open(QIODevice::ReadOnly)) {
        qDebug("Script file not found!");
    } else {
        QTextStream stream(&fileobject);
        if (res.exists()&&res.isReadable()) {
            // ---Find all Accessable Panes and generate constructors---
            QStringList jsFilter, results, subDirResults;
            jsFilter << "*.js";
            results = res.entryList(jsFilter, QDir::AllDirs | QDir::Files | QDir::NoDotAndDotDot);
            QString tempName;
            int i, j;
            for (i=0; i<results.length(); i++) {
                if (results[i].endsWith(".js")) {
                    tempName = results[i];
                    tempName.remove(QRegExp("[^A-Za-z]"));
                    tempName.truncate(tempName.length()-2);//delets the 'js' left over from the extension (the '.' was already deleted)
                    if ((tempName!=paneTypeName&&tempName!="Pane")&&!tempName.isEmpty()) {
                        paneFiles[tempName] = resourcePath+results[i];
                        tempFunct = engine->newFunction(createPane);
                        tempFunct.setProperty("functionName", QScriptValue(tempName));
                        engine->globalObject().setProperty(tempName, tempFunct);
                    }
                } else {
                    subDirResults = QDir(resourcePath+results[i]).entryList(jsFilter, QDir::Files | QDir::NoDotAndDotDot);
                    for (j=0; j<subDirResults.length(); j++) {
                        tempName = subDirResults[j];
                        tempName.remove(QRegExp("[^A-Za-z]"));
                        tempName.truncate(tempName.length()-2);//delets the 'js' left over from the extension (the '.' was already deleted)
                        if ((tempName!=paneTypeName&&tempName!="Pane")&&!tempName.isEmpty()) {
                            paneFiles[tempName] = resourcePath+results[i]+'/'+subDirResults[j];
                            tempFunct = engine->newFunction(createPane);
                            tempFunct.setProperty("functionName", QScriptValue(tempName));
                            engine->globalObject().setProperty(tempName, tempFunct);
                        }
                    }
                }
            }
        } else qDebug("Resource directory not found or unreadable.");
        // ---Evaluate the pane script---
        QString script = stream.readAll();
        engine->evaluate(script, scriptPath);
    }
    setFloating(floating);
}

Pane::Pane(bool floating, QWidget *parent) :
    QWidget(parent)
{
    engine = NULL;
    scriptPath = "";
    paneTypeName = "Pane";
    setFloating(floating);
}

QString Pane::script() const {
    return scriptPath;
}

QString Pane::paneType() const {
    return paneTypeName;
}

bool Pane::isFloating() const {
    return m_floating;
}
void Pane::setFloating(bool floating) {
    m_floating = floating;
    if (m_floating) {
        setWindowFlags(Qt::Dialog);
        setAttribute(Qt::WA_DeleteOnClose);
        setWindowModality(Qt::NonModal);
        show();
    } else {
        setWindowFlags(Qt::Widget);
    }
    if (engine!=NULL) engine->globalObject().setProperty("FloatingPane", m_floating);
}

void Pane::moveEvent(QMoveEvent *) {
    if (engine!=NULL) engine->globalObject().property("OnReposition").call();
}

void Pane::resizeEvent(QResizeEvent *) {
    if (engine!=NULL) engine->globalObject().property("OnResize").call();
}

void Pane::paintEvent(QPaintEvent *) {
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    if (!m_floating) style()->drawPrimitive(QStyle::PE_FrameGroupBox, &opt, &p, this);
}
