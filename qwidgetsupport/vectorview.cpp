/*! \file vectorview.cpp
    \brief The VectorView widget
*/

//!  \addtogroup Custom_Widgets

#include "vectorview.h"

VectorView::VectorView(QWidget *parent) :
    QWidget(parent)
{
    m_dataJSON = "";
}

QString VectorView::dataJSON() const {
    return(m_dataJSON);
}

void VectorView::setDataJSON(QString JSONstring) {
    m_dataJSON = JSONstring;
}
