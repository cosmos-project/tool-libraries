#ifndef PANE_H
#define PANE_H

#include <QWidget>
#include "qtscriptsupport/cosmosscriptengine.h"

class Pane : public QWidget
{
    Q_OBJECT

	Q_PROPERTY(QString script READ script)
	Q_PROPERTY(QString paneType READ paneType)
	Q_PROPERTY(bool floatingPane READ isFloating WRITE setFloating)
public:
    explicit Pane(QString scriptFile, bool floating = false, QWidget *parent = 0);
    explicit Pane(bool floating = false, QWidget *parent = 0);

    QString script() const;

    QString paneType() const;

    bool isFloating() const;
    void setFloating(bool floating);
    
signals:
    
public slots:

protected:
    void moveEvent(QMoveEvent *e);
    void resizeEvent(QResizeEvent *e);

    void paintEvent(QPaintEvent *);

private:
    COSMOSScriptEngine *engine;
    QString scriptPath;
    QString resourcePath;
    QString paneTypeName;
    bool m_floating;
    
};

#endif // PANE_H
