#include "graphlib.h"


//! Draw part with openGL.
/*! Draw the defined COSMOS Part using openGL calls. using the current translation and rotation.
    \param piece ::piecestruc defining the Part to be drawn.
    \param color ::rvector with the RGBA value for the color
*/
void opengl_piece(cosmosstruc *cinfo, uint16_t pidx, uvector color)
{
    static GLUquadricObj* satquadric = NULL;

    if (satquadric == NULL)
    {
        satquadric = gluNewQuadric();
        gluQuadricDrawStyle(satquadric, GLU_FILL);
        gluQuadricNormals(satquadric, GLU_SMOOTH);
        gluQuadricOrientation(satquadric, GLU_OUTSIDE);
    }

    // TODO: update opengl to shaders
    glPushMatrix();
    glColor4d(color.a4[0],color.a4[1],color.a4[2],color.a4[3]);
    glBegin(GL_POLYGON);
    for (uint16_t i=0; i<cinfo->pieces[pidx].face_cnt; i++)
    {
        if (cinfo->pieces[pidx].face_idx[i] < cinfo->faces.size())
        {
            Vector normal = cinfo->faces[cinfo->pieces[pidx].face_idx[i]].normal;
            glNormal3d(normal[0], normal[1], normal[2]);
            facestruc *f = &cinfo->faces[cinfo->pieces[pidx].face_idx[i]];
            for (uint16_t k=0; k<f->vertex_cnt; ++k)
            {
                if (f->vertex_idx[k] < cinfo->vertexs.size())
                {
                    glVertex3d(cinfo->vertexs[f->vertex_idx[k]][0], cinfo->vertexs[f->vertex_idx[k]][1], cinfo->vertexs[f->vertex_idx[k]][2]);
                }
            }
        }

    }
    glEnd();
    //    switch (piece.type)
    //        {
    //    case PIECE_TYPE_INTERNAL_PANEL:
    //    case PIECE_TYPE_EXTERNAL_PANEL:
    //        if (piece.pnt_cnt > MAXPNT)
    //            break;
    //        glColor4d(color.a4[0],color.a4[1],color.a4[2],color.a4[3]);
    //        glBegin(GL_POLYGON);
    //        normal = rv_cross(rv_sub(piece.points[1],piece.points[0]),rv_sub(piece.points[2],piece.points[1]));
    //        glNormal3d(normal.col[0],normal.col[1],normal.col[2]);
    //        for (j=0; j<piece.pnt_cnt; j++)
    //        {
    //            glVertex3d(piece.points[j].col[0],piece.points[j].col[1],piece.points[j].col[2]);

    //        }
    //        glEnd();
    //        break;
    //    case PIECE_TYPE_CYLINDER:
    //    case PIECE_TYPE_CONE:
    //        if (piece.pnt_cnt != 3)
    //            break;
    //        normal = rv_sub(piece.points[1],piece.points[0]);
    //        length = length_rv(normal);
    //        radius = length_rv(rv_sub(piece.points[2],piece.points[1]));
    //        quat = q_change_between_rv(rv_unitz(),normal);
    //        axis = rv_quaternion2axis(quat);
    //        glTranslated(piece.points[0].col[0],piece.points[0].col[1],piece.points[0].col[2]);
    //        glRotated(DEGOF(length_rv(axis)),axis.col[0],axis.col[1],axis.col[2]);
    //        if (piece.type == PIECE_TYPE_CYLINDER)
    //        {
    //            glColor4d(color.a4[0],color.a4[1],color.a4[2],color.a4[3]);
    //            gluCylinder(satquadric,radius,radius,length,10,10);
    //        }
    //        else
    //        {
    //            glColor4d(color.a4[0],color.a4[1],color.a4[2],color.a4[3]);
    //            gluCylinder(satquadric,0.,radius,length,10,10);
    //        }
    //        break;
    //    case PIECE_TYPE_BOX:
    //        if (piece.pnt_cnt != 8)
    //                break;
    //        glColor4d(color.a4[0],color.a4[1],color.a4[2],color.a4[3]);
    //        for (j=0; j<6; j++)
    //        {
    //            glBegin(GL_POLYGON);
    //            normal = rv_cross(rv_sub(piece.points[box[j][1]],piece.points[box[j][0]]),rv_sub(piece.points[box[j][2]],piece.points[box[j][0]]));
    //            glNormal3d(normal.col[0],normal.col[1],normal.col[2]);
    //            // Faces
    //            for (k=0; k<4; k++)
    //            {
    //                glVertex3d(piece.points[box[j][k]].col[0],piece.points[box[j][k]].col[1],piece.points[box[j][k]].col[2]);
    //            }
    //            glEnd();
    //        }
    //        break;
    //    case PIECE_TYPE_SPHERE:
    //        if (piece.pnt_cnt != 2)
    //            break;
    //        glColor4d(color.a4[0],color.a4[1],color.a4[2],color.a4[3]);
    //        radius = length_rv(rv_sub(piece.points[1],piece.points[0]));
    //        glTranslated(piece.points[0].col[0],piece.points[0].col[1],piece.points[0].col[2]);
    //        gluSphere(satquadric,radius,10,10);
    //        break;
    //    }
    glPopMatrix();

}

//! COSMOS RGBA Color
/*! Create an RGBA color for use with the OpenGL Graphics routines. All colors and the transparency
  * from 0. to 1.
    \param red Red color value.
    \param green Green color value.
    \param blue Blue color value.
    \param transparency Transparency value.
    \return ::rvector with the color for use in OpenGL calls.
*/
uvector opengl_color(double red, double green, double blue, double transparency)
{
    uvector color;

    color.a4[0] = red;
    color.a4[1] = green;
    color.a4[2] = blue;
    color.a4[3] = transparency;

    return (color);
}
