/*! \file levelbar.h
    \brief LevelBar header file
*/

/*!  \defgroup Custom_Widgets Custom GUI Widgets
* @{
* LevelBar Class
*
* A colored bar that displays a value between zero and some maximum value.
*
* @}
*/

#ifndef LEVELBAR_H
#define LEVELBAR_H

#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif


class LevelBar : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString title READ title WRITE setTitle)
    Q_PROPERTY(QColor barColor READ barColor WRITE setBarColor)
    Q_PROPERTY(bool horizontal READ isHorizontal WRITE setHorizontal)
    Q_PROPERTY(double level READ level WRITE setLevel)

public:
    explicit LevelBar(QWidget *parent = 0);

    QString title() const;
    void setTitle(QString newTitle);

    QColor barColor() const;
    void setBarColor(QColor newColor);

    bool isHorizontal() const;
    void setHorizontal(bool horizontal);

    double level() const;
    void setLevel(double lvl);

private:

    QString m_title;
    QColor m_barColor;
    bool m_horizontal;
    double m_level;

protected:
    void paintEvent(QPaintEvent *);

};

#endif // LEVELBAR_H
