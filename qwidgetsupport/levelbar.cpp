/*! \file levelbar.cpp
    \brief The LevelBar widget
*/

//!  \addtogroup Custom_Widgets

#include "levelbar.h"

LevelBar::LevelBar(QWidget *parent) :
    QWidget(parent)
{
    //set default property values
    m_level = 0.0;
    m_title = QString("");
    m_horizontal = false;
    m_barColor = QColor(0, 255, 0, 255);
}

QString LevelBar::title() const {
    return(m_title);
}

void LevelBar::setTitle(QString newTitle) {
    m_title = newTitle;
    setToolTip(m_title);
}

QColor LevelBar::barColor() const {
    return(m_barColor);
}

void LevelBar::setBarColor(QColor newColor) {
    if (m_barColor!=newColor) update();
    m_barColor = newColor;
}

bool LevelBar::isHorizontal() const {
    return(m_horizontal);
}

void LevelBar::setHorizontal(bool horizontal) {
    if (m_horizontal!=horizontal) update();
    m_horizontal = horizontal;
}

double LevelBar::level() const {
    return m_level;
}
void LevelBar::setLevel(double lvl) { //Takes a number between 0 (empty) and 1 (full)
    if (lvl>1.0) lvl = 1.0;
    else if (lvl<0.0) lvl = 0.0;
    if (lvl!=m_level) {
        m_level = lvl;
        update();
    }
}

void LevelBar::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    QLinearGradient gradient;
    painter.setBrush(QBrush(QColor(50, 50, 50)));
    QPen outlinePen = QPen(Qt::black);
    outlinePen.setWidth(1);
    painter.setPen(outlinePen);
    int w=width()-1, h=height()-1;
    painter.drawRect(0, 0, w, h);
    w -= 1;
    h -= 1;
    if (m_horizontal) {
        gradient = QLinearGradient(0, h, 1, 0);
        gradient.setColorAt(0, m_barColor.darker(150));
        gradient.setColorAt(1, m_barColor.lighter(110));
        painter.setBrush(QBrush(gradient));
        painter.fillRect(1, 1, (int)(w*m_level + 0.5), h, painter.brush());
    } else {
        gradient = QLinearGradient(-(w/10), 0, (w/2)-(w/10), 0);
        gradient.setColorAt(0, m_barColor.darker(110));
        gradient.setColorAt(1, m_barColor.lighter(110));
        gradient.setSpread(QGradient::ReflectSpread);
        painter.setBrush(QBrush(gradient));
        painter.fillRect(1, h+1, w, -(int)(h*m_level + 0.5), painter.brush());
    }
}
