#ifndef GRAPHLIB_H
#define GRAPHLIB_H

#include "support/configCosmos.h"
#include <QtGlobal>

#ifdef COSMOS_MAC_OS
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

#include "support/cosmos-errno.h"
#include "math/mathlib.h"
#include "physics/physicslib.h"
#include "support/cosmos-defs.h"

/*! \file graphlib.h
*	\brief COSMOS OpenGL Graphics header file
*/

/*! \defgroup grpahics COSMOS OpenGL Grpahics Library
* @{
* COSMOS OpenGL Grpahics support library.
*
* This library provides functions to support both the COSMOS Vehicle Description and the MOST DEM.
* The Vehicle Description is supported through function calls that create the OpenGL language to create
* the various elements included in a Vehicle Description ::piecestruc.

* The Parts are defined as follows:

* - PIECE_TYPE_INTERNAL_PANEL and PIECE_TYPE_EXTERNAL_PANEL: a polygon of piecestruc.pntcount points and thickness
* piecestruc.dim. Normals follow a right hand rule with respect to the listing of the vertices.
* - PIECE_TYPE_BOX: A box defined by 8 points. The first 4 points define the primary outward facing side, while the
* next 4 define the opposing side in a parallel sense (0,1,2,3 matches 4,5,6,7).  Thickness of the walls is defined by
* piecestruc.dim.
* - PIECE_TYPE_CYLINDER: A cylinder defined by a near end, a far end, and a far end point on the circumference.  Thickness of the walls is defined by
* piecestruc.dim.
* - PIECE_TYPE_CONE: A cone defined by a near point, a far end, and a far end point on the circumference.  Thickness of the walls is defined by
* piecestruc.dim.
* - PIECE_TYPE_SPHERE: A sphere defined by a center and a point on the outside. Thickness of the walls is defined by
* piecestruc.dim.

* @}
*/

//! \ingroup graphics
//! \defgroup grpahics_functions COSMOS OpenGL Graphics functions
//! @{

void opengl_piece(cosmosstruc *cinfo, uint16_t pidx, uvector color);
uvector opengl_color(double red, double green, double blue, double transparency);


//! @}

#endif // GRAPHLIB_H
