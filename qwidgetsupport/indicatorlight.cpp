/*! \file indicatorlight.cpp
    \brief The IndicatorLight widget
*/

//!  \addtogroup Custom_Widgets

#include "indicatorlight.h"

IndicatorLight::IndicatorLight(QWidget *parent) :
    QWidget(parent)
{
    m_lightColor = QColor(0, 255, 0, 255); //default light color
    m_lightState = false;
}

QColor IndicatorLight::lightColor() const {
    return(m_lightColor);
}

void IndicatorLight::setLightColor(QColor newColor) {
    if (m_lightColor!=newColor) update();
    m_lightColor = newColor;
}

int IndicatorLight::state() const {
    if (m_lightState) return 1;
    else return 0;
}
void IndicatorLight::setState(int on) {
    if(m_lightState!=(on==1)) { //If the new state is different,
        m_lightState = !m_lightState; //change states,
        update(); //and re-render.
    }
}

void IndicatorLight::copyValue() {
    if (m_lightState) QApplication::clipboard()->setText("On");
    else QApplication::clipboard()->setText("Off");
}

void IndicatorLight::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    QRadialGradient gradient;
    gradient = QRadialGradient(width()/2, height()/2, (width()>height())?width():height(), width()/2, height()/2);
    gradient.setSpread(QGradient::ReflectSpread);
    QPen outlinePen = QPen(Qt::black);
    outlinePen.setWidth(1);
    painter.setPen(outlinePen);
    if (m_lightState) {
        gradient.setColorAt(0, m_lightColor.lighter(160));
        gradient.setColorAt(1, m_lightColor.darker(200));
    } else {
        gradient.setColorAt(0, QColor(65, 65, 65));
        gradient.setColorAt(1, QColor(10, 10, 10));
    }
    painter.setBrush(QBrush(gradient));
    painter.drawRect(0, 0, width()-1, height()-1);
}
