/*! \file chartitem.h
    \brief ChartItem header file
*/

/*!  \addtogroup Custom_Widgets
* @{
* ChartItem Class
*
* Represents an item in a chart, storing a piece of information and variables that determine how it will be drawn.
*
* @}
*/

#ifndef CHARTITEM_H
#define CHARTITEM_H

#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif


class ChartItem : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString title READ title WRITE setTitle)
    Q_PROPERTY(double value READ value WRITE setValue)
    Q_PROPERTY(QColor color READ color WRITE setColor)

public:
    explicit ChartItem(QWidget *parent = 0);

    QString title() const;
    void setTitle(QString name);

    double value() const;
    void setValue(double newValue);

    QColor color() const;
    void setColor(QColor newColor);
    
signals:
    
private:

    double dataValue;

    QString m_title;
    QColor m_color;
    
};

#endif // CHARTITEM_H
