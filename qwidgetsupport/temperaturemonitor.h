/*! \file temperaturemonitor.h
    \brief TemperatureMonitor header file
*/

/*!  \addtogroup Custom_Widgets
* @{
* TemperatureMonitor Class
*
* Displays a temperature in Kelvin or Celcius on top of a colored background which changes from blue-green to green to yellow as
* the temperautre goes from a minimum to a maximum value.  When the temperature exceeds or cools below the specified levels the backgroud
* changes to red (hotter) or midnight blue (colder), the temperature turns white and the border and background flash to signal that
* the temperature is out of range.
*
* @}
*/

#ifndef TEMPERATUREMONITOR_H
#define TEMPERATUREMONITOR_H

#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif


class TemperatureMonitor : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(double temperature READ temperature WRITE updateTemp)
    Q_PROPERTY(double minTemp READ minTemp WRITE setMinTemp)
    Q_PROPERTY(double maxTemp READ maxTemp WRITE setMaxTemp)
    Q_PROPERTY(bool celcius READ celcius WRITE setCelcius)
    Q_PROPERTY(bool unitsShown READ unitsShown WRITE showUnits)

public:
    explicit TemperatureMonitor(QWidget *parent = 0);

    double temperature() const;
    void updateTemp(double t);

    double minTemp() const;
    void setMinTemp(double t);

    double maxTemp() const;
    void setMaxTemp(double t);

    bool celcius() const;
    void setCelcius(bool useCelcius);

    bool unitsShown() const;
    void showUnits(bool show);

private:
    double m_temperature;
    double m_minTemp;
    double m_maxTemp;

    bool m_celcius;
    bool m_unitsShown;

public slots:
    void copyValue();

protected:
    void paintEvent(QPaintEvent *);

};

#endif // TEMPERATUREMONITOR_H
