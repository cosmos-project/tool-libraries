import qbs

Product  { // could be DynamicLibrary but at this point loading a dll does not seem to work well
    type: "staticlibrary"
    name: "qwidgetsupport"

    files: [
//        "graphlib.cpp",
//        "graphlib.h",
//        "cosmosuiloader.cpp",
//        "cosmosuiloader.h"
        "*.cpp", "*.h"
    ]

    Depends {
        name: "Qt";
        submodules: ["core", "gui", "widgets"]
    }

    Depends { name: "cpp" }
    Properties {
        condition: qbs.targetOS.contains("windows")
        cpp.minimumWindowsVersion: "7.0"
    }
    cpp.cxxLanguageVersion : "c++11"
    cpp.commonCompilerFlags : "-std=c++11"

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: ["./","../", "../../"]
    }

    cpp.includePaths : [
        './',
        "../",
        "../../most/source",
        "../../../core/libraries/",
        "../../../core/libraries/support/",
        "../../../core/libraries/thirdparty/"
    ]


}
