/*! \file rotationview.cpp
    \brief The RotationView widget
*/

//!  \addtogroup Custom_Widgets

#include "rotationview.h"
#include <QMenu>
#include <QGridLayout>
#include <QMouseEvent>
#include <QToolTip>
#include <QDebug>

RotationView::RotationView(QWidget *parent) :
    QWidget(parent)
{
    //Set up widget:
    /*
    <---------------------Set up quaternion view:--------------------->
    */
    quatBox = new QWidget(this);
    quatX = new QLineEdit(quatBox);
    quatX->setToolTip("x");
    quatY = new QLineEdit(quatBox);
    quatY->setToolTip("y");
    quatZ = new QLineEdit(quatBox);
    quatZ->setToolTip("z");
    quatW = new QLineEdit(quatBox);
    quatW->setToolTip("w");
    QGridLayout *tempLayout = new QGridLayout(quatBox);
    tempLayout->setMargin(0);
    tempLayout->setSpacing(0);
    XLabel = new QLabel("x", quatBox);
    tempLayout->addWidget(XLabel, 0, 0);
    tempLayout->addWidget(quatX, 0, 1);
    YLabel = new QLabel("y", quatBox);
    tempLayout->addWidget(YLabel, 1, 0);
    tempLayout->addWidget(quatY, 1, 1);
    ZLabel = new QLabel("z", quatBox);
    tempLayout->addWidget(ZLabel, 2, 0);
    tempLayout->addWidget(quatZ, 2, 1);
    WLabel = new QLabel("w", quatBox);
    tempLayout->addWidget(WLabel, 3, 0);
    tempLayout->addWidget(quatW, 3, 1);
    /*
    <------------------------Set up DCM view:------------------------->
    */
    dcmBox = new QWidget(this);
    dcm1_1 = new QLabel("1", dcmBox);
    dcm1_1->setMouseTracking(true);
    dcm1_2 = new QLabel("0", dcmBox);
    dcm1_2->setMouseTracking(true);
    dcm1_3 = new QLabel("0", dcmBox);
    dcm1_3->setMouseTracking(true);
    dcm2_1 = new QLabel("0", dcmBox);
    dcm2_1->setMouseTracking(true);
    dcm2_2 = new QLabel("1", dcmBox);
    dcm2_2->setMouseTracking(true);
    dcm2_3 = new QLabel("0", dcmBox);
    dcm2_3->setMouseTracking(true);
    dcm3_1 = new QLabel("0", dcmBox);
    dcm3_1->setMouseTracking(true);
    dcm3_2 = new QLabel("0", dcmBox);
    dcm3_2->setMouseTracking(true);
    dcm3_3 = new QLabel("1", dcmBox);
    dcm3_3->setMouseTracking(true);
    tempLayout = new QGridLayout(dcmBox);
    tempLayout->setMargin(0);
    tempLayout->setSpacing(0);
    tempLayout->addWidget(dcm1_1, 0, 0);
    tempLayout->addWidget(dcm1_2, 0, 1);
    tempLayout->addWidget(dcm1_3, 0, 2);
    tempLayout->addWidget(dcm2_1, 1, 0);
    tempLayout->addWidget(dcm2_2, 1, 1);
    tempLayout->addWidget(dcm2_3, 1, 2);
    tempLayout->addWidget(dcm3_1, 2, 0);
    tempLayout->addWidget(dcm3_2, 2, 1);
    tempLayout->addWidget(dcm3_3, 2, 2);
    /*
    <--------------------Set up Euler angles view:-------------------->
    */
    eulerBox = new QWidget(this);
    eulerH = new QLineEdit(eulerBox);
    eulerH->setToolTip("Heading");
    eulerE = new QLineEdit(eulerBox);
    eulerE->setToolTip("Elevation");
    eulerB = new QLineEdit(eulerBox);
    eulerB->setToolTip("Bank");
    tempLayout = new QGridLayout(eulerBox);
    tempLayout->setMargin(0);
    tempLayout->setSpacing(0);
    HLabel = new QLabel("h", eulerBox);
    tempLayout->addWidget(HLabel, 0, 0);
    tempLayout->addWidget(eulerH, 0, 1);
    ELabel = new QLabel("e", eulerBox);
    tempLayout->addWidget(ELabel, 1, 0);
    tempLayout->addWidget(eulerE, 1, 1);
    BLabel = new QLabel("b", eulerBox);
    tempLayout->addWidget(BLabel, 2, 0);
    tempLayout->addWidget(eulerB, 2, 1);
    /*
    <---------------------Set up axis-angle view:--------------------->
    */
    axisAngleBox = new QWidget(this);
    axisAngle = new QLineEdit(axisAngleBox);
    axisAngle->setToolTip("Angle");
    axisX = new QLineEdit(axisAngleBox);
    axisX->setToolTip("x");
    axisY = new QLineEdit(axisAngleBox);
    axisY->setToolTip("y");
    axisZ = new QLineEdit(axisAngleBox);
    axisZ->setToolTip("z");
    tempLayout = new QGridLayout(axisAngleBox);
    tempLayout->setMargin(0);
    tempLayout->setSpacing(0);
    axisLabelX = new QLabel("x", axisAngleBox);
    tempLayout->addWidget(axisLabelX, 0, 0);
    tempLayout->addWidget(axisX, 0, 1);
    axisLabelY = new QLabel("y", axisAngleBox);
    tempLayout->addWidget(axisLabelY, 1, 0);
    tempLayout->addWidget(axisY, 1, 1);
    axisLabelZ = new QLabel("z", axisAngleBox);
    tempLayout->addWidget(axisLabelZ, 2, 0);
    tempLayout->addWidget(axisZ, 2, 1);
    labelAngle = new QLabel(QString::fromUtf8("\u03B8"), axisAngleBox);
    tempLayout->addWidget(labelAngle, 3, 0);
    tempLayout->addWidget(axisAngle, 3, 1);
    /*
    <----------------Set up mode switching menu button:--------------->
    */
    menuButton = new QToolButton(this);
    menuButton->setMouseTracking(true);
    menuButton->setGeometry(0, 0, 12, 12);
    menuButton->setPopupMode(QToolButton::InstantPopup);
    //create mode menu:
    modeMenu = new QMenu(this);
    modeMenu->addAction("Quaternion");
    connect(modeMenu->actions().last(), SIGNAL(triggered()), this, SLOT(switchToQuat()));
    modeMenu->addAction("DCM");
    connect(modeMenu->actions().last(), SIGNAL(triggered()), this, SLOT(switchToDCM()));
    modeMenu->addAction("Euler");
    connect(modeMenu->actions().last(), SIGNAL(triggered()), this, SLOT(switchToEuler()));
    modeMenu->addAction("Angle-Axis");
    connect(modeMenu->actions().last(), SIGNAL(triggered()), this, SLOT(switchToAngleAxis()));
    menuButton->setMenu(modeMenu);
    /*
    <--------------Enable mouse tracking on all widgets:-------------->
    */
    quatBox->setMouseTracking(true);
    quatX->setMouseTracking(true);
    quatY->setMouseTracking(true);
    quatZ->setMouseTracking(true);
    quatW->setMouseTracking(true);
    dcmBox->setMouseTracking(true);
    eulerBox->setMouseTracking(true);
    eulerH->setMouseTracking(true);
    eulerE->setMouseTracking(true);
    eulerB->setMouseTracking(true);
    XLabel->setMouseTracking(true);
    YLabel->setMouseTracking(true);
    ZLabel->setMouseTracking(true);
    WLabel->setMouseTracking(true);
    HLabel->setMouseTracking(true);
    ELabel->setMouseTracking(true);
    BLabel->setMouseTracking(true);
    axisAngleBox->setMouseTracking(true);
    axisAngle->setMouseTracking(true);
    axisX->setMouseTracking(true);
    axisY->setMouseTracking(true);
    axisZ->setMouseTracking(true);
    axisLabelX->setMouseTracking(true);
    axisLabelY->setMouseTracking(true);
    axisLabelZ->setMouseTracking(true);
    labelAngle->setMouseTracking(true);
    /*
    <--------------Disable context menus on all line edits:-------------->
    */
    quatX->setContextMenuPolicy(Qt::NoContextMenu);
    quatY->setContextMenuPolicy(Qt::NoContextMenu);
    quatZ->setContextMenuPolicy(Qt::NoContextMenu);
    quatW->setContextMenuPolicy(Qt::NoContextMenu);
    eulerH->setContextMenuPolicy(Qt::NoContextMenu);
    eulerE->setContextMenuPolicy(Qt::NoContextMenu);
    eulerB->setContextMenuPolicy(Qt::NoContextMenu);
    axisX->setContextMenuPolicy(Qt::NoContextMenu);
    axisY->setContextMenuPolicy(Qt::NoContextMenu);
    axisZ->setContextMenuPolicy(Qt::NoContextMenu);
    axisAngle->setContextMenuPolicy(Qt::NoContextMenu);
    //Other:
    /*
    <-----------Initialize other variables and properties:------------>
    */
    mouseOver = false;
    m_showLabels = true;
    m_modeLocked = false;
    m_radians = true;
    m_mode = euler;
    quatBox->hide();
    dcmBox->hide();
    axisAngleBox->hide();
    eulerBox->show();
    setMouseTracking(true);
    controller = NULL;
}

void RotationView::linkTo(RotationView *view) {
    if (controller!=NULL) {
        for (int i=0; i<controller->linkedViews.length(); i++) {
            if (controller->linkedViews[i]==this) {
                controller->linkedViews.removeAt(i);
                break;
            }
        }
    }
    controller = view;
    if (controller!=NULL) {
        controller->linkedViews << this;
        lockMode(true);
        setMode(controller->mode());
    } else lockMode(false);
}

quaternion *RotationView::attitude() const {
    return m_rotation;
}

void RotationView::setAttitude(quaternion *attPtr) {
    if (attPtr == NULL) return;
    m_rotation = attPtr;
    quatX->setText(QString::number(m_rotation->d.x));
    quatY->setText(QString::number(m_rotation->d.y));
    quatZ->setText(QString::number(m_rotation->d.z));
    quatW->setText(QString::number(m_rotation->w));
    dcmRotation = rm_quaternion2dcm(*m_rotation);
    dcm1_1->setText(QString("%1").arg(dcmRotation.row[0].col[0], 1, 'f', 1));
    dcm1_2->setText(QString("%1").arg(dcmRotation.row[0].col[1], 1, 'f', 1));
    dcm1_3->setText(QString("%1").arg(dcmRotation.row[0].col[2], 1, 'f', 1));
    dcm2_1->setText(QString("%1").arg(dcmRotation.row[1].col[0], 1, 'f', 1));
    dcm2_2->setText(QString("%1").arg(dcmRotation.row[1].col[1], 1, 'f', 1));
    dcm2_3->setText(QString("%1").arg(dcmRotation.row[1].col[2], 1, 'f', 1));
    dcm3_1->setText(QString("%1").arg(dcmRotation.row[2].col[0], 1, 'f', 1));
    dcm3_2->setText(QString("%1").arg(dcmRotation.row[2].col[1], 1, 'f', 1));
    dcm3_3->setText(QString("%1").arg(dcmRotation.row[2].col[2], 1, 'f', 1));
    avector eulerRotation = a_quaternion2euler(*m_rotation);
    eulerH->setText(QString::number(eulerRotation.h));
    eulerE->setText(QString::number(eulerRotation.e));
    eulerB->setText(QString::number(eulerRotation.b));
    rvector axisRotation = rv_quaternion2axis(*m_rotation);
    axisAngle->setText(QString::number(length_rv(axisRotation)));
    axisRotation = rv_normal(axisRotation);
    axisX->setText(QString::number(axisRotation.col[0]));
    axisY->setText(QString::number(axisRotation.col[1]));
    axisZ->setText(QString::number(axisRotation.col[2]));
    //Must be shown for rotations
    quatW->show();
    WLabel->show();
    //If in DCM mode, update the mouse over field
    if (mouseOver&&m_mode==dcm) updateMouseOver();
}

rvector* RotationView::derivative() const {
    return(m_derivatives);
}

void RotationView::setDerivative(rvector *derivPtr) {
    if (derivPtr==NULL) return;
    m_derivatives = derivPtr;
    quatX->setText(QString::number(m_derivatives->col[0]));
    quatY->setText(QString::number(m_derivatives->col[1]));
    quatZ->setText(QString::number(m_derivatives->col[2]));
    dcmRotation = rm_zero();
    eulerH->setText(QString::number(m_derivatives->col[2]));
    eulerE->setText(QString::number(m_derivatives->col[1]));
    eulerB->setText(QString::number(m_derivatives->col[0]));
    rvector axis = rv_normal(*m_derivatives);
    axisX->setText(QString::number(axis.col[0]));
    axisY->setText(QString::number(axis.col[1]));
    axisZ->setText(QString::number(axis.col[2]));
    axisAngle->setText(QString::number(length_rv(*m_derivatives)));
    //Must be hidden for rotation rates and accelerations
    quatW->hide();
    WLabel->hide();
    //If in DCM mode, update the mouse over field
    if (mouseOver&&m_mode==dcm) updateMouseOver();
}

RotationView::displayMode RotationView::mode() const {
    return(m_mode);
}

void RotationView::setMode(displayMode newMode) {
    m_mode = newMode;
    switch (m_mode) {
    case quat:
        dcmBox->hide();
        eulerBox->hide();
        axisAngleBox->hide();
        quatBox->show();
        break;
    case dcm:
        quatBox->hide();
        eulerBox->hide();
        axisAngleBox->hide();
        dcmBox->show();
        break;
    case euler:
        quatBox->hide();
        dcmBox->hide();
        axisAngleBox->hide();
        eulerBox->show();
        break;
    case angleAxis:
        quatBox->hide();
        dcmBox->hide();
        eulerBox->hide();
        axisAngleBox->show();
        break;
    default:
        break;
    }
    //If we are not controlled by any other views, set all the views we control to our current mode
    if (controller==NULL) {
        for (int i=0; i<linkedViews.length(); i++) {
            linkedViews[i]->setMode(m_mode);
        }
    }
}

bool RotationView::labelsShown() const {
    return(m_showLabels);
}

void RotationView::showLabels(bool show) {
    m_showLabels = show;
    XLabel->setVisible(m_showLabels);
    YLabel->setVisible(m_showLabels);
    ZLabel->setVisible(m_showLabels);
    WLabel->setVisible(m_showLabels);
    HLabel->setVisible(m_showLabels);
    ELabel->setVisible(m_showLabels);
    BLabel->setVisible(m_showLabels);
    axisLabelX->setVisible(m_showLabels);
    axisLabelY->setVisible(m_showLabels);
    axisLabelZ->setVisible(m_showLabels);
    labelAngle->setVisible(m_showLabels);
}

bool RotationView::modeLocked() const {
    return(m_modeLocked);
}

void RotationView::lockMode(bool lock) {
    m_modeLocked = lock;
    menuButton->setVisible(!m_modeLocked);
}

void RotationView::switchToQuat() {
    setMode(quat);
}

void RotationView::switchToDCM() {
    setMode(dcm);
}

void RotationView::switchToEuler() {
    setMode(euler);
}

void RotationView::switchToAngleAxis() {
    setMode(angleAxis);
}

void RotationView::resizeEvent(QResizeEvent *) {
    int offset = m_modeLocked?0:10; //move the side of each widget right to make way for the mode menu button (except DCM since it seems to fit fine on it's own).
    quatBox->setGeometry(offset, 0, width()-offset, height());
    dcmBox->setGeometry(0, 0, width(), height());
    eulerBox->setGeometry(offset, 0, width()-offset, height());
    axisAngleBox->setGeometry(offset, 0, width()-offset, height());
}

void RotationView::enterEvent(QEvent *) {
    mouseOver = true;
}

void RotationView::leaveEvent(QEvent *) {
    mouseOver = false;
    QToolTip::hideText();
}

void RotationView::mouseMoveEvent(QMouseEvent *event) {
    if (mouseOver&&m_mode==dcm) {
        mousePos = event->globalPos();
        updateMouseOver();
    }
}

void RotationView::updateMouseOver() {
    QPoint loc  = mapFromGlobal(mousePos);
    float scaledX = ((float)loc.x())/((float)width()), scaledY = ((float)loc.y())/((float)height());
    if (scaledY<=0.333) {
        if (scaledX<=0.333) {
            QToolTip::showText(mousePos, QString("(0,0)=%1 ").arg(dcmRotation.row[0].col[0]));
            QToolTip::showText(mousePos, QString("(0,0)=%1").arg(dcmRotation.row[0].col[0]));
        } else if (scaledX<=0.666) {
            QToolTip::showText(mousePos, QString("(0,1)=%1 ").arg(dcmRotation.row[0].col[1]));
            QToolTip::showText(mousePos, QString("(0,1)=%1").arg(dcmRotation.row[0].col[1]));
        } else {
            QToolTip::showText(mousePos, QString("(0,2)=%1 ").arg(dcmRotation.row[0].col[2]));
            QToolTip::showText(mousePos, QString("(0,2)=%1").arg(dcmRotation.row[0].col[2]));
        }
    } else if (scaledY<=0.666) {
        if (scaledX<=0.333) {
            QToolTip::showText(mousePos, QString("(1,0)=%1 ").arg(dcmRotation.row[1].col[0]));
            QToolTip::showText(mousePos, QString("(1,0)=%1").arg(dcmRotation.row[1].col[0]));
        } else if (scaledX<=0.666) {
            QToolTip::showText(mousePos, QString("(1,1)=%1 ").arg(dcmRotation.row[1].col[1]));
            QToolTip::showText(mousePos, QString("(1,1)=%1").arg(dcmRotation.row[1].col[1]));
        } else {
            QToolTip::showText(mousePos, QString("(1,2)=%1 ").arg(dcmRotation.row[1].col[2]));
            QToolTip::showText(mousePos, QString("(1,2)=%1").arg(dcmRotation.row[1].col[2]));
        }
    } else {
        if (scaledX<=0.333) {
            QToolTip::showText(mousePos, QString("(2,0)=%1 ").arg(dcmRotation.row[2].col[0]));
            QToolTip::showText(mousePos, QString("(2,0)=%1").arg(dcmRotation.row[2].col[0]));
        } else if (scaledX<=0.666) {
            QToolTip::showText(mousePos, QString("(2,1)=%1 ").arg(dcmRotation.row[2].col[1]));
            QToolTip::showText(mousePos, QString("(2,1)=%1").arg(dcmRotation.row[2].col[1]));
        } else {
            QToolTip::showText(mousePos, QString("(2,2)=%1 ").arg(dcmRotation.row[2].col[2]));
            QToolTip::showText(mousePos, QString("(2,2)=%1").arg(dcmRotation.row[2].col[2]));
        }
    }
}

QString RotationView::quatXLabel() const {
    return(XLabel->text());
}
void RotationView::setQuatXLabel(QString text) {
    XLabel->setText(text);
    quatX->setToolTip(text);
}

QString RotationView::quatYLabel() const {
    return(YLabel->text());
}
void RotationView::setQuatYLabel(QString text) {
    YLabel->setText(text);
    quatY->setToolTip(text);
}

QString RotationView::quatZLabel() const {
    return(ZLabel->text());
}
void RotationView::setQuatZLabel(QString text) {
    ZLabel->setText(text);
    quatZ->setToolTip(text);
}

QString RotationView::quatWLabel() const {
    return(WLabel->text());
}
void RotationView::setQuatWLabel(QString text) {
    WLabel->setText(text);
    quatW->setToolTip(text);
}

QString RotationView::eulerHLabel() const {
    return(HLabel->text());
}
void RotationView::setEulerHLabel(QString text) {
    HLabel->setText(text);
    eulerH->setToolTip(text);
}

QString RotationView::eulerELabel() const {
    return(ELabel->text());
}
void RotationView::setEulerELabel(QString text) {
    ELabel->setText(text);
    eulerE->setToolTip(text);
}

QString RotationView::eulerBLabel() const {
    return(BLabel->text());
}
void RotationView::setEulerBLabel(QString text) {
    BLabel->setText(text);
    eulerB->setToolTip(text);
}

QString RotationView::axisXLabel() const {
    return(axisLabelX->text());
}
void RotationView::setAxisXLabel(QString text) {
    axisLabelX->setText(text);
    axisX->setToolTip(text);
}

QString RotationView::axisYLabel() const {
    return(axisLabelY->text());
}
void RotationView::setAxisYLabel(QString text) {
    axisLabelY->setText(text);
    axisY->setToolTip(text);
}

QString RotationView::axisZLabel() const {
    return(axisLabelZ->text());
}
void RotationView::setAxisZLabel(QString text) {
    axisLabelZ->setText(text);
    axisZ->setToolTip(text);
}

QString RotationView::angleLabel() const {
    return(labelAngle->text());
}
void RotationView::setAngleLabel(QString text) {
    labelAngle->setText(text);
    axisAngle->setToolTip(text);
}
