/*! \file temperaturemonitor.cpp
    \brief The TemperatureMonitor widget
*/

//!  \addtogroup Custom_Widgets

#include "temperaturemonitor.h"
#include <QTime>

TemperatureMonitor::TemperatureMonitor(QWidget *parent) :
    QWidget(parent)
{
    m_temperature = 280.0;
    m_minTemp = 260.0;
    m_maxTemp = 320.0;
    m_celcius = false;
    m_unitsShown = true;
    setMaximumWidth(50);
    setMinimumWidth(40);
    setMinimumHeight(23);
    setMaximumHeight(25);
    //This timer setup might be temporary, or it may only activate when the temperature is out of range.
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(100);
}

double TemperatureMonitor::temperature() const {
    return(m_temperature);
}
void TemperatureMonitor::updateTemp(double t) {
    if (m_temperature!=t) update(); //The update function doesn't trigger an immediate update so this is ok.
    m_temperature = t;
}

double TemperatureMonitor::minTemp() const {
    return(m_minTemp);
}
void TemperatureMonitor::setMinTemp(double t) {
    if (m_minTemp!=t) update();
    m_minTemp = t;
}

double TemperatureMonitor::maxTemp() const {
    return(m_maxTemp);
}
void TemperatureMonitor::setMaxTemp(double t) {
    if (m_maxTemp!=t) update();
    m_maxTemp = t;
}

bool TemperatureMonitor::celcius() const {
    return(m_celcius);
}
void TemperatureMonitor::setCelcius(bool useCelcius) {
    if (m_celcius!=useCelcius) update();
    m_celcius = useCelcius;
}

bool TemperatureMonitor::unitsShown() const {
    return(m_unitsShown);
}
void TemperatureMonitor::showUnits(bool show) {
    if (m_unitsShown!=show) update();
    m_unitsShown = show;
}

void TemperatureMonitor::copyValue() {
    QApplication::clipboard()->setText(QString::number(m_temperature));
}

void TemperatureMonitor::paintEvent(QPaintEvent *) {
    int deltaT = 500-QTime::currentTime().msec();
    if (deltaT<0) deltaT += 500;
    double min = m_minTemp, max = m_maxTemp;
    if (min>max) min = max-1;
    QPainter painter(this);
    double f;
    bool outOfRange = false;
    QPen outlinePen = QPen();
    outlinePen.setWidth(1);
    QColor bkgColor, textColor;
    QRadialGradient gradient;
    gradient = QRadialGradient(width()/2, height()/2, (width()>height())?width():height(), width()/2, height()/2);
    gradient.setSpread(QGradient::ReflectSpread);
    if (m_temperature>max) { //Above the maximum temperature
        f = (m_temperature-max)/20.0;
        if (f>1) f = 1;
        bkgColor = QColor::fromHsv(10-(int)(10*f), 255, 255); //fade from red-orange to red
        textColor = QColor(Qt::white);
        outlinePen.setColor(QColor(0, 0, 0));
        gradient.setColorAt(0, bkgColor.lighter(120+(deltaT/10)));
        gradient.setColorAt(1-(deltaT/1250.0), bkgColor.darker(200));
        outOfRange = true;
    } else if (m_temperature<min) { //Below the minimum temperature
        f = 20+m_temperature-min;
        if (f<0) f=0;
        else f /= 20.0;
        bkgColor = QColor::fromHsv(260-(int)(30*f), 255, 255); //fade from magenta to blue
        textColor = QColor(Qt::white);
        outlinePen.setColor(QColor(0, 0, 0));
        gradient.setColorAt(0, bkgColor.lighter(120+(deltaT/10)));
        gradient.setColorAt(1-(deltaT/1250.0), bkgColor.darker(200));
        outOfRange = true;
    } else { //In the operational temperature range
        f = fabs(m_temperature-min)/fabs(max-min);
        bkgColor = QColor::fromHsv(175-(int)(120*f), 255, 255); //fade from green-blue to yellow
        textColor = QColor(Qt::black);
        gradient.setColorAt(0, bkgColor.lighter(150));
        gradient.setColorAt(1, bkgColor.darker(160));
    }
    outlinePen.setColor(QColor(0, 0, 0));
    painter.setBrush(gradient);
    painter.setPen(outlinePen);
    painter.drawRect(0, 0, width()-1, height()-1);
    QString tempText;
    if (m_unitsShown) {
        if (m_celcius) tempText = QString("%1C").arg(m_temperature-273.15, 0, 'f', 1);
        else tempText = QString("%1K").arg(m_temperature, 0, 'f', 1);
    } else {
        tempText = QString("%1");
        if (m_celcius) tempText.arg(m_temperature-273.15, 0, 'f', 2);
        else tempText.arg(m_temperature, 0, 'f', 2);
    }
    painter.setPen(textColor);
    painter.drawText(5, (height()+(fontMetrics().height()-3))/2, tempText);
    if (outOfRange&&deltaT<250) {
        painter.setBrush(Qt::NoBrush);
        painter.drawRect(1, 1, width()-3, height()-3);
    }
}
