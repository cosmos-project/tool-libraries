/*! \file indicatorlight.h
    \brief IndicatorLight header file
*/

/*!  \addtogroup Custom_Widgets
* @{
* IndicatorLight Class
*
* A simple colored indicator light.  Lights up in the specified color when on, appears dark gray when off.
*
* @}
*/

#ifndef INDICATORLIGHT_H
#define INDICATORLIGHT_H

#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif


class IndicatorLight : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QColor lightColor READ lightColor WRITE setLightColor)
    Q_PROPERTY(int state READ state WRITE setState)

public:
    explicit IndicatorLight(QWidget *parent = 0);

    QColor lightColor() const;
    void setLightColor(QColor newColor);

    int state() const;
    void setState(int on); //1 -> on, !1 -> off

signals:

public slots:
    void copyValue();

private:

    QColor m_lightColor;
    bool m_lightState;
    
protected:
    void paintEvent(QPaintEvent *);
    
};

#endif // INDICATORLIGHT_H
