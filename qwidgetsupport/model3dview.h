/*! \file model3dview.h
    \brief Model3DView header file
*/

/*!  \addtogroup Custom_Widgets
* @{
* Model3DView Class
*
* Displays a 3D model of the satellite in its current attitude in the selected reference frame.
*
* @}
*/

#ifndef MODEL3DVIEW_H
#define MODEL3DVIEW_H

#include "support/configCosmos.h"

#include <QOpenGLWidget>

#include <QtGlobal>

#ifdef COSMOS_MAC_OS
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

#include "rotationview.h"


//class QTimer;

class Model3DView : public QOpenGLWidget
{
	Q_OBJECT

    Q_PROPERTY(RotationView::referenceFrame mode READ mode WRITE setMode)

public:

    explicit Model3DView(QWidget *parent = 0);
    ~Model3DView();

    RotationView::referenceFrame mode() const;
    void setMode(RotationView::referenceFrame newMode);

	//int l;
	GLfloat rtri, rquad;

signals:

public slots:
//	void update(); //timeOutSlot();

protected:
	// required, used to initialized the OpenGL environment
	void initializeGL();
	// used to draw in the window
	void paintGL();
	// used to reset the drawing frustum when the window is resized
	void resizeGL( int w, int h );

    void Model3DViewObject();
    void Model3DViewVectors();
	void viewTransform ();
        void GetNormal(rvector *points, rvector *normal);
	//void timeOut();

protected slots:
	// virtual void update();
	//timeOutSlot();

private:
    RotationView::referenceFrame m_mode;

	float rotX, rotY, rotInc ;
	GLfloat xRot, yRot, zRot, scale, xTrans, yTrans, zTrans; // variables to keep track of viewing position & scale
	//QTimer *m_timer;
};

#endif // MODEL3DVIEW_H
