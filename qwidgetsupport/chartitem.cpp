/*! \file chartitem.cpp
    \brief The ChartItem widget
*/

//!  \addtogroup Custom_Widgets

#include "chartitem.h"
#include "piechart.h"

ChartItem::ChartItem(QWidget *parent) :
    QWidget(parent)
{
    dataValue = 1.0;
    m_color = QColor(0, 255, 0, 255);
    m_title = objectName();
    //Add to the parent chart's items list:
    if (qobject_cast<PieChart*>(parent)!=NULL) {
        ((PieChart*)parent)->items << this;
    }
}

QString ChartItem::title() const {
    return(m_title);
}

void ChartItem::setTitle(QString name) {
    m_title = name;
}

double ChartItem::value() const {
    return(dataValue);
}

void ChartItem::setValue(double newValue) {
    if (dataValue!=newValue) parentWidget()->update();
    dataValue = newValue;
}

QColor ChartItem::color() const {
    return(m_color);
}

void ChartItem::setColor(QColor newColor) {
    if (m_color!=newColor) parentWidget()->update();
    m_color = newColor;
}
