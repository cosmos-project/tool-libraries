/*! \file model3dview.cpp
	\brief The Model3DView widget
*/

//!  \addtogroup Custom_Widgets
#include "model3dview.h"
#include "physics/physicslib.h"
#include "graphlib.h"
//#include "widgetMOST.h"

#include <QDebug>
#include <QTimer>
#include <math.h>
#include "qtsupport/cosmosdata.h"

extern COSMOSData *COSMOS;

static float MaxSize = 0.;
//GLUquadricObj* satquadric2;

//********************************************************************
//** Create a Model3DView widget
Model3DView::Model3DView(QWidget *parent) :
QOpenGLWidget(parent)
{
	//	xRot = yRot = zRot = xTrans = yTrans = zTrans = 0.0;		// default object rotation
	scale = 1.0;			// default object scale

	m_mode = RotationView::lvlh; //default mode

	QTimer *timerGUI = new QTimer(this);
	connect(timerGUI, SIGNAL(timeout()), this, SLOT(update()));
	timerGUI->start(30);

	rtri = rquad = 0.0f;

	//m_timer = new QTimer( this );
	//connect( m_timer, SIGNAL(timeout()), this, SLOT(timeOutSlot()) );
	//m_timer->start( 50 );
}


//********************************************************************//
// Release allocated resources
Model3DView::~Model3DView()
{
}

RotationView::referenceFrame Model3DView::mode() const {
	return(m_mode);
}

void Model3DView::setMode(RotationView::referenceFrame newMode) {
	m_mode = newMode;
}


//void drawSatellite::timeOut()
//{
//    //rtri += 0.5f;
//    //rquad -= 0.25f;
//
//    //updateGL();
//}

//void Model3DView::update()
//{
//	//timeOut();
//	rtri += 0.5f;
//	rquad -= 0.25f;

//	//Sleep(40);
//	updateGL();
//}


//********************************************************************
// Set up the rendering context, define display lists etc.:
void Model3DView::initializeGL()
{
	glShadeModel(GL_SMOOTH);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Let OpenGL clear to black
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST); // Enable the depth test for working in a 3D environment.
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	/*
	GLUquadricObj* tempsatquadric = gluNewQuadric();
	gluQuadricDrawStyle(tempsatquadric, GLU_FILL);
	gluQuadricNormals(tempsatquadric, GLU_SMOOTH);
	gluQuadricOrientation(tempsatquadric, GLU_OUTSIDE);
	*/


} // end initialize gl


//*********************************************************************
// Set up the OpenGL view port, matrix mode, etc.
void Model3DView::resizeGL( int width, int height )
{
	//    glViewport( 0, 0, (GLint)width, (GLint)h );
	//    glMatrixMode( GL_PROJECTION );
	//    glLoadIdentity();
	//        glFrustum( -1.0, 1.0, -1.0, 1.0, 5.0, 16.0 );
	//        //glOrtho( -1.0, 1.0, -1.0, 1.0, 5.0, 15.0 );
	//    //gluPerspective( 90.0, 1.0, 0.1, 20.0 );
	//    glMatrixMode( GL_MODELVIEW );
	//

	height = height?height:1;

	glViewport( 0, 0, (GLint)width, (GLint)height );

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//        glRotatef(90.,0.,0.,1.);

} // end resize gl


void Model3DView::Model3DViewObject()
{
	//	uint32_t i;




	//qDebug() << qPrintable( QString("x=%1, y=%2, z = %3").arg(up.col[0]) .arg(up.col[1]) .arg(up.col[2]) );
	// Determine largest panel size



	//Do each panel ().
	glEnable(GL_COLOR_MATERIAL);
    for (uint32_t i=0 ; i<COSMOS->data->piece.size() ; i++)
	{
        if (COSMOS->data->piece[i].cidx == 65535)
		{
            opengl_part(COSMOS->data->piece[i],opengl_color(1.,1.,1.,1.));
		}
		else
		{
			switch (COSMOS->data->device[COSMOS->data->piece[i].cidx].all.gen.type)
			{
			case DEVICE_TYPE_MOTR:
				if (COSMOS->data->devspec.motr[COSMOS->data->device[COSMOS->data->piece[i].cidx].all.gen.didx]->spd > .01)
                    opengl_part(COSMOS->data->piece[i],opengl_color(0.,1.,0.,1.));
				else
                    opengl_part(COSMOS->data->piece[i],opengl_color(1.,1.,1.,1.));
				break;
			case DEVICE_TYPE_THST:
				if (COSMOS->data->devspec.thst[COSMOS->data->device[COSMOS->data->piece[i].cidx].all.gen.didx]->flw > .01)
				{
                    opengl_part(COSMOS->data->piece[i],opengl_color(1.,0.,0.,1.));
				}
				else
                    opengl_part(COSMOS->data->piece[i],opengl_color(1.,1.,1.,1.));
				break;
			case DEVICE_TYPE_STRG:
                opengl_part(COSMOS->data->piece[i],opengl_color(0.,0.,1.,1.));
				break;
			default:
                opengl_part(COSMOS->data->piece[i],opengl_color(1.,1.,1.,1.));
				break;
			}
		}


	}
	glDisable(GL_COLOR_MATERIAL);
}


void Model3DView::Model3DViewVectors()
{
	rvector  sunvec ;

	glColor3f (1., 1., 1.) ;
	glEnable ( GL_COLOR_MATERIAL );


	// ***************************** Draw Sun Vector  *********************************
	// ********************************************************************************
	// transform vector to sun in ECI to vector in Body frame
    sunvec = transform_q(COSMOS->data->node.loc.att.icrf.s,rv_smult(-MaxSize,rv_normal(COSMOS->data->node.loc.pos.extra.sun2earth.s))) ;

	//	viewTransform () ;
	glColor3f (.9, 0.9, 0.) ;

	glBegin (GL_LINES) ;
	glVertex3f (0., 0., 0.);
	glVertex3f (sunvec.col[0], sunvec.col[1], sunvec.col[2]);
	glEnd () ;



	// *** draw the velocity vector in ECI space, repeating calc as we did the sun vector
	// ********************************************************************************
    if (length_rv(COSMOS->data->node.loc.pos.geoc.v) < 10. || length_rv(COSMOS->data->node.loc.pos.selc.v) < 10.)
	{
        switch (COSMOS->data->node.loc.pos.extra.closest)
		{
		case COSMOS_EARTH:
		default:
            sunvec = transform_q(COSMOS->data->node.loc.att.geoc.s,rv_smult(MaxSize,rv_normal(COSMOS->data->node.loc.pos.geoc.v))) ;
			break;
		case COSMOS_MOON:
            sunvec = transform_q(COSMOS->data->node.loc.att.selc.s,rv_smult(MaxSize,rv_normal(COSMOS->data->node.loc.pos.selc.v))) ;
			break;
		}
	}
	else
        sunvec = transform_q(COSMOS->data->node.loc.att.icrf.s,rv_smult(MaxSize,rv_normal(COSMOS->data->node.loc.pos.eci.v))) ;

//	sunvec = transform_q(COSMOS->data->node.loc.att.geoc.s,rv_smult(MaxSize,rv_unitx()));

	glColor3f (.9, 0., 0.) ;

	glBegin (GL_LINES) ;
	glVertex3f (0., 0., 0.);
	glVertex3f (sunvec.col[0], sunvec.col[1], sunvec.col[2]);
	glEnd () ;


	// *** draw the Planetary vector in ECI space, repeating calc as we did the sun vector
	// ********************************************************************************
    switch (COSMOS->data->node.loc.pos.extra.closest)
	{
	case COSMOS_EARTH:
	default:
        sunvec = transform_q(COSMOS->data->node.loc.att.geoc.s,rv_smult(-MaxSize,rv_normal(COSMOS->data->node.loc.pos.geoc.s))) ;
		break;
	case COSMOS_MOON:
        sunvec = transform_q(COSMOS->data->node.loc.att.geoc.s,rv_smult(-MaxSize,rv_normal(COSMOS->data->node.loc.pos.sci.s))) ;
		break;
	}
//	sunvec = transform_q(COSMOS->data->node.loc.att.geoc.s,rv_smult(MaxSize,rv_unity()));

	glColor3f (0., .9, 0.) ;

	glBegin (GL_LINES) ;
	glVertex3f (0., 0., 0.);
	glVertex3f (sunvec.col[0], sunvec.col[1], sunvec.col[2]);
	glEnd () ;

	// *** draw the Magnetic vector in ECI space
//	sunvec = transform_q(q_conjugate(COSMOS->data->node.loc.att.icrf.s),transform_q(COSMOS->data->node.loc.att.geoc.s,rv_smult(MaxSize,rv_normal(COSMOS->data->node.loc.bearth)))) ;
    sunvec = transform_q(COSMOS->data->node.loc.att.geoc.s,rv_smult(MaxSize,rv_normal(COSMOS->data->node.loc.bearth))) ;
//	sunvec = rv_smult(MaxSize,rv_normal(COSMOS->data->node.loc.bearth)) ;
	//	viewTransform () ;
	glColor3f (0., 0.9, .9) ;

	glBegin (GL_LINES) ;
	glVertex3f (0., 0., 0.);
	glVertex3f (sunvec.col[0], sunvec.col[1], sunvec.col[2]);
	glEnd () ;


	glDisable ( GL_COLOR_MATERIAL );

}


void Model3DView::viewTransform ()
{
	//glLoadIdentity() ;
	//glScalef (10., 10., 10.) ;
	//	glRotatef (rotX, 1, 0, 0) ;
	//	glRotatef (rotY, 0, 1, 0) ;
}


//********************************************************************
// Paint the box. The actual openGL commands for drawing the box are
// performed here.
void Model3DView::paintGL()
{
	rvector axis;
	uint32_t i, j, k;
	GLfloat amb_color [] = {0.3f, 0.3f, 0.8f, 1.0} ;
	GLfloat diff_color [] = {0.9f, 0.9f, 0.9f, 1.0} ;
	GLfloat  ambient_light[] = {0.3, 0.3, .3, 1.0};
	GLfloat   source_light[] = {1, 1, 1, 1.0};
	GLfloat light_pos[] = {2.f, 1.f, 01.f, 0.0f};

    if (COSMOS->data->node.loc.utc == 0.)
		return;

	if (MaxSize == 0.)
	{
        for( i=0 ; i<COSMOS->data->piece.size() ; i++ ) {
            for( j=0 ; j<COSMOS->data->piece[i].pnt_cnt ; j++ ) {
				for( k=0 ; k<3 ; k++ ) {
                    if( MaxSize < fabs(COSMOS->data->piece[i].points[j].col[k]) ) {
                        MaxSize = fabs(COSMOS->data->piece[i].points[j].col[k]);
					}
				}
			}
		}
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode (GL_MODELVIEW) ;
	glLoadIdentity () ;

	gluLookAt (0.,0.,4.*MaxSize, 0,0,0, 0,1,0);


	// Set up light
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMaterialfv (GL_FRONT, GL_AMBIENT, amb_color) ;
	glMaterialfv (GL_FRONT, GL_DIFFUSE, diff_color) ;
	glMaterialfv (GL_FRONT, GL_SPECULAR, ambient_light) ;
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, source_light);
	glLightfv(GL_LIGHT0, GL_SPECULAR, source_light);

    light_pos[0] = -COSMOS->data->node.loc.pos.extra.sun2earth.s.col[0];
    light_pos[1] = -COSMOS->data->node.loc.pos.extra.sun2earth.s.col[1];
    light_pos[2] = -COSMOS->data->node.loc.pos.extra.sun2earth.s.col[2];

	glLightfv(GL_LIGHT0, GL_POSITION, light_pos) ;

	glEnable (GL_LIGHTING) ;
	glEnable (GL_LIGHT0) ;
	glEnable(GL_DEPTH_TEST);

	//   glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);
	//   glEnable(GL_COLOR_MATERIAL);


	// Rotate for orientation of satellite
	// Rotate everything for desired view frame (ICRF, LVLH, Body)

	switch(m_mode) {
	case RotationView::lvlh:
        axis = rv_quaternion2axis((COSMOS->data->node.loc.att.lvlh.s));
		break;
	case RotationView::topo:
        axis = rv_quaternion2axis((COSMOS->data->node.loc.att.topo.s));
		break;
	case RotationView::inert:
        axis = rv_quaternion2axis((COSMOS->data->node.loc.att.icrf.s));
		break;
    case RotationView::geoc:
        axis = rv_quaternion2axis((COSMOS->data->node.loc.att.geoc.s));
        break;
	case RotationView::body:
		axis = rv_zero();
		break;
	}


	glPushMatrix();

	// Rotations are run in backwards order for OpenGL
	// Orient display so X is right, Y is in, Z is up
	glRotated(-90.,1.,0.,0.);

	// Orient drawing so body frame is transformed into desired frame
	glRotated(DEGOF(length_rv(axis)),axis.col[0],axis.col[1],axis.col[2]);

	// Draw vectors in body frame
	Model3DViewVectors();

	Model3DViewObject();

	glPopMatrix();

	//#ifdef _DEBUG
	GLenum error;
	while ((error = glGetError()) != GL_NO_ERROR) {
		qDebug("OpenGL Error: %s\n", (char *)
		gluErrorString(error));
	}
	//#endif

} // end paint gl


void Model3DView::GetNormal(rvector *points, rvector *normal)
{
	rvector tv0, tv1, tv2;

	tv0 = points[0];
	tv1 = points[1];
	tv2 = points[2];

	normal->col[0] = tv0.col[1]*(tv1.col[2]-tv2.col[2])+tv1.col[1]*(tv2.col[2]-tv0.col[2])+tv2.col[1]*(tv0.col[2]-tv1.col[2]);
	normal->col[1] = tv0.col[2]*(tv1.col[0]-tv2.col[0])+tv1.col[2]*(tv2.col[0]-tv0.col[0])+tv2.col[2]*(tv0.col[0]-tv1.col[0]);
	normal->col[2] = tv0.col[0]*(tv1.col[1]-tv2.col[1])+tv1.col[0]*(tv2.col[1]-tv0.col[1])+tv2.col[0]*(tv0.col[1]-tv1.col[1]);
}
