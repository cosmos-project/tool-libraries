/*! \file piechart.h
    \brief PieChart header file
*/

/*!  \addtogroup Custom_Widgets
* @{
* PieChart Class
*
* A simple pie chart that displays the that data of any child ChartItems it has.
*
* @}
*/

#ifndef PIECHART_H
#define PIECHART_H

#include <QWidget>
#include "chartitem.h"

class PieChart : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(double chartSize READ chartSize WRITE setChartSize)
    Q_PROPERTY(QColor background READ background WRITE setBackground)
    Q_PROPERTY(bool showNamesOnHover READ showNamesOnHover WRITE setShowNames)

public:
    explicit PieChart(QWidget *parent = 0);

    int itemCount();

    int updateItems();

    double chartSize() const;
    void setChartSize(double newSize);

    QColor background() const;
    void setBackground(QColor color);

    bool showNamesOnHover() const;
    void setShowNames(bool enabled);

    QList<ChartItem*> items;
    
private:
    int numItems;

    QList<int> sliceAngles;
    int chartRightEdge;

    double m_chartSize;
    QColor m_bkgcolor;
    bool m_showNames, mouseOver;
    QPoint mousePos;
    int mouseAngle;

    void updateToolTip();

public slots:
    void mouseMoveEvent(QMouseEvent * event);
    void enterEvent(QEvent * event);
    void leaveEvent(QEvent *event);
    void contextMenuEvent(QContextMenuEvent * e);

    void copyAllJSON();
	void copyJSON(/*int index*/);

protected:
    void paintEvent(QPaintEvent *);
    
};

#endif // PIECHART_H
