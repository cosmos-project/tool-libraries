/*! \file cosmosuiloader.cpp
  \brief custom ui form loading functions
*/

#include "rotationview.h"
#include "cosmosuiloader.h"
#include "cosmosdatumwdgt.h"
#include "levelbar.h"
#include "indicatorlight.h"
#include "piechart.h" // chartitem.h included within
#include "model3dview.h"
#include "vectorview.h"
#include "temperaturemonitor.h"
#include "gridflowlayout.h"
#include "drawOrbit.h"

#include <QWidget>

//! \addtogroup dynamic_ui

COSMOSUiLoader::COSMOSUiLoader(QObject *parent) :
    QUiLoader(parent)
{
}

//! Instantiate a new widget.
/*! Instantiates one of the supported custom widgets, or calls the original QUiLoader::createWidget() routine.
    \param className A string that holds the name of the class of the new widget
    \param parent A pointer to this new widget's parent
    \param name The name of the new widget
    \return A pointer to the newly created widget
*/
QWidget *COSMOSUiLoader::createWidget(const QString &className, QWidget *parent, const QString &name) {
    if (QString::compare(className, "COSMOSDatumWdgt", Qt::CaseInsensitive)==0) {
        COSMOSDatumWdgt *newWidget = new COSMOSDatumWdgt(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "IndicatorLight", Qt::CaseInsensitive)==0) {
        IndicatorLight *newWidget = new IndicatorLight(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "LevelBar", Qt::CaseSensitive)==0) {
        LevelBar *newWidget = new LevelBar(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "PieChart", Qt::CaseSensitive)==0) {
        PieChart *newWidget = new PieChart(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "ChartItem", Qt::CaseSensitive)==0) { //included in piechart.h
        ChartItem *newWidget = new ChartItem(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "RotationView", Qt::CaseSensitive)==0) {
        RotationView *newWidget = new RotationView(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "Model3DView", Qt::CaseSensitive)==0) {
        // MN: is this the same as drawSatellite.cpp?
        Model3DView *newWidget = new Model3DView(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "drawOrbit", Qt::CaseSensitive)==0) {
        drawOrbit *newWidget = new drawOrbit(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "VectorView", Qt::CaseSensitive)==0) {
        VectorView *newWidget = new VectorView(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    if (QString::compare(className, "TemperatureMonitor", Qt::CaseSensitive)==0) {
        TemperatureMonitor *newWidget = new TemperatureMonitor(parent);
        newWidget->setObjectName(name);
        return(newWidget);
    }
    return(QUiLoader::createWidget(className, parent, name));
}

QStringList COSMOSUiLoader::availableWidgets() const {
    QStringList avail = QUiLoader::availableWidgets();
    avail << "COSMOSDatumWdgt" << "LevelBar" << "IndicatorLight" << "PieChart" \
          << "ChartItem" << "RotationView" << "Model3DView" << "VectorView" \
          << "TemperatureMonitor";
    return(avail);
}

QLayout* COSMOSUiLoader::createLayout(const QString &className, QObject *parent, const QString &name) {
    if (parent==NULL) {
        if (QString::compare(className, "QHBoxLayout", Qt::CaseInsensitive)==0) {
            QHBoxLayout *newLayout = new QHBoxLayout();
            newLayout->setObjectName(name);
            return(newLayout);
        }
        if (QString::compare(className, "QVBoxLayout", Qt::CaseInsensitive)==0) {
            QVBoxLayout *newLayout = new QVBoxLayout();
            newLayout->setObjectName(name);
            return(newLayout);
        }
    }
    return(QUiLoader::createLayout(className, parent, name));
}
