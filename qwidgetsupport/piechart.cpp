/*! \file piechart.cpp
    \brief The PieChart widget
*/

//!  \addtogroup Custom_Widgets

#include "piechart.h"

#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif


PieChart::PieChart(QWidget *parent) :
    QWidget(parent)
{
    numItems = 0;
    m_chartSize = -1.0; //by default the size will be the sum of the elements.
    m_bkgcolor = QColor(120, 120, 120);
    m_showNames = true;
    setMouseTracking(true);
    mouseOver = false;
    mousePos = QPoint();
    mouseAngle = 0;
}

int PieChart::itemCount() {
    return(numItems);
}

int PieChart::updateItems() {
    int newLen = items.length();
	for (numItems=0; numItems<newLen; numItems++) {
        sliceAngles.push_front(0);
        items[numItems]->setGeometry(-10, -10, 5, 5);
    }
    return(numItems);
}

double PieChart::chartSize() const {
    return(m_chartSize);
}

void PieChart::setChartSize(double newSize) {
    if (m_chartSize!=newSize) update();
    m_chartSize = newSize;
}

QColor PieChart::background() const {
    return(m_bkgcolor);
}

void PieChart::setBackground(QColor color) {
    m_bkgcolor = color;
}

bool PieChart::showNamesOnHover() const {
    return(m_showNames);
}

void PieChart::setShowNames(bool enabled) {
    m_showNames = enabled;
}

void PieChart::paintEvent(QPaintEvent *) {
    if (items.length()!=numItems) updateItems();
    updateToolTip();
    int w=width()-2, h=height()-2, start, sweep = 0, i;
    QPainter painter(this);
    double size, val;
    size = 0.0;
    for (i=0; i<numItems; i++) {
        val = items[i]->value();
        val = val<0?0:val;
        size += val;
    }
    if (m_chartSize>size) {
        start = (90 * 16) - ((size/m_chartSize)* 180 * 16);
        size = m_chartSize;
    } else if (size<=0.000000000000001) {
        start = 90 * 16;
        size = 1.0;
    } else start = -90 * 16;
    chartRightEdge = start;
    painter.setRenderHints(QPainter::Antialiasing, true);
    painter.setBrush(m_bkgcolor);
    painter.setPen(QPen(Qt::NoPen));
    painter.drawEllipse(0, 0, w+2, h+2);
    painter.setPen(QPen(QColor(0, 0, 0), 1));
//    bool chartEmpty = true;
    for (i=0; i<numItems; i++){
        painter.setBrush(items[i]->color());
        val = items[i]->value();
        val = val<0?0:val;
        painter.drawPie(1, 1, w, h, start, (sweep =(int)((val*(360 * 16))/size + 0.5)));
//        if (sweep != 0) chartEmpty = false;
        start += sweep;
        if (start>=360 * 16) start -= 360 * 16;
        sliceAngles[i] = i==0?sweep:sliceAngles[i-1]+sweep; //remember that this records degrees (*16) away from the chartRightEdge (that's why this works)
    }
}

void PieChart::enterEvent(QEvent *) {
    mouseOver = true;
}

void PieChart::leaveEvent(QEvent *) {
    mouseOver = false;
    QToolTip::hideText();
}

void PieChart::mouseMoveEvent(QMouseEvent *event) {
    mousePos = event->globalPos();
    mouseAngle = (int)((16*180*atan2((float)((height()/2)-event->y()), (float)(event->x()-(width()/2))))/(M_PI))-chartRightEdge;
    if (mouseAngle<0) mouseAngle += 360 * 16;
    updateToolTip();
}

void PieChart::updateToolTip() {
    if (mouseOver) {
        int i;
        for(i=0; i<numItems; i++) {
            if (mouseAngle<=sliceAngles[i]) {
                QToolTip::showText(mousePos, QString("%1: %2 ").arg(items[i]->title()).arg(items[i]->value()));
                QToolTip::showText(mousePos, QString("%1: %2").arg(items[i]->title()).arg(items[i]->value()));
                break;
            }
        }
    }
}

void PieChart::contextMenuEvent(QContextMenuEvent *e) {
    QMenu *menu = new QMenu(this);
    menu->addAction("Copy all data");
    connect(menu->actions().last(), SIGNAL(triggered()), this, SLOT(copyAllJSON()));
    QSignalMapper *mapper = new QSignalMapper(this);
    int i;
    for (i=0; i<numItems; i++) {
        menu->addAction(QString("Copy %1 data").arg(items[i]->title()));
        mapper->setMapping(menu->actions().last(), i);
        connect(menu->actions().last(), SIGNAL(triggered()), mapper, SLOT(map()));
        connect(mapper, SIGNAL(mapped(int)), this, SLOT(copyJSON(int)));
    }
    menu->popup(e->globalPos());
}

void PieChart::copyAllJSON() {
    QString jsonNames = QString();
	//int i;
    //for (i=0; i<numItems; i++) jsonNames.append((items[i]->valueName()).append("\n"));
    QApplication::clipboard()->setText(jsonNames);
}

void PieChart::copyJSON(/*int index*/) {
    //QApplication::clipboard()->setText(items[index]->valueName());
}
