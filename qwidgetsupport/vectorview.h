/*! \file vectorview.h
    \brief VectorView header file
*/

/*!  \addtogroup Custom_Widgets
* @{
* VectorView Class
*
* Displays the values of the components of a vector.
*
* @}
*/

#ifndef VECTORVIEW_H
#define VECTORVIEW_H

#include "math/mathlib.h"

#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif

class VectorView : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString dataJSON READ dataJSON WRITE setDataJSON)

public:
    explicit VectorView(QWidget *parent = 0);
    QString dataJSON() const;
    void setDataJSON(QString JSONdata);

    void updateVector(rvector *vecPtr);
    
private:

    QString m_dataJSON;

signals:
    
public slots:
    
};

#endif // VECTORVIEW_H
