#ifndef COSMOSDATUM_H
#define COSMOSDATUM_H

#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif

class COSMOSDatumWdgt : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString valueName READ valueName WRITE setValueName NOTIFY valueNameChanged)
    Q_PROPERTY(QString minName READ minName WRITE setMinName NOTIFY minNameChanged)
    Q_PROPERTY(QString maxName READ maxName WRITE setMaxName NOTIFY maxNameChanged)
public:
    explicit COSMOSDatumWdgt(QWidget *parent = 0);

    QString valueName() const;
    void setValueName(QString jsonName);

    QString minName() const;
    void setMinName(QString jsonName);

    QString maxName() const;
    void setMaxName(QString jsonName);

    void setParent(QWidget *parent);
    
private:
    QString m_valueName;
    void *m_value;
    void *value_entry;
    bool valueIsEq;
    QString m_minName;
    void *m_min;
    void *min_entry;
    bool minIsEq;
    QString m_maxName;
    void *m_max;
    void *max_entry;
    bool maxIsEq;

    Qt::ContextMenuPolicy oldPolicy;
    void initForNewParent(QWidget *parent);

public slots:
    virtual void updateDisplay();
    void copyValueName();
    void copyMinName();
    void copyMaxName();
    void displayDialog(const QPoint & pos);

signals:
    void valueNameChanged();
    void minNameChanged();
    void maxNameChanged();

protected:
    void* valuePointer();
    void* valueJSONEntry();
    void* minPointer();
    void* minJSONEntry();
    void* maxPointer();
    void* maxJSONEntry();
    
};

#endif // COSMOSDATUM_H
