/*! \file rotationview.h
    \brief RotationView header file
*/

/*!  \addtogroup Custom_Widgets
* @{
* RotationView Class
*
* Allows the user to view a rotation numerically via several different representations.
*
* @}
*/

#ifndef ROTATIONVIEW_H
#define ROTATIONVIEW_H

#include "math/mathlib.h"
#include "convertdef.h"
#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif

#include <QToolButton>
#include <QLabel>
#include <QLineEdit>

Q_DECLARE_METATYPE(qatt*)

class RotationView : public QWidget
{
    Q_OBJECT

    Q_ENUMS(displayMode)
    Q_ENUMS(referenceFrame)

    Q_PROPERTY(quaternion* attitude READ attitude WRITE setAttitude) //the current rotation
    Q_PROPERTY(rvector* derivative READ derivative WRITE setDerivative) //either the first or second derivative of the attitude
    Q_PROPERTY(displayMode mode READ mode WRITE setMode)
    Q_PROPERTY(bool showLabels READ labelsShown WRITE showLabels)
    Q_PROPERTY(bool modeLocked READ modeLocked WRITE lockMode) //can the user change modes
    Q_PROPERTY(QString quatXLabel READ quatXLabel WRITE setQuatXLabel)
    Q_PROPERTY(QString quatYLabel READ quatYLabel WRITE setQuatYLabel)
    Q_PROPERTY(QString quatZLabel READ quatZLabel WRITE setQuatZLabel)
    Q_PROPERTY(QString quatWLabel READ quatWLabel WRITE setQuatWLabel)
    Q_PROPERTY(QString eulerHLabel READ eulerHLabel WRITE setEulerHLabel)
    Q_PROPERTY(QString eulerELabel READ eulerELabel WRITE setEulerELabel)
    Q_PROPERTY(QString eulerBLabel READ eulerBLabel WRITE setEulerBLabel)
    Q_PROPERTY(QString angleLabel READ angleLabel WRITE setAngleLabel)
    Q_PROPERTY(QString axisXLabel READ axisXLabel WRITE setAxisXLabel)
    Q_PROPERTY(QString axisYLabel READ axisYLabel WRITE setAxisYLabel)
    Q_PROPERTY(QString axisZLabel READ axisZLabel WRITE setAxisZLabel)

public:
    enum displayMode {quat, dcm, euler, angleAxis};
    enum referenceFrame{lvlh, topo, inert, body, geoc};

    explicit RotationView(QWidget *parent = 0);

    Q_INVOKABLE void linkTo(RotationView* view); //makes the mode of THIS view be controlled by ANOTHER view
    QList<RotationView*> linkedViews;

    quaternion* attitude() const;
    void setAttitude(quaternion* attPtr);

    rvector* derivative() const;
    void setDerivative(rvector* derivPtr);

    displayMode mode() const;
    void setMode(displayMode newMode);

    bool labelsShown() const;
    void showLabels(bool show);

    bool modeLocked() const;
    void lockMode(bool lock);

    bool useRadians() const;
    void setRadians(bool radians);

    QString quatXLabel() const;
    void setQuatXLabel(QString text);
    QString quatYLabel() const;
    void setQuatYLabel(QString text);
    QString quatZLabel() const;
    void setQuatZLabel(QString text);
    QString quatWLabel() const;
    void setQuatWLabel(QString text);
    QString eulerHLabel() const;
    void setEulerHLabel(QString text);
    QString eulerELabel() const;
    void setEulerELabel(QString text);
    QString eulerBLabel() const;
    void setEulerBLabel(QString text);
    QString axisXLabel() const;
    void setAxisXLabel(QString text);
    QString axisYLabel() const;
    void setAxisYLabel(QString text);
    QString axisZLabel() const;
    void setAxisZLabel(QString text);
    QString angleLabel() const;
    void setAngleLabel(QString text);
    
private:

    void updateMouseOver();

    //Ui elements:
    QToolButton *menuButton;
    QMenu *modeMenu;
    QWidget *quatBox;
    QLineEdit *quatX;
    QLineEdit *quatY;
    QLineEdit *quatZ;
    QLineEdit *quatW;
    QLabel *XLabel;
    QLabel *YLabel;
    QLabel *ZLabel;
    QLabel *WLabel;
    QWidget *dcmBox;
    QLabel *dcm1_1;
    QLabel *dcm1_2;
    QLabel *dcm1_3;
    QLabel *dcm2_1;
    QLabel *dcm2_2;
    QLabel *dcm2_3;
    QLabel *dcm3_1;
    QLabel *dcm3_2;
    QLabel *dcm3_3;
    QWidget *eulerBox;
    QLineEdit *eulerH;
    QLineEdit *eulerE;
    QLineEdit *eulerB;
    QLabel *HLabel;
    QLabel *ELabel;
    QLabel *BLabel;
    QWidget *axisAngleBox;
    QLineEdit *axisX;
    QLineEdit *axisY;
    QLineEdit *axisZ;
    QLineEdit *axisAngle;
    QLabel *axisLabelX;
    QLabel *axisLabelY;
    QLabel *axisLabelZ;
    QLabel *labelAngle;

    //miscellaneous variables
    rmatrix dcmRotation;
    bool mouseOver;
    QPoint mousePos;
    RotationView *controller;

    //property variables
    quaternion* m_rotation;
    rvector* m_derivatives;
    displayMode m_mode;
    bool m_showLabels;
    bool m_modeLocked;
    bool m_radians;
    
public slots:
    void switchToQuat();
    void switchToDCM();
    void switchToEuler();
    void switchToAngleAxis();

    void mouseMoveEvent(QMouseEvent * event);
    void enterEvent(QEvent * event);
    void leaveEvent(QEvent *event);
    
protected:
    void resizeEvent(QResizeEvent *);

};

#endif // ROTATIONVIEW_H
