/*! \file cosmosuiloader.h
    \brief cosmosuiloader include file
*/

/*! \addtogroup dynamic_ui
* @{
* COSMOSUiLoader Class
*
* This class is an extension of Qt's QUiLoader class, which loads uiforms at runtime.
* Here the createWidget function has been subclassed so that it can create custom widgets
* without a custom widget plugin.  The consequence of this is that createWidget must be
* updated whenever a new widget is made with a case to support that particular widget.
*
* @}
*/

#ifndef COSMOSUILOADER_H
#define COSMOSUILOADER_H

#include <QtUiTools/QUiLoader>

class COSMOSUiLoader : public QUiLoader
{
    Q_OBJECT
public:
    explicit COSMOSUiLoader(QObject *parent = 0);

    QWidget *createWidget(const QString &className, QWidget *parent, const QString &name);
    QStringList availableWidgets() const;

    QLayout *createLayout(const QString &className, QObject *parent, const QString &name);

    int counter;
    
signals:
    
public slots:
    
};

#endif // COSMOSUILOADER_H
