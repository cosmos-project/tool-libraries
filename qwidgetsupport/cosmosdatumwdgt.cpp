#include "support/jsonlib.h"
#include "cosmosdatumwdgt.h"

#include "levelbar.h"
#include "piechart.h"
#include "indicatorlight.h"
#include "temperaturemonitor.h"
#include "vectorview.h"
#include "rotationview.h"
#include "qtsupport/cosmosdata.h"

extern COSMOSData *COSMOS;

COSMOSDatumWdgt::COSMOSDatumWdgt(QWidget *parent) :
    QWidget(parent)
{
    m_valueName = m_minName = m_maxName = QString();
    m_value = m_min = m_max = NULL;
    value_entry = min_entry = max_entry = NULL;
    valueIsEq = minIsEq = maxIsEq = false;
    addAction(new QAction("Copy namespace name", this));
    connect(actions().last(), SIGNAL(triggered()), this, SLOT(copyValueName()));
    connect(COSMOS, SIGNAL(dataupdate()), this, SLOT(updateDisplay()));
    initForNewParent(parent);
    hide();
}

void COSMOSDatumWdgt::setParent(QWidget *parent) {
    if (parentWidget()!=NULL) {
        disconnect(parentWidget(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(displayDialog(QPoint)));
        parentWidget()->setContextMenuPolicy(oldPolicy);
    }
    QWidget::setParent(parent);
    initForNewParent(parent);
}

void COSMOSDatumWdgt::initForNewParent(QWidget *parent) {
    if (parent==NULL) {
        oldPolicy = Qt::DefaultContextMenu;
        return;
    }
    oldPolicy = parent->contextMenuPolicy();
    if (qobject_cast<QLineEdit*>(parent)!=NULL);
    else if (qobject_cast<TemperatureMonitor*>(parent)!=NULL);
    else if (qobject_cast<LevelBar*>(parent)!=NULL);
    else if (qobject_cast<VectorView*>(parent)!=NULL);
    else if (qobject_cast<IndicatorLight*>(parent)!=NULL);
    else if (qobject_cast<RotationView*>(parent)!=NULL);
    else if (qobject_cast<ChartItem*>(parent)!=NULL);
    else if (qobject_cast<PieChart*>(parent)!=NULL);
    else {
        //Not a supported widget
        return;
    }
    //Operations common to all supported widgets
    connect(parent, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(displayDialog(QPoint)));
    parent->setContextMenuPolicy(Qt::CustomContextMenu);
}

QString COSMOSDatumWdgt::valueName() const {
    return m_valueName;
}
void COSMOSDatumWdgt::setValueName(QString jsonName) {
    m_valueName = jsonName;
    if (!m_valueName.isEmpty()) {
        QByteArray namestring = m_valueName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        jsonentry *ptr;
        if ((ptr=json_entry_of(tempPointer,  *COSMOS->meta)) == NULL) {
            m_value = NULL;
        } else {
            if (ptr->type==JSON_TYPE_EQUATION) m_value = new double(json_equation(tempPointer,   *COSMOS->meta, *COSMOS->data)); //could cause some sort of error if namestring terminates before final close paren...
            else m_value = json_ptr_of_offset(ptr->offset,ptr->group,  *COSMOS->meta, *COSMOS->data);
            value_entry = (void*)ptr;
        }
    }
    emit valueNameChanged();
}

void* COSMOSDatumWdgt::valuePointer() {
    if (value_entry==NULL) return NULL;
    if (((jsonentry*)value_entry)->type==JSON_TYPE_EQUATION) {
        QByteArray namestring = m_valueName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        *(double*)m_value = json_equation(tempPointer,   *COSMOS->meta, *COSMOS->data); //Re-evaluate the equation
    }
    return m_value;
}

void* COSMOSDatumWdgt::valueJSONEntry() {
    return value_entry;
}

void COSMOSDatumWdgt::copyValueName() {
    QApplication::clipboard()->setText(m_valueName);
}

QString COSMOSDatumWdgt::minName() const {
    return m_minName;
}
void COSMOSDatumWdgt::setMinName(QString jsonName) {
    m_minName = jsonName;
    if (!m_minName.isEmpty()) {
        if (m_min==NULL) { //Add an action to copy this JSON name
            addAction(new QAction("Copy namespace name of min", this));
            connect(actions().last(), SIGNAL(triggered()), this, SLOT(copyMinName()));
        }
        QByteArray namestring = m_minName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        jsonentry *ptr;
        if ((ptr=json_entry_of(tempPointer,  *COSMOS->meta)) == NULL) m_min = NULL;
        else {
            if (ptr->type==JSON_TYPE_EQUATION) m_min = new double(json_equation(tempPointer,   *COSMOS->meta, *COSMOS->data)); //could cause some sort of error if namestring terminates before final close paren...
            else m_min = json_ptr_of_offset(ptr->offset,ptr->group,  *COSMOS->meta, *COSMOS->data);
            min_entry = (void*)ptr;
        }
    }
    emit minNameChanged();
}

void* COSMOSDatumWdgt::minPointer() {
    if (min_entry==NULL) return NULL;
    if (((jsonentry*)min_entry)->type==JSON_TYPE_EQUATION) {
        QByteArray namestring = m_minName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        *(double*)m_min = json_equation(tempPointer,   *COSMOS->meta, *COSMOS->data);
    }
    return m_min;
}

void* COSMOSDatumWdgt::minJSONEntry() {
    return min_entry;
}

void COSMOSDatumWdgt::copyMinName() {
    QApplication::clipboard()->setText(m_minName);
}

QString COSMOSDatumWdgt::maxName() const {
    return m_maxName;
}
void COSMOSDatumWdgt::setMaxName(QString jsonName) {
    m_maxName = jsonName;
    if (!m_maxName.isEmpty()) {
        if (m_max==NULL) { //Add an action to copy this JSON name
            addAction(new QAction("Copy namespace name of max", this));
            connect(actions().last(), SIGNAL(triggered()), this, SLOT(copyMaxName()));
        }
        QByteArray namestring = m_maxName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        jsonentry *ptr;
        if ((ptr=json_entry_of(tempPointer,  *COSMOS->meta)) == NULL) m_max = NULL;
        else {
            if (ptr->type==JSON_TYPE_EQUATION) m_max = new double(json_equation(tempPointer,   *COSMOS->meta, *COSMOS->data)); //could cause some sort of error if namestring terminates before final close paren...
            else m_max = json_ptr_of_offset(ptr->offset,ptr->group,  *COSMOS->meta, *COSMOS->data);
            max_entry = (void*)ptr;
        }
    }
    emit maxNameChanged();
}

void* COSMOSDatumWdgt::maxPointer() {
    if (max_entry==NULL) return NULL;
    if (((jsonentry*)max_entry)->type==JSON_TYPE_EQUATION) {
        QByteArray namestring = m_maxName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        *(double*)m_max = json_equation(tempPointer,   *COSMOS->meta, *COSMOS->data);
    }
    return m_max;
}

void* COSMOSDatumWdgt::maxJSONEntry() {
    return max_entry;
}

void COSMOSDatumWdgt::copyMaxName() {
    QApplication::clipboard()->setText(m_maxName);
}

void COSMOSDatumWdgt::updateDisplay() {
    if (parent()==NULL) return;
    QWidget *obj;
    if ((obj = qobject_cast<QLineEdit*>(parent()))!=NULL) {
        if (value_entry!=NULL) {
            if (((jsonentry*)value_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)value_entry)->type==JSON_TYPE_DOUBLE) {
                ((QLineEdit*)obj)->setText(QString::number(*(double*)m_value));
            } else if (((jsonentry*)value_entry)->type==JSON_TYPE_FLOAT) ((QLineEdit*)obj)->setText(QString::number(*(float*)m_value));
        }
        return;
    }
    if ((obj = qobject_cast<TemperatureMonitor*>(parent()))!=NULL) {
        if (value_entry!=NULL) {
            TemperatureMonitor *tmp = (TemperatureMonitor*)obj;
            if (((jsonentry*)value_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)value_entry)->type==JSON_TYPE_DOUBLE) {
                tmp->updateTemp(*(double*)m_value);
            } else if (((jsonentry*)value_entry)->type==JSON_TYPE_FLOAT) tmp->updateTemp(*(float*)m_value);
            if (min_entry!=NULL) {
                if (((jsonentry*)min_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)min_entry)->type==JSON_TYPE_DOUBLE) {
                    tmp->setMinTemp(*(double*)m_min);
                } else if (((jsonentry*)min_entry)->type==JSON_TYPE_FLOAT) tmp->setMinTemp(*(float*)m_min);
            }
            if (max_entry!=NULL) {
                if (((jsonentry*)max_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)max_entry)->type==JSON_TYPE_DOUBLE) {
                    tmp->setMaxTemp(*(double*)m_max);
                } else if (((jsonentry*)max_entry)->type==JSON_TYPE_FLOAT) tmp->setMaxTemp(*(float*)m_max);
            }
        }
        return;
    }
    if ((obj = qobject_cast<LevelBar*>(parent()))!=NULL) {
        LevelBar *lvl = (LevelBar*)obj;
        double quotient;
        if (value_entry!=NULL&&max_entry!=NULL) {
            if (((jsonentry*)value_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)value_entry)->type==JSON_TYPE_DOUBLE) {
                quotient = *(double*)m_value;
            } else if (((jsonentry*)value_entry)->type==JSON_TYPE_FLOAT) quotient = *(float*)m_value;
            else quotient = 0;
            if (((jsonentry*)max_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)max_entry)->type==JSON_TYPE_DOUBLE) {
                quotient /= *(double*)m_max;
            } else if (((jsonentry*)max_entry)->type==JSON_TYPE_FLOAT) quotient /= *(float*)m_max;
            else quotient = 0;
        } else quotient = 0.0;
        lvl->setLevel(quotient);
        return;
    }
    if ((obj = qobject_cast<VectorView*>(parent()))!=NULL) {
        return;
    }
    if ((obj = qobject_cast<IndicatorLight*>(parent()))!=NULL) {
        if (value_entry!=NULL) {
            IndicatorLight *light = (IndicatorLight*)obj;
            if (((jsonentry*)value_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)value_entry)->type==JSON_TYPE_DOUBLE) {
                light->setState(*(double*)m_value);
            } else if (((jsonentry*)value_entry)->type==JSON_TYPE_FLOAT) light->setState(*(float*)m_value);
            else if ((((jsonentry*)value_entry)->type==JSON_TYPE_INT16||((jsonentry*)value_entry)->type==JSON_TYPE_INT32)||(((jsonentry*)value_entry)->type==JSON_TYPE_UINT16||((jsonentry*)value_entry)->type==JSON_TYPE_UINT32)) {
                light->setState(*(int*)m_value);
            }
        }
        return;
    }
    if ((obj = qobject_cast<RotationView*>(parent()))!=NULL) {
        if (value_entry!=NULL) {
            if (((jsonentry*)value_entry)->type==JSON_TYPE_QUATERNION) {
                ((RotationView*)obj)->setAttitude((quaternion*)m_value);
            } else if (((jsonentry*)value_entry)->type==JSON_TYPE_RVECTOR) {
                ((RotationView*)obj)->setDerivative((rvector*)m_value);
            }
        }
        return;
    }
    if ((obj = qobject_cast<ChartItem*>(parent()))!=NULL) {
        ChartItem *itm = (ChartItem*)obj;
        if (value_entry!=NULL) {
            if (((jsonentry*)value_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)value_entry)->type==JSON_TYPE_DOUBLE) {
                itm->setValue(*(double*)m_value);
            } else if (((jsonentry*)value_entry)->type==JSON_TYPE_FLOAT) itm->setValue(*(float*)m_value);
        }
        if (max_entry!=NULL) {
            //ChartItem and PieChart don't yet support this, I'm workin' on it!
        }
        return;
    }
    if ((obj = qobject_cast<PieChart*>(parent()))!=NULL) {
        PieChart *chart = (PieChart*)obj;
        if (max_entry!=NULL&&value_entry==NULL) {
            if (((jsonentry*)max_entry)->type==JSON_TYPE_EQUATION||((jsonentry*)max_entry)->type==JSON_TYPE_DOUBLE) {
                chart->setChartSize(*(double*)m_max);
            } else if (((jsonentry*)max_entry)->type==JSON_TYPE_FLOAT) chart->setChartSize(*(float*)m_max);
        } else { // place inside ChartItems and add to PieChart
            ChartItem *itm = new ChartItem(chart);
            this->setParent(itm);
            updateDisplay(); //Now run again to update the value for the ChartItem
        }
        return;
    }
}

void COSMOSDatumWdgt::displayDialog(const QPoint &pos) {
    //will display the dialog for this cosmos datum.
    QMenu *menu = new QMenu(this);
    menu->addActions(actions());
    menu->popup(qobject_cast<QWidget*>(parent())->mapToGlobal(pos));
}
