#include "plot.h"

Plot::Plot(QWidget *parent) :
    QWidget(parent)
{

    //QWidget *window = new QWidget;

    // set v
    plot->legend->setVisible(true);
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    //QGroupBox *plotGroupBox = new QGroupBox;
    QVBoxLayout *plot_layout = new QVBoxLayout;

    plot_layout->addWidget(plot);
    //plotGroupBox->setLayout(vbox1);

    setLayout(plot_layout);

}

Plot::~Plot()
{
    //delete ui;
}

void Plot::setAxisTitles(QString x_axis_title, QString y_axis_title){
    QFont font = plot->font();
    font.setPixelSize(20);

    plot->xAxis->setLabelFont(font);
    plot->yAxis->setLabelFont(font);

    plot->xAxis->setLabel(x_axis_title);
    plot->yAxis->setLabel(y_axis_title);
}


void Plot::setAxisTitles(QString x_axis_title, QString y_axis_title, int fontSize){
    QFont font = plot->font();
    font.setPixelSize(fontSize);

    plot->xAxis->setLabelFont(font);
    plot->yAxis->setLabelFont(font);

    plot->xAxis->setLabel(x_axis_title);
    plot->yAxis->setLabel(y_axis_title);
}

void Plot::addData(QVector<double> x, QVector<double> y, QString name, QColor color){

    //cout << "plot count:" << plot_count << endl;
    //plot->clearGraphs();
    plot->addGraph();
    plot->graph(plot_count)->setPen(QPen(color));
    plot->graph(plot_count)->setData(x, y);
    plot->graph(plot_count)->setName(name);

    plot_count ++;

    // add to stackY
//    push2StackX(x.toStdVector());
//    push2StackY(y.toStdVector());

    stackX = x.toStdVector();
    stackY = y.toStdVector();
}

void Plot::addDataFromStdVector(vector<double> x, vector<double> y,
                                QString name, QColor color){

    //cout << "plot count:" << plot_count << endl;
    //plot->clearGraphs();
    plot->addGraph();
    plot->graph(plot_count)->setPen(QPen(color));
    plot->graph(plot_count)->setData(QVector<double>::fromStdVector(x),
                                     QVector<double>::fromStdVector(y));
    plot->graph(plot_count)->setName(name);

    plot_count ++;

    // add to stackY
    push2StackX(x);
    push2StackY(y);

}

void Plot::setAxesRange(float xmin, float xmax, float ymin, float ymax){
    // set axes ranges, so we can see all the data:
    plot->xAxis->setRange(xmin, xmax);
    plot->yAxis->setRange(ymin, ymax);
}

void Plot::setAxesRangeAuto(){

    float offsetPercetage = 0.1;
    float rangeX = getMaxX() - getMinX();
    float rangeY = getMaxY() - getMinY();
    setAxesRange(getMinX() - rangeX*offsetPercetage,
                 getMaxX() + rangeX*offsetPercetage,
                 getMinY() - rangeY*offsetPercetage,
                 getMaxY() + rangeY*offsetPercetage);
}



double Plot::getMaxY(){
    return *max_element(begin(stackY), end(stackY));
}

double Plot::getMinY(){
    return *min_element(begin(stackY), end(stackY));
}

double Plot::getMaxX(){
    return *max_element(begin(stackX), end(stackX));
}

double Plot::getMinX(){
    return *min_element(begin(stackX), end(stackX));
}


double Plot::getMaxOf(vector<double> &v){
    return *max_element(begin(v), end(v));
}

double Plot::getMinOf(vector<double> &v){
    return *min_element(begin(v), end(v));
}

void Plot::push2StackY(vector<double> &v){
    stackY.push_back(getMaxOf(v));
    stackY.push_back(getMinOf(v));
}

void Plot::push2StackX(vector<double> &v){
    stackX.push_back(getMaxOf(v));
    stackX.push_back(getMinOf(v));
}


void Plot::setFinal(){

    //    //plot->replot();


    //    //QGroupBox *plotGroupBox = new QGroupBox;
    //    QVBoxLayout *plot_layout = new QVBoxLayout;

    //    plot_layout->addWidget(plot);
    //    //plotGroupBox->setLayout(vbox1);

    //    setLayout(plot_layout);
}

void Plot::clear(){
//    plot->clearItems();
    // delete the stacks so that when we clear the
    // auto axis can compute the new stack correctly
    stackX.clear();
    stackY.clear();
    plot->clearGraphs();
    //plot->repaint();
    //plot->replot();
    plot_count = 0;

}

void Plot::refresh(){
    plot->repaint();
    plot->replot();
}

void Plot::save(){
    // save to pdf does not work very well in this version on QCustomPlot
    //plot->savePdf("plot.pdf",false,500,1000);
    plot->savePng("plot.png",1000,500);
}
