#ifndef PLOT_H
#define PLOT_H

// Qt libs
#include <QGroupBox>
#include <QVBoxLayout>
#include <QWidget>

// C++ libs
#include <iostream>
using namespace std;

#include "qcustomplot/qcustomplot.h" // the header file of QCustomPlot. Don't forget to add it to your project, if you use an IDE, so it gets compiled.

class Plot : public QWidget
{
    Q_OBJECT

public:
    explicit Plot(QWidget *parent = 0);
    ~Plot();
    void setAxisTitles(QString x_axis_title, QString y_axis_title);
    void setAxisTitles(QString x_axis_title, QString y_axis_title, int fontSize);

    void addData(QVector<double> x, QVector<double> y, QString name = "", QColor color = Qt::blue);
    void setFinal();
    void setAxesRange(float xmin = -1, float xmax = 1, float ymin = -1, float ymax = 1);

    void clear();
    void refresh();
    void addDataFromStdVector(vector<double> x, vector<double> y, QString name, QColor color);
    void save();
    void push2StackY(vector<double> &v);
    void push2StackX(vector<double> &v);
    double getMinY();
    double getMaxY();
    double getMinX();
    double getMaxX();
    double getMinOf(vector<double> &v);
    double getMaxOf(vector<double> &v);
    void setAxesRangeAuto();
private:
//    Ui::Plot *ui;
    QCustomPlot* plot = new QCustomPlot(this);
    // count the number of plots
    int plot_count = 0;
    vector<double> stackY;
    vector<double> stackX;

};

#endif // PLOT_H
