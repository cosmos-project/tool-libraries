message(" -- cosmos/tools/libraries/modules.pri --")

#message("")
#message("Tools Libraries Modules >>")

#--------------------------------------------------------------------
#add COSMOS/tools/libraries to the path
COSMOS_TOOLS_LIBRARIES = $$COSMOS_SOURCE/tools/libraries/

INCLUDEPATH     += $$COSMOS_TOOLS_LIBRARIES


contains(MODULES, qtsupport){
    message( "- source/tools/libraries/qtsupport" )
    INCLUDEPATH     += $$COSMOS_TOOLS_LIBRARIES/qtsupport
    MODULES += cosmosdata
    MODULES += cosmosdatum
    MODULES += event
}

contains(MODULES, cosmosdata){
    message( "- tools/libraries/qtsupport/cosmosdata" )
    INCLUDEPATH     += $$COSMOS_TOOLS_LIBRARIES/qtsupport
    SOURCES += $$COSMOS_TOOLS_LIBRARIES/qtsupport/cosmosdata.cpp
    HEADERS += $$COSMOS_TOOLS_LIBRARIES/qtsupport/cosmosdata.h
}

contains(MODULES, cosmosdatum){
    message( "- tools/libraries/qtsupport/cosmosdatum" )
    INCLUDEPATH     += $$COSMOS_TOOLS_LIBRARIES/qtsupport
    SOURCES += $$COSMOS_TOOLS_LIBRARIES/qtsupport/cosmosdatum.cpp
    HEADERS += $$COSMOS_TOOLS_LIBRARIES/qtsupport/cosmosdatum.h
}

contains(MODULES, event){
    message( "- tools/libraries/qtsupport/event" )
    INCLUDEPATH     += $$COSMOS_TOOLS_LIBRARIES/qtsupport
    SOURCES += $$COSMOS_TOOLS_LIBRARIES/qtsupport/event.cpp
    HEADERS += $$COSMOS_TOOLS_LIBRARIES/qtsupport/event.h
}

contains(MODULES, qmlsupport){
    message( "- tools/libraries/qmlsupport" )
    INCLUDEPATH     += $$COSMOS_TOOLS_LIBRARIES/qmlsupport
    MODULES += sharedobjectlist
    MODULES += miscqmlsupport
}

contains(MODULES, sharedobjectlist){
    message( "- tools/libraries/qmlsupport/sharedobjectlist" )
    INCLUDEPATH     += $$COSMOS_TOOLS_LIBRARIES/qmlsupport
    SOURCES += $$COSMOS_TOOLS_LIBRARIES/qmlsupport/sharedobjectlist.cpp
    HEADERS += $$COSMOS_TOOLS_LIBRARIES/qmlsupport/sharedobjectlist.h
}

contains(MODULES, miscqmlsupport){
    message( "- tools/libraries/qmlsupport/miscqmlsupport" )
    INCLUDEPATH     += $$COSMOS_TOOLS_LIBRARIES/qmlsupport
    SOURCES += $$COSMOS_TOOLS_LIBRARIES/qmlsupport/miscqmlsupport.cpp
    HEADERS += $$COSMOS_TOOLS_LIBRARIES/qmlsupport/miscqmlsupport.h
}

contains(MODULES, qtgl){
    message( "- tools/libraries/qtgl" )
    INCLUDEPATH += $$COSMOS_TOOLS_LIBRARIES/qtgl
    SOURCES     += $$files($$COSMOS_TOOLS_LIBRARIES/qtgl/*.cpp)
    HEADERS     += $$files($$COSMOS_TOOLS_LIBRARIES/qtgl/*.h)
}

contains(MODULES, glPrimitives){
    message( "- glPrimitives" )
    INCLUDEPATH += $$COSMOS_TOOLS_LIBRARIES/qtgl
    SOURCES     += $$COSMOS_TOOLS_LIBRARIES/qtgl/glPrimitives.cpp
    HEADERS     += $$COSMOS_TOOLS_LIBRARIES/qtgl/glPrimitives.h
}

contains(MODULES, glModel3D){
    message( "- glModel3D" )
    INCLUDEPATH += $$COSMOS_TOOLS_LIBRARIES/qtgl
    SOURCES     += $$COSMOS_TOOLS_LIBRARIES/qtgl/glModel3D.cpp
    HEADERS     += $$COSMOS_TOOLS_LIBRARIES/qtgl/glModel3D.h
}

contains(MODULES, qtplot){
    message( "- qtplot" )
    INCLUDEPATH += $$COSMOS_TOOLS_LIBRARIES/qtplot
    SOURCES     += $$COSMOS_TOOLS_LIBRARIES/qtplot/plot.cpp
    HEADERS     += $$COSMOS_TOOLS_LIBRARIES/qtplot/plot.h
}


message(" ")
