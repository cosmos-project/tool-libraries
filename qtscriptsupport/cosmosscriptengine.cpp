#include "cosmosscriptengine.h"
#include "qwidgetsupport/cosmosuiloader.h"

#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif

#include <QList>
#include <QLabel>
#include <QPushButton>
#include <QMenu>
#include <QComboBox>
#include <QLayoutItem>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "qwidgetsupport/temperaturemonitor.h"
#include "qwidgetsupport/cosmosdatumwdgt.h"

Q_DECLARE_METATYPE(QPushButton*)
Q_DECLARE_METATYPE(QLabel*)
Q_DECLARE_METATYPE(QMenu*)
Q_DECLARE_METATYPE(TemperatureMonitor*)
Q_DECLARE_METATYPE(QComboBox*)

Q_DECLARE_METATYPE(COSMOSDatumWdgt*)

Q_DECLARE_METATYPE(QLayoutItem*)
Q_DECLARE_METATYPE(QLayout*)
Q_DECLARE_METATYPE(QBoxLayout*)
Q_DECLARE_METATYPE(QVBoxLayout*)
Q_DECLARE_METATYPE(QHBoxLayout*)

Q_DECLARE_METATYPE(QRect)
Q_DECLARE_METATYPE(QRect*)

COSMOSScriptEngine::COSMOSScriptEngine(QObject *parent) :
    QScriptEngine(parent)
{
    installTranslatorFunctions();
}

//Utilities:

//Allows certian script functions to be blocked temporarily or perminantly
QScriptValue blockedFunct(QScriptContext* context, QScriptEngine*) {
    return context->throwError("This function is blocked within this frame.");
}
QScriptValue COSMOSScriptEngine::BlockedFunction() {
    return newFunction(blockedFunct);
}

void COSMOSScriptEngine::implementFullBindings() {
    implementQObjects();
    implementNonQObjects();
}

//QObject classes:

QScriptValue loadUI(QScriptContext *context, QScriptEngine *engine) {
    if (!(context->argumentCount()==1||context->argumentCount()==2)) return context->throwError("loadUI takes 1 or 2 arguments");
    COSMOSUiLoader loader;
    QFile file(context->argument(0).toString());
    if (!file.open(QIODevice::ReadOnly)) return context->throwError("Unable to read file");
    QWidget *w;
    if (context->argumentCount()==2) {
        QObject *p = context->argument(1).toQObject();
        w = loader.load(&file, (QWidget*)p);
    } else w = loader.load(&file);
    w->show();
    return engine->toScriptValue(w);
}

QScriptValue className(QScriptContext *context, QScriptEngine *engine) {
    if (context->argumentCount()!=1) return context->throwError("This function takes 1 argument");
    QObject *obj = context->argument(0).toQObject();
    if (!obj) return context->throwError("Object does not inherit QObject");
    return engine->toScriptValue(QString(obj->metaObject()->className()));
}

QScriptValue filterByType(QScriptContext *context, QScriptEngine *engine) {
    if (context->argumentCount()>=1) {
        QString targetClass = context->argument(0).toString();
        bool acceptSubclasses;
        if (context->argumentCount()>=2) acceptSubclasses = context->argument(1).toBool();
        else acceptSubclasses = true;
        QScriptValue oldArray = context->thisObject();
        QScriptValue filtered = engine->newArray();
        int i=0, l = oldArray.property("length").toInteger();
        if (acceptSubclasses) {
            for (int j = 0; j<l; j++) {
                QObject *obj = oldArray.property(j).toQObject();
                if (obj->inherits(targetClass.toUtf8().constData())) {
                    filtered.setProperty(i, oldArray.property(j));
                    i++;
                }
            }
        } else {
            for (int j = 0; j<l; j++) {
                QObject *obj = oldArray.property(j).toQObject();
                if (QString::compare(obj->metaObject()->className(), targetClass)==0) {
                    filtered.setProperty(i, oldArray.property(j));
                    i++;
                }
            }
        }
        return filtered;
    } else return context->throwError("Function takes arguments in the form (string) or (string, boolean)");
}

QScriptValue script_findChildren(QScriptContext *context, QScriptEngine *engine) {
    QScriptValue objectQSV;
    objectQSV = context->thisObject();
    QList<QObject*> results;
    if (context->argumentCount()!=0) { //Run findChildren with no arguments
        if (context->argument(0).isString()) { //search with the given string
            results = objectQSV.toQObject()->findChildren<QObject*>(context->argument(0).toString());
        } else if (context->argument(0).isRegExp()) { //search with the given regular expression
            results = objectQSV.toQObject()->findChildren<QObject*>(context->argument(0).toRegExp());
        } else results = objectQSV.toQObject()->findChildren<QObject*>();
    } else results = objectQSV.toQObject()->findChildren<QObject*>();
    QScriptValue array = engine->newArray(results.length());
    int i;
    for (i=0;i<results.length();i++) array.setProperty(i, engine->toScriptValue(results[i]));
    return array;
}

QScriptValue script_children(QScriptContext *context, QScriptEngine *engine) {
    QScriptValue objectQSV;
    objectQSV = context->thisObject();
    QList<QObject*> results = objectQSV.toQObject()->children();
    QScriptValue array = engine->newArray(results.length());
    int i;
    for (i=0;i<results.length();i++) array.setProperty(i, engine->toScriptValue(results[i]));
    return array;
}

void addQObjectProperties(QScriptValue *obj, QScriptEngine *engine) {
    obj->setProperty("findChildren", engine->newFunction(script_findChildren));
    obj->setProperty("children", engine->newFunction(script_children));
}

//QLayout

QScriptValue QLayout_layout_get(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("'layout' can only be accessed on a QLayoutItem, or a QLayoutItem derived class.");
    QLayout* lt = qobject_cast<QLayout*>(context->thisObject().toQObject());
    if (lt==0) return context->throwError("'layout' can only be accessed on a QLayoutItem, or a QLayoutItem derived class.");
    return engine->toScriptValue((QObject*)lt->layout());
}

QScriptValue QLayout_addWidget(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("'addWidget' can only run on a QLayout or a QLayout derived class.");
    QLayout* layout = qobject_cast<QLayout*>(context->thisObject().toQObject());
    if (layout==0) return context->throwError("'addWidget' can only run on a QLayout or a QLayout derived class.");
    if (context->argumentCount()==0) return context->throwError("'addWidget' requires one argument (the widget to be added)");
    if (!context->argument(0).isQObject()) return context->throwError("The argument must be a QWidget or QWidget subclass.");
    QWidget* wdgt = qobject_cast<QWidget*>(context->argument(0).toQObject());
    if (wdgt==0) return context->throwError("The argument must be a QWidget or QWidget subclass.");
    if (qobject_cast<QBoxLayout*>(layout)!=NULL) ((QBoxLayout*)layout)->addWidget(wdgt);
    else layout->addWidget(wdgt);
    return engine->undefinedValue();
}

QScriptValue QLayout_removeWidget(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("'removeWidget' can only run on a QLayout or a QLayout derived class.");
    QLayout* layout = qobject_cast<QLayout*>(context->thisObject().toQObject());
    if (layout==0) return context->throwError("'removeWidget' can only run on a QLayout or a QLayout derived class.");
    if (context->argumentCount()==0) return context->throwError("'removeWidget' requires one argument (the widget to be removed)");
    if (!context->argument(0).isQObject()) return context->throwError("The argument must be a QWidget or QWidget subclass.");
    QWidget* wdgt = qobject_cast<QWidget*>(context->argument(0).toQObject());
    if (wdgt==0) return context->throwError("The argument must be a QWidget or QWidget subclass.");
    layout->removeWidget(wdgt);
    return engine->undefinedValue();
}

QScriptValue QLayout_removeItem(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("'removeItem' can only run on a QLayout or a QLayout derived class.");
    QLayout* layout = qobject_cast<QLayout*>(context->thisObject().toQObject());
    if (layout==0) return context->throwError("'removeItem' can only run on a QLayout or a QLayout derived class.");
    if (context->argumentCount()==0) return context->throwError("'removeItem' requires one argument (the item to be removed)");
    if (!context->argument(0).isQObject()) return context->throwError("The argument must be a QLayoutItem or QLayoutItem subclass.");
    QLayout* itm = qobject_cast<QLayout*>(context->argument(0).toQObject()); //Doesn't work with QLayoutItem at the moment
    if (itm==0) return context->throwError("The argument must be a QLayoutItem or QLayoutItem subclass.");
    layout->removeItem(itm);
    return engine->undefinedValue();
}

QScriptValue construct_Layout(QScriptContext *context, QScriptEngine *engine) {
    QScriptValue parentQSV;
    if (context->argumentCount()==0) parentQSV = engine->undefinedValue();
    else parentQSV = context->argument(0);
    COSMOSUiLoader loader;
    QString subclass = context->callee().property("functionName").toString();
    QObject *layout = loader.createLayout(subclass, parentQSV.toQObject(), "");
    return engine->toScriptValue(layout);
}

//QBoxLayout (not supported directly)

QScriptValue QBoxLayout_addLayout(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("'addLayout' can only run on a QBoxLayout derived class.");
    QBoxLayout* layout = qobject_cast<QBoxLayout*>(context->thisObject().toQObject());
    if (layout==0) return context->throwError("'addLayout' can only run on a QBoxLayout derived class.");
    if (context->argumentCount()==0) return context->throwError("'addLayout' requires one argument (the layout to be added)");
    if (!context->argument(0).isQObject()) return context->throwError("The argument must be a QLayout subclass.");
    QLayout* layout2 = qobject_cast<QLayout*>(context->argument(0).toQObject());
    if (layout2==0) return context->throwError("The argument must be a QLayout subclass.");
    if (layout==layout2) return context->throwError("A layout cannot be added to itself.");
    layout->addLayout(layout2);
    return engine->undefinedValue();
}

//QVBoxLayout

QScriptValue QVBoxLayout_toScriptValue(QScriptEngine *engine, QVBoxLayout* const &layout) {
    QScriptValue obj = engine->toScriptValue((QObject*)layout);
    return obj;
}

void QVBoxLayout_fromScriptValue(const QScriptValue &obj, QVBoxLayout* &layout) {
    layout = qobject_cast<QVBoxLayout*>(obj.toQObject());
}

//QHBoxLayout

QScriptValue QHBoxLayout_toScriptValue(QScriptEngine *engine, QHBoxLayout* const &layout) {
    QScriptValue obj = engine->toScriptValue((QObject*)layout);
    return obj;
}

void QHBoxLayout_fromScriptValue(const QScriptValue &obj, QHBoxLayout* &layout) {
    layout = qobject_cast<QHBoxLayout*>(obj.toQObject());
}

//QWidget

QScriptValue QWidget_cosmosDatum_getSet(QScriptContext *context, QScriptEngine *engine) {
    QScriptValue qsv = context->thisObject();
    if (!qsv.isQObject()) return context->throwError("The 'cosmosDatum' property can only be accessed on QWidget derived classes.");
    QWidget *obj = qobject_cast<QWidget*>(qsv.toQObject());
    if (obj==NULL) return context->throwError("The 'cosmosDatum' property can only be accessed on QWidget derived classes.");
    QObjectList children = obj->children();
    QObject *dataObj = NULL;
    for (int i=0; i<children.length(); i++) {
        if (qobject_cast<COSMOSDatumWdgt*>(children[i])!=NULL) {
            dataObj = children[i];
            break;
        }
    }
    if (context->argumentCount()==1) {
        qsv = context->argument(0);
        if (!qsv.isQObject()) return context->throwError("The 'cosmosDatum' property must be set to a COSMOSDatumWdgt object.");
        if (qobject_cast<COSMOSDatumWdgt*>(qsv.toQObject())!=NULL) {
            if (dataObj!=NULL) { //Remove the old COSMOSDatumWdgt widget
                ((COSMOSDatumWdgt*)dataObj)->setParent(0);
                dataObj->deleteLater();
            }
            dataObj = qobject_cast<COSMOSDatumWdgt*>(qsv.toQObject());
            ((COSMOSDatumWdgt*)dataObj)->setParent(obj); //Add the new one.
            return engine->toScriptValue(dataObj);
        } else return context->throwError("The 'cosmosDatum' property must be set to a COSMOSDatumWdgt object.");
    } else {
        if (dataObj!=NULL) return engine->toScriptValue(dataObj);
        else return engine->undefinedValue();
    }
}

QScriptValue QWidget_layout_get(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("'layout' can only be accessed on a QWidget or a QWidget derived class.");
    QWidget* wdgt = qobject_cast<QWidget*>(context->thisObject().toQObject());
    if (wdgt==0) return context->throwError("'layout' can only be accessed on a QWidget or a QWidget derived class.");
    return engine->toScriptValue((QObject*)wdgt->layout());
}

QScriptValue QWidget_addAction(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("'addAction' can only run on a QWidget or a QWidget derived class.");
    QWidget* wdgt = qobject_cast<QWidget*>(context->thisObject().toQObject());
    if (wdgt==0) return context->throwError("'addAction' can only run on a QWidget or a QWidget derived class.");
    if (context->argumentCount()==0) return context->throwError("'addAction' requires one argument");
    if (!context->argument(0).isString()) return context->throwError("The action name must be a string.");
    QAction *act = new QAction(context->argument(0).toString(), wdgt);
    wdgt->addAction(act);
    return engine->toScriptValue((QObject*)act);
}

QScriptValue QWidget_actions_get(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("The 'actions' property only exists on QWidgets or QWidget derived classes.");
    QWidget* wdgt = qobject_cast<QWidget*>(context->thisObject().toQObject());
    if (wdgt==0) return context->throwError("The 'actions' property only exists on QWidgets or QWidget derived classes.");
    QList<QAction*> results = wdgt->actions();
    QScriptValue array = engine->newArray(results.length());
    int i;
    for (i=0;i<results.length();i++) array.setProperty(i, engine->toScriptValue((QObject*)results[i]));
    return array;
}

QScriptValue QWidget_toScriptValue(QScriptEngine *engine, QWidget* const &wdgt) {
    QScriptValue obj = engine->toScriptValue((QObject*)wdgt);
    return obj;
}

void QWidget_fromScriptValue(const QScriptValue &obj, QWidget* &wdgt) {
    wdgt = qobject_cast<QWidget*>(obj.toQObject());
}

QScriptValue construct_Widget(QScriptContext *context, QScriptEngine *engine) {
    QScriptValue parentQSV;
    if (context->argumentCount()==0) parentQSV = engine->undefinedValue();
    else parentQSV = context->argument(0);
    COSMOSUiLoader loader;
    QString subclass = context->callee().property("functionName").toString();
    QWidget *wdgt = loader.createWidget(subclass, qobject_cast<QWidget*>(parentQSV.toQObject()), "");
    wdgt->show();
    return engine->toScriptValue(wdgt);
}

//QPushButton

QScriptValue QPushButton_setMenu(QScriptContext *context, QScriptEngine *engine) {
    if (context->argumentCount()!=0) {
        QScriptValue buttonQSV = context->thisObject();
        QScriptValue menuQSV = context->argument(0);
        if (!buttonQSV.isQObject()||!menuQSV.isQObject()) return context->throwError("Menu cannot be set with non QObject");
        else {
            QPushButton* button = qobject_cast<QPushButton*>(buttonQSV.toQObject());
            if (button==0) return context->throwError("Function called on a non-QPushButton derived class.");
            QMenu* menu = qobject_cast<QMenu*>(menuQSV.toQObject());
            if (menu==0) return context->throwError("Argument provided is not a QMenu or QMenu subclass.");
            //And finally,...
            button->setMenu(menu); //Ta-da!
        }
    } else return context->throwError("This function requires one argument.");
    return engine->undefinedValue();
}

QScriptValue QPushButton_toScriptValue(QScriptEngine *engine, QPushButton* const &wdgt) {
    QScriptValue obj = engine->toScriptValue((QObject*)wdgt);
    return obj;
}

void QPushButton_fromScriptValue(const QScriptValue &obj, QPushButton* &wdgt) {
    wdgt = qobject_cast<QPushButton*>(obj.toQObject());
}

//QLabel

QScriptValue QLabel_toScriptValue(QScriptEngine *engine, QLabel* const &wdgt) {
    QScriptValue obj = engine->toScriptValue((QObject*)wdgt);
    return obj;
}

void QLabel_fromScriptValue(const QScriptValue &obj, QLabel* &wdgt) {
    wdgt = qobject_cast<QLabel*>(obj.toQObject());
}

//QMenu

QScriptValue QMenu_toScriptValue(QScriptEngine *engine, QMenu* const &wdgt) {
    QScriptValue obj = engine->toScriptValue((QObject*)wdgt);
    return obj;
}

void QMenu_fromScriptValue(const QScriptValue &obj, QMenu* &wdgt) {
    wdgt = qobject_cast<QMenu*>(obj.toQObject());
}

//TemperatureMonitor

QScriptValue TemperatureMonitor_toScriptValue(QScriptEngine *engine, TemperatureMonitor* const &wdgt) {
    QScriptValue obj = engine->toScriptValue((QObject*)wdgt);
    return obj;
}

void TemperatureMonitor_fromScriptValue(const QScriptValue &obj, TemperatureMonitor* &wdgt) {
    wdgt = qobject_cast<TemperatureMonitor*>(obj.toQObject());
}

//QComboBox

QScriptValue QComboBox_addItem(QScriptContext *context, QScriptEngine *engine) {
    if (!context->thisObject().isQObject()) return context->throwError("'addItem' can only run on a QComboBox");
    QComboBox* box = qobject_cast<QComboBox*>(context->thisObject().toQObject());
    if (box==0) return context->throwError("'addItem' can only run on a QComboBox");
    if (context->argumentCount()==0) return context->throwError("'addItem' requires one argument");
    if (!context->argument(0).isString()) return context->throwError("The item name must be a string.");
    box->addItem(context->argument(0).toString());
    return engine->undefinedValue();
}

QScriptValue QComboBox_toScriptValue(QScriptEngine *engine, QComboBox* const &wdgt) {
    QScriptValue obj = engine->toScriptValue((QObject*)wdgt);
    return obj;
}

void QComboBox_fromScriptValue(const QScriptValue &obj, QComboBox* &wdgt) {
    wdgt = qobject_cast<QComboBox*>(obj.toQObject());
}

//COSMOSDatumWdgt

QScriptValue COSMOSDatumWdgt_toScriptValue(QScriptEngine *engine, COSMOSDatumWdgt* const &wdgt) {
    QScriptValue obj = engine->toScriptValue((QObject*)wdgt);
    return obj;
}

void COSMOSDatumWdgt_fromScriptValue(const QScriptValue &obj, COSMOSDatumWdgt* &wdgt) {
    wdgt = qobject_cast<COSMOSDatumWdgt*>(obj.toQObject());
}

//General QObject-derived stuff

QScriptValue QObject_toScriptValue(QScriptEngine *engine, QObject* const &obj) {
    QScriptValue qsv = engine->newQObject(obj);
    addQObjectProperties(&qsv, engine);
    if (qobject_cast<QWidget*>(obj)!=NULL) {
        qsv.setProperty("layout", engine->newFunction(QWidget_layout_get), QScriptValue::PropertyGetter);
        qsv.setProperty("addAction", engine->newFunction(QWidget_addAction));
        qsv.setProperty("actions", engine->newFunction(QWidget_actions_get), QScriptValue::PropertyGetter);
        qsv.setProperty("cosmosDatum", engine->newFunction(QWidget_cosmosDatum_getSet), QScriptValue::PropertyGetter|QScriptValue::PropertySetter);
        if (qobject_cast<QPushButton*>(obj)!=NULL) {
            qsv.setProperty("setMenu", engine->newFunction(QPushButton_setMenu));
        } else if (qobject_cast<QLabel*>(obj)!=NULL) {
            //Custom properties go here
        } else if (qobject_cast<QMenu*>(obj)!=NULL) {
            //Custom properties go here
        } else if (qobject_cast<TemperatureMonitor*>(obj)!=NULL) {
            //Custom properties go here
        } else if (qobject_cast<QComboBox*>(obj)!=NULL) {
            qsv.setProperty("addItem", engine->newFunction(QComboBox_addItem));
        }
    } else if (qobject_cast<QLayout*>(obj)!=NULL) {
        qsv.setProperty("layout", engine->newFunction(QLayout_layout_get), QScriptValue::PropertyGetter);
        qsv.setProperty("addWidget", engine->newFunction(QLayout_addWidget));
        qsv.setProperty("removeWidget", engine->newFunction(QLayout_removeWidget));
        qsv.setProperty("removeItem", engine->newFunction(QLayout_removeItem));
        if (qobject_cast<QBoxLayout*>(obj)!=NULL) {
            qsv.setProperty("addLayout", engine->newFunction(QBoxLayout_addLayout));
        }
    }
    return qsv;
}

void QObject_fromScriptValue(const QScriptValue &qsv, QObject* &obj) {
    obj = qsv.toQObject();
}

QScriptValue QObject_restoreType(QScriptContext *context, QScriptEngine *engine) {
    if (context->argumentCount()==0) return context->throwError("'RestoreType' must be called with a QObject or QObject subclass.");
    if (!context->argument(0).isQObject()) return context->throwError("'RestoreType' must be called with a QObject or QObject subclass.");
    return engine->toScriptValue(context->argument(0).toQObject());
}

void COSMOSScriptEngine::implementQObjects() {
    globalObject().property("Array").property("prototype").setProperty("filterByType", newFunction(filterByType));
    globalObject().setProperty("loadUI", newFunction(loadUI));
    globalObject().setProperty("className", newFunction(className));
    globalObject().setProperty("RestoreType", newFunction(QObject_restoreType));
    qScriptRegisterMetaType<QObject*>(this, QObject_toScriptValue, QObject_fromScriptValue);
    qScriptRegisterMetaType<QWidget*>(this, QWidget_toScriptValue, QWidget_fromScriptValue);
    qScriptRegisterMetaType<QPushButton*>(this, QPushButton_toScriptValue, QPushButton_fromScriptValue);
    qScriptRegisterMetaType<QLabel*>(this, QLabel_toScriptValue, QLabel_fromScriptValue);
    qScriptRegisterMetaType<QMenu*>(this, QMenu_toScriptValue, QMenu_fromScriptValue);
    qScriptRegisterMetaType<TemperatureMonitor*>(this, TemperatureMonitor_toScriptValue, TemperatureMonitor_fromScriptValue);
    qScriptRegisterMetaType<QComboBox*>(this, QComboBox_toScriptValue, QComboBox_fromScriptValue);
    qScriptRegisterMetaType<COSMOSDatumWdgt*>(this, COSMOSDatumWdgt_toScriptValue, COSMOSDatumWdgt_fromScriptValue);
    COSMOSUiLoader loader;
    QStringList widgets = loader.availableWidgets();
    QScriptValue funct;
    for (int i=0; i<widgets.size(); i++) {
        funct = newFunction(construct_Widget);
        funct.setProperty("functionName", QScriptValue(widgets[i]));
        globalObject().setProperty(widgets[i], funct);
    }
    qScriptRegisterMetaType<QVBoxLayout*>(this, QVBoxLayout_toScriptValue, QVBoxLayout_fromScriptValue);
    qScriptRegisterMetaType<QHBoxLayout*>(this, QHBoxLayout_toScriptValue, QHBoxLayout_fromScriptValue);
    widgets = loader.availableLayouts();
    for (int i=0; i<widgets.size(); i++) {
        funct = newFunction(construct_Layout);
        funct.setProperty("functionName", QScriptValue(widgets[i]));
        globalObject().setProperty(widgets[i], funct);
    }
}

QScriptValue COSMOSScriptEngine::newQObject(QObject *object, ValueOwnership ownership, const QObjectWrapOptions &options) {
    QScriptValue qsv = QScriptEngine::newQObject(object, ownership, options);
    addQObjectProperties(&qsv, this);
    return qsv;
}

//Non-QObject derived classes:

void COSMOSScriptEngine::implementNonQObjects() {
    implementQRect();
}

//QRect

QScriptValue constructQRect(QScriptContext *context, QScriptEngine *engine) {
    if (context->argumentCount()==4) {
        int x = context->argument(0).toInteger(), y = context->argument(1).toInteger();
        int width = context->argument(2).toInteger(), height = context->argument(3).toInteger();
        return engine->toScriptValue(QRect(x, y, width, height));
    } else return engine->toScriptValue(QRect());
}

QScriptValue QRect_getSetComp(QScriptContext *context, QScriptEngine *engine) {
    QRect *thisQRect = qscriptvalue_cast<QRect*>(context->thisObject());
    QString name = context->callee().property("functionName").toString();
    int result;
    if (context->argumentCount()==1) {
        result = context->argument(0).toInteger();
        if (name=="x") thisQRect->moveLeft(result); //setX() and setY() don't do what you'd think.
        else if (name=="y") thisQRect->moveTop(result);
        else if (name=="width") thisQRect->setWidth(result);
        else thisQRect->setHeight(result);
    } else {
        if (name=="x") result = thisQRect->x();
        else if (name=="y") result = thisQRect->y();
        else if (name=="width") result = thisQRect->width();
        else result = thisQRect->height();
    }
    return engine->toScriptValue(result);
}

void COSMOSScriptEngine::implementQRect() {
    QScriptValue QRectProto = newObject();
    QScriptValue funct = newFunction(QRect_getSetComp);
    funct.setProperty("functionName", QScriptValue("x"));
    QRectProto.setProperty("x", funct, QScriptValue::PropertyGetter|QScriptValue::PropertySetter);
    funct = newFunction(QRect_getSetComp);
    funct.setProperty("functionName", QScriptValue("y"));
    QRectProto.setProperty("y", funct, QScriptValue::PropertyGetter|QScriptValue::PropertySetter);
    funct = newFunction(QRect_getSetComp);
    funct.setProperty("functionName", QScriptValue("width"));
    QRectProto.setProperty("width", funct, QScriptValue::PropertyGetter|QScriptValue::PropertySetter);
    funct = newFunction(QRect_getSetComp);
    funct.setProperty("functionName", QScriptValue("height"));
    QRectProto.setProperty("height", funct, QScriptValue::PropertyGetter|QScriptValue::PropertySetter);
    setDefaultPrototype(qMetaTypeId<QRect>(), QRectProto);
    globalObject().setProperty("QRect", newFunction(constructQRect, QRectProto));
}
