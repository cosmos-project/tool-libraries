#ifndef COSMOSSCRIPTENGINE_H
#define COSMOSSCRIPTENGINE_H

#include <QtScript/QScriptEngine>

class COSMOSScriptEngine : public QScriptEngine
{
    Q_OBJECT
public:
    explicit COSMOSScriptEngine(QObject *parent = 0);

    void implementFullBindings();

    void implementQObjects();

    void implementNonQObjects();
    void implementQRect();

    QScriptValue newQObject(QObject *object, ValueOwnership ownership = QtOwnership, const QObjectWrapOptions &options = 0);

    QScriptValue BlockedFunction();
signals:

public slots:

private:

};

#endif // COSMOSSCRIPTENGINE_H
