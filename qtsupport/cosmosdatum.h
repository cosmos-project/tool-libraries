#ifndef COSMOSDATUM_H
#define COSMOSDATUM_H

#include <QObject>

class COSMOSDatum : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString datumName READ datumName WRITE setDatumName NOTIFY datumNameChanged)
    Q_PROPERTY(double doubleValue READ doubleValue NOTIFY datumChanged)
    Q_PROPERTY(float floatValue READ floatValue NOTIFY datumChanged)

public:
    explicit COSMOSDatum(QObject *parent = 0);

    QString datumName() const;
    void setDatumName(QString jsonName);

    double doubleValue() const;
    float floatValue() const;

private:
    QString m_datumName;
    void *datum_ptr;
    double double_copy;
    float float_copy; //(support for other types coming later)
    void *datum_entry;
    bool isEquation;

signals:
    void datumNameChanged();
    void datumChanged();

public slots:
    void onDataUpdate();

};

#endif // COSMOSDATUM_H
