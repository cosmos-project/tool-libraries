#include "support/configCosmos.h"
#include "cosmosdatum.h"
#include "support/jsonlib.h"
#include "cosmosdata.h"

#include <QDebug>

extern COSMOSData* COSMOS;

COSMOSDatum::COSMOSDatum(QObject *parent) :
    QObject(parent)
{
    m_datumName = QString();
    double_copy = 0.0;
    datum_ptr = datum_entry = NULL;
    isEquation = false;
    connect(COSMOS, SIGNAL(dataupdate()), this, SLOT(onDataUpdate()));
}

QString COSMOSDatum::datumName() const {
    return m_datumName;
}
void COSMOSDatum::setDatumName(QString jsonName) {
    m_datumName = jsonName;
    if (!m_datumName.isEmpty()) {
        QByteArray namestring = m_datumName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        jsonentry *ptr;
        if ((ptr=json_entry_of(tempPointer, COSMOS->info)) == NULL) {
            datum_ptr = NULL;
            double_copy = 0.0;
            float_copy = 0.0;
            isEquation = false;
        } else {
            if (ptr->type==JSON_TYPE_EQUATION) isEquation = true;
            else {
                isEquation = false;
                datum_ptr = json_ptr_of_offset(ptr->offset,ptr->group, COSMOS->info);
            }
            datum_entry = (void*)ptr;
        }
    }
    emit datumNameChanged();
    onDataUpdate();
}

double COSMOSDatum::doubleValue() const {
    if (isEquation) {
        QByteArray namestring = m_datumName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        return json_equation(tempPointer,  COSMOS->info); //Re-evaluate the equation
    } else return double_copy;
}
float COSMOSDatum::floatValue() const {
    if (isEquation) {
        QByteArray namestring = m_datumName.toLocal8Bit();
        const char *tempPointer = namestring.data();
        return json_equation(tempPointer,  COSMOS->info); //Re-evaluate the equation
	}
	return float_copy;
}

void COSMOSDatum::onDataUpdate() {
    if (isEquation) emit datumChanged();
    else if (datum_ptr!=NULL&&datum_entry!=NULL) {
        if (((jsonentry*)datum_entry)->type==JSON_TYPE_DOUBLE) {
            if (double_copy!=*(double*)datum_ptr) {
                double_copy = *(double*)datum_ptr;
                emit datumChanged();
            }
        }
        if (((jsonentry*)datum_entry)->type==JSON_TYPE_FLOAT) {
            if (float_copy!=*(float*)datum_ptr) {
                float_copy = *(float*)datum_ptr;
                emit datumChanged();
            }
        }
    }
}
