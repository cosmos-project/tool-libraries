#ifndef COSMOSDATA_H
#define COSMOSDATA_H

#include "support/jsondef.h"
#include <QObject>
#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui>
#else
#include <QtWidgets>
#endif

class COSMOSData : public QObject
{
    Q_OBJECT
public:
    explicit COSMOSData(QObject *parent = 0, cosmosstruc *cinfo = 0);
	~COSMOSData();
    void updateCdata(cosmosstruc *cinfo);

    cosmosstruc *info;

signals:
    void dataupdate();

public slots:
    void updateDatums();

};

#endif // COSMOSDATA_H
