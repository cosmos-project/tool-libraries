#ifndef EVENT_H
#define EVENT_H

#include "support/configCosmos.h"
#include <QObject>
#include <QColor>
#include "support/jsondef.h"

namespace Cosmos {

    class CosmosEvent : public QObject
    {
        Q_OBJECT

        //All the properties of eventstruc, made accessable as Qt Properties:
        // Time event is to start.
        Q_PROPERTY(double utc READ utc WRITE setUtc NOTIFY utcChanged)
        // Time event was executed.
        Q_PROPERTY(double utcexec READ utcexec WRITE setUtcexec NOTIFY utcexecChanged)
        // Node for event
        Q_PROPERTY(QString node READ node WRITE setNode NOTIFY nodeChanged)
        // Name of event.
        Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
        // User of event.
        Q_PROPERTY(QString user READ user WRITE setUser NOTIFY userChanged)
        // CosmosEvent flags.
        Q_PROPERTY(int flag READ flag WRITE setFlag NOTIFY flagChanged) //Was uint32_t
        // CosmosEvent type.
        Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged) //Was uint32_t
        // Value of condition
        Q_PROPERTY(double value READ value WRITE setValue NOTIFY valueChanged)
        // CosmosEvent initial time consumed.
        Q_PROPERTY(double dtime READ dtime WRITE setDtime NOTIFY dtimeChanged)
        // CosmosEvent continuous time consumed.
        Q_PROPERTY(double ctime READ ctime WRITE setCtime NOTIFY ctimeChanged)
        // CosmosEvent initial energy consumed.
        Q_PROPERTY(float denergy READ denergy WRITE setDenergy NOTIFY denergyChanged)
        // CosmosEvent continuous energy consumed.
        Q_PROPERTY(float cenergy READ cenergy WRITE setCenergy NOTIFY cenergyChanged)
        // CosmosEvent initial mass consumed.
        Q_PROPERTY(float dmass READ dmass WRITE setDmass NOTIFY dmassChanged)
        // CosmosEvent continuous mass consumed.
        Q_PROPERTY(float cmass READ cmass WRITE setCmass NOTIFY cmassChanged)
        // CosmosEvent initial bytes consumed.
        Q_PROPERTY(float dbytes READ dbytes WRITE setDbytes NOTIFY dbytesChanged)
        // CosmosEvent continuous bytes consumed.
        Q_PROPERTY(float cbytes READ cbytes WRITE setCbytes NOTIFY cbytesChanged)
        // Handle of condition that caused event, NULL if timed event.
        Q_PROPERTY(jsonhandle handle READ handle WRITE setHandle NOTIFY handleChanged)
        // CosmosEvent specific data.
        Q_PROPERTY(QString data READ data WRITE setData NOTIFY dataChanged)
        // Condition that caused event, NULL if timed event.
        Q_PROPERTY(QString condition READ condition WRITE setCondition NOTIFY conditionChanged)

        Q_PROPERTY(double primaryUtc READ primaryUtc WRITE setPrimaryUtc NOTIFY primaryUtcChanged)
        Q_PROPERTY(QColor color READ color NOTIFY colorChanged)
        Q_PROPERTY(bool orphaned MEMBER m_orphaned NOTIFY orphanedChanged)
        Q_PROPERTY(bool selected MEMBER m_selected NOTIFY selectedChanged)
        Q_PROPERTY(QString jsonString MEMBER m_jsonString NOTIFY jsonStringChanged)
        Q_PROPERTY(bool jsonValid MEMBER m_jsonValid NOTIFY jsonValidChanged)

        Q_ENUMS(CosmosEventType CosmosEventFlag)

    public:
        enum CosmosEventType {TypeRequest=EVENT_TYPE_REQUEST, TypePhysical=EVENT_TYPE_PHYSICAL, TypeLatA=EVENT_TYPE_LATA, \
                        TypeLatD=EVENT_TYPE_LATD, TypeLatMax=EVENT_TYPE_LATMAX, TypeLatMin=EVENT_TYPE_LATMIN, \
                        TypeApogee=EVENT_TYPE_APOGEE, TypePerigee=EVENT_TYPE_PERIGEE, TypeUmbra=EVENT_TYPE_UMBRA, \
                        TypePenumbra=EVENT_TYPE_PENUMBRA, TypeGS=EVENT_TYPE_GS, TypeGS5=EVENT_TYPE_GS5, TypeGS10=EVENT_TYPE_GS10, \
                        TypeGSMax=EVENT_TYPE_GSMAX, TypeTarg=EVENT_TYPE_TARG, TypeTargMin=EVENT_TYPE_TARGMIN, \
                        TypeCommand=EVENT_TYPE_COMMAND, TypeBus=EVENT_TYPE_BUS, TypeEPS=EVENT_TYPE_EPS, TypeADCS=EVENT_TYPE_ADCS, \
                        TypePayload=EVENT_TYPE_PAYLOAD, TypeSystem=EVENT_TYPE_SYSTEM, TypeLog=EVENT_TYPE_LOG, \
                        TypeMessage=EVENT_TYPE_MESSAGE};
        enum CosmosEventFlag {FlagWarning=EVENT_FLAG_WARNING, ScaleWarning=EVENT_SCALE_WARNING, FlagAlarm=EVENT_FLAG_ALARM, \
                        ScaleAlarm=EVENT_SCALE_ALARM, FlagPriority=EVENT_FLAG_PRIORITY, FlagColor=EVENT_FLAG_COLOR, \
                        ScaleColor=EVENT_SCALE_COLOR, FlagCountdown=EVENT_FLAG_COUNTDOWN, FlagExit=EVENT_FLAG_EXIT, \
                        FlagPair=EVENT_FLAG_PAIR, FlagActual=EVENT_FLAG_ACTUAL, FlagConditional=EVENT_FLAG_CONDITIONAL, \
                        FlagRepeat=EVENT_FLAG_REPEAT, FlagTrue=EVENT_FLAG_TRUE};

        explicit CosmosEvent(QObject *parent = 0);
        explicit CosmosEvent(eventstruc event, QObject *parent = 0);


        double utc();
        void setUtc(double utc);

        double utcexec();
        void setUtcexec(double utcexec);

        QString node();
        void setNode (QString node);

        QString name();
        void setName(QString name);

        QString user();
        void setUser(QString user);

        uint32_t flag(); //Was uint32_t
        void setFlag(uint32_t flag);

        uint32_t type(); //Was uint32_t
        void setType(uint32_t type);

        double value();
        void setValue(double value);

        double dtime();
        void setDtime(double dtime);

        double ctime();
        void setCtime(double ctime);

        float denergy();
        void setDenergy(float denergy);

        float cenergy();
        void setCenergy(float cenergy);

        float dmass();
        void setDmass(float dmass);

        float cmass();
        void setCmass(float cmass);

        float dbytes();
        void setDbytes(float dbytes);

        float cbytes();
        void setCbytes(float cbytes);

        jsonhandle handle();
        void setHandle(jsonhandle handle);

        QString data();
        void setData(QString data);

        QString condition();
        void setCondition(QString condition);

        double primaryUtc();
        void setPrimaryUtc(double primaryUtc);

        QColor color();
        Q_INVOKABLE void setColor(int index);

        eventstruc getCosmosEvent();

    private:
        eventstruc evt;

        bool m_orphaned;
        bool m_selected;
        QString m_jsonString;
        bool m_jsonValid;

    signals:
        void utcChanged(double utc);
        void utcexecChanged(double utcexec);
        void nodeChanged(QString node);
        void nameChanged(QString name);
        void userChanged(QString user);
        void flagChanged(uint32_t flag); //Was uint32_t
        void typeChanged(uint32_t type); //Was uint32_t
        void valueChanged(double value);
        void dtimeChanged(double dtime);
        void ctimeChanged(double ctime);
        void denergyChanged(float denergy);
        void cenergyChanged(float cenergy);
        void dmassChanged(float dmass);
        void cmassChanged(float cmass);
        void dbytesChanged(float dbytes);
        void cbytesChanged(float cbytes);
        void handleChanged(jsonhandle handle);
        void dataChanged(QString data);
        void conditionChanged(QString condition);

        void primaryUtcChanged(double primaryUtc);
        void colorChanged(QColor color);
        void orphanedChanged(bool orphaned);
        void selectedChanged(bool selected);
        void jsonStringChanged(QString jsonString);
        void jsonValidChanged(bool jsonValid);

    };
}

#endif // EVENT_H
