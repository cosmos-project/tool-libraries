#include "support/configCosmos.h"
#include "agent/agentclass.h"

#include "cosmosdata.h"

COSMOSData::COSMOSData(QObject *parent, cosmosstruc *cinfo) :
    QObject(parent)
{
    info = cinfo;
}

COSMOSData::~COSMOSData()
{
    info = nullptr;
}

void COSMOSData::updateCdata(cosmosstruc *cinfo)
{
    info = cinfo;
}

void COSMOSData::updateDatums() {
    emit dataupdate();
}
