#include "cosmosevent.h"

#include <QDebug>

namespace Cosmos {

    CosmosEvent::CosmosEvent(QObject *parent) :
        QObject(parent)
    {
        evt = {}; //initializes to blank
        m_orphaned = false;
        m_selected = false;
    }

    CosmosEvent::CosmosEvent(eventstruc event, QObject *parent) :
        QObject(parent)
    {
        evt = event;
        m_orphaned = false;
        m_selected = false;
    }


    double CosmosEvent::utc() {
        return evt.l.utc;
    }
    void CosmosEvent::setUtc(double utc) {
        if (evt.l.utc != utc) {
            evt.l.utc = utc;
            emit utcChanged(utc);
            if (!(evt.s.flag & CosmosEvent::FlagActual)) emit primaryUtcChanged(utc);
        }
    }

    double CosmosEvent::utcexec() {
        return evt.l.utcexec;
    }
    void CosmosEvent::setUtcexec(double utcexec) {
        if (evt.l.utcexec != utcexec) {
            evt.l.utcexec = utcexec;
            emit utcexecChanged(utcexec);
            if (evt.s.flag & CosmosEvent::FlagActual) emit primaryUtcChanged(utcexec);
        }
    }

    QString CosmosEvent::node() {
        return QString(evt.l.node);
    }
    void CosmosEvent::setNode (QString node) {
        node.truncate(COSMOS_MAX_NAME);
        if (QString(evt.l.node) != node) {
            QByteArray ba = node.toLatin1(); //Apparently "toASCII" is depreciated, and it didn't quite do what it said anyway.
            qstrcpy(evt.l.node, ba.data());
            emit nodeChanged(node);
        }
    }

    QString CosmosEvent::name() {
        return QString(evt.l.name);
    }
    void CosmosEvent::setName(QString name) {
        name.truncate(COSMOS_MAX_NAME);
        if (QString(evt.l.name) != name) {
            QByteArray ba = name.toLatin1();
            qstrcpy(evt.l.name, ba.data());
            emit nameChanged(name);
        }
    }

    QString CosmosEvent::user() {
        return QString(evt.l.user);
    }
    void CosmosEvent::setUser(QString user) {
        user.truncate(COSMOS_MAX_NAME);
        if (QString(evt.l.user) != user) {
            QByteArray ba = user.toLatin1();
            qstrcpy(evt.l.user, ba.data());
            emit userChanged(user);
        }
    }

    //Was uint32_t
    uint32_t CosmosEvent::flag() {
        return evt.l.flag;
    }
    void CosmosEvent::setFlag(uint32_t flag) {
        uint32_t oldFlag;
        if (evt.l.flag != flag) {
            oldFlag = evt.l.flag;
            evt.l.flag = flag;
            if ((oldFlag & CosmosEvent::FlagColor) != (flag & CosmosEvent::FlagColor)) emit colorChanged(color());
            if ((oldFlag & CosmosEvent::FlagActual) != (flag & CosmosEvent::FlagActual)) emit primaryUtcChanged(primaryUtc());
            emit flagChanged(flag);
        }
    }

    //Was uint32_t
    uint32_t CosmosEvent::type() {
        return evt.l.type;
    }
    void CosmosEvent::setType(uint32_t type) {
        if (evt.l.type != type) {
            evt.l.type = type;
            emit typeChanged(type);
        }
    }

    double CosmosEvent::value() {
        return evt.l.value;
    }
    void CosmosEvent::setValue(double value) {
        if (evt.l.value != value) {
            evt.l.value = value;
            emit valueChanged(value);
        }
    }

    double CosmosEvent::dtime() {
        return evt.l.dtime;
    }
    void CosmosEvent::setDtime(double dtime) {
        if (evt.l.dtime != dtime) {
            evt.l.dtime = dtime;
            emit dtimeChanged(dtime);
        }
    }

    double CosmosEvent::ctime() {
        return evt.l.ctime;
    }
    void CosmosEvent::setCtime(double ctime) {
        if (evt.l.ctime != ctime) {
            evt.l.ctime = ctime;
            emit ctimeChanged(ctime);
        }
    }

    float CosmosEvent::denergy() {
        return evt.l.denergy;
    }
    void CosmosEvent::setDenergy(float denergy) {
        if (evt.l.denergy != denergy) {
            evt.l.denergy = denergy;
            emit denergyChanged(denergy);
        }
    }

    float CosmosEvent::cenergy() {
        return evt.l.cenergy;
    }
    void CosmosEvent::setCenergy(float cenergy) {
        if (evt.l.cenergy != cenergy) {
            evt.l.cenergy = cenergy;
            emit cenergyChanged(cenergy);
        }
    }

    float CosmosEvent::dmass() {
        return evt.l.dmass;
    }
    void CosmosEvent::setDmass(float dmass) {
        if (evt.l.dmass != dmass) {
            evt.l.dmass = dmass;
            emit dmassChanged(dmass);
        }
    }

    float CosmosEvent::cmass() {
        return evt.l.cmass;
    }
    void CosmosEvent::setCmass(float cmass) {
        if (evt.l.cmass != cmass) {
            evt.l.cmass = cmass;
            emit cmassChanged(cmass);
        }
    }

    float CosmosEvent::dbytes() {
        return evt.l.dbytes;
    }
    void CosmosEvent::setDbytes(float dbytes) {
        if (evt.l.dbytes != dbytes) {
            evt.l.dbytes = dbytes;
            emit dbytesChanged(dbytes);
        }
    }

    float CosmosEvent::cbytes() {
        return evt.l.cbytes;
    }
    void CosmosEvent::setCbytes(float cbytes) {
        if (evt.l.cbytes != cbytes) {
            evt.l.cbytes = cbytes;
            emit cbytesChanged(cbytes);
        }
    }

    jsonhandle CosmosEvent::handle() { //Eh, I'm not sure how committed I am to providing access to handle.
        return evt.l.handle;
    }
    void CosmosEvent::setHandle(jsonhandle handle) {
        if ((evt.l.handle.hash != handle.hash)||(evt.l.handle.index != handle.index)) {
            evt.l.handle = handle;
            emit handleChanged(handle);
        }
    }

    QString CosmosEvent::data() {
        return QString(evt.l.data);
    }
    void CosmosEvent::setData(QString data) {
        data.truncate(JSON_MAX_DATA); //for shorteventstructs this limit is actually COSMOS_MAX_NAME, take note when adding support for them.
        if (QString(evt.l.data) != data) {
            QByteArray ba = data.toLatin1();
            qstrcpy(evt.l.data, ba.data());
            emit dataChanged(data);
        }
    }

    QString CosmosEvent::condition() {
        return QString(evt.l.condition);
    }
    void CosmosEvent::setCondition(QString condition) {
        condition.truncate(JSON_MAX_DATA);
        if (QString(evt.l.condition) != condition) {
            QByteArray ba = condition.toLatin1();
            qstrcpy(evt.l.condition, ba.data());
            emit conditionChanged(condition);
        }
    }

    double CosmosEvent::primaryUtc() {
        return (evt.s.flag & CosmosEvent::FlagActual) ? evt.s.utcexec : evt.s.utc;
    }
    void CosmosEvent::setPrimaryUtc(double primaryUtc) {
        if (evt.s.flag & CosmosEvent::FlagActual) setUtcexec(primaryUtc);
        else setUtc(primaryUtc);
    }

    QColor CosmosEvent::color() {
        switch ((evt.s.flag & CosmosEvent::FlagColor)/CosmosEvent::ScaleColor) {
        case 1: return QColor("gray");
        case 2: return QColor("magenta");
        case 3: return QColor("cyan");
        case 4: return QColor("green");
        case 5: return QColor("orange");
        case 6: return QColor("yellow");
        case 7: return QColor("red");
        case 8: return QColor("brown");
        case 9: return QColor("white");
        default: return QColor("blue"); //not a color in COSMOS, indicates something has gone wrong.
        }
    }
    void CosmosEvent::setColor(int index) {
        if (index>0&&index<10) {
            //Erase the old color and add in the new one
            evt.s.flag = (evt.s.flag & ~CosmosEvent::FlagColor) | (index * CosmosEvent::ScaleColor);
            emit colorChanged(color());
            emit flagChanged(evt.s.flag);
        }
    }

    eventstruc CosmosEvent::getCosmosEvent() {
        return evt;
    }
}
