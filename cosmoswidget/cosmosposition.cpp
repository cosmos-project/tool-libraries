/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmosposition.h"

CosmosPosition::CosmosPosition(QWidget *parent) :
    QTabWidget(parent)
{
    cinfo = nullptr;
    
    jtype = JSON_TYPE_NONE;
}

void CosmosPosition::setCinfoPtr(cosmosstruc* newcinfo)
{
    if (cinfo != newcinfo)
    {
        cinfo = newcinfo;
        // Completely new node?
        if (cinfo != nullptr && cnode != &cinfo->node)
        {
            cnode = &cinfo->node;

            // Clear out old widget tabs if necessary
            clear();

            for (size_t jidx=JSON_TYPE_POS_FIRST; jidx<=JSON_TYPE_POS_LAST; ++jidx)
            {
                QLabel *tlabel;
                tabstruc ttab;
                QGridLayout *tlayout = new QGridLayout;
                size_t tcol = 0;

                ttab.w_me = new QWidget;
                ttab.w_me->setLayout(tlayout);

                ttab.jidx = jidx;

                // Date
                tlabel = new QLabel("Date:", ttab.w_me);
                tlayout->addWidget(tlabel, 0, tcol);
                ttab.l_date = new QLabel(ttab.w_me);
                tlayout->addWidget(ttab.l_date, 1, tcol);
                ++tcol;

                // Age
                tlabel = new QLabel("Age:", ttab.w_me);
                tlayout->addWidget(tlabel, 0, tcol);
                ttab.l_age = new QLabel(ttab.w_me);
                tlayout->addWidget(ttab.l_age, 1, tcol);
                ++tcol;

                // Position, Velocity, Acceleration
                for (size_t i=0; i<3; ++i)
                {
                    ttab.l_sname[i] = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_sname[i], 0, tcol);
                    ttab.l_sval[i] = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_sval[i], 1, tcol);
                    ++tcol;
                }

                for (size_t i=0; i<3; ++i)
                {
                    ttab.l_vname[i] = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_vname[i], 0, tcol);
                    ttab.l_vval[i] = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_vval[i], 1, tcol);
                    ++tcol;
                }

                for (size_t i=0; i<3; ++i)
                {
                    ttab.l_aname[i] = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_aname[i], 0, tcol);
                    ttab.l_aval[i] = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_aval[i], 1, tcol);
                    ++tcol;
                }

                ttab.jidx = jidx;
                tabstrucs.push_back(ttab);

                //            addTab(ttab.w_me, QString::fromStdString(cinfo->piece[dev.all.pidx].name));
                switch (jidx)
                {
                case JSON_TYPE_POS_ICRF:
                    ttab.s_name = "Barycentric";
                    break;
                case JSON_TYPE_POS_ECI:
                    ttab.s_name = "Earth Centered Inertial";
                    break;
                case JSON_TYPE_POS_GEOC:
                    ttab.s_name = "Geocentric";
                    break;
                case JSON_TYPE_POS_SCI:
                    ttab.s_name = "Selene Centered Inertial";
                    break;
                case JSON_TYPE_POS_SELC:
                    ttab.s_name = "Selenocentric";
                    break;
                case JSON_TYPE_POS_GEOD:
                    ttab.s_name = "Geodetic";
                    break;
                case JSON_TYPE_POS_GEOS:
                    ttab.s_name = "Geospherical";
                    break;
                case JSON_TYPE_POS_SELG:
                    ttab.s_name = "Selenographic";
                    break;
                }

                addTab(ttab.w_me, ttab.s_name);
            }
        }
    }

    updateCinfoValue();
}

void CosmosPosition::updateCinfoValue()
{
    if (cinfo == nullptr)
    {
        return;
    }


    // Update all tabs
    for (tabstruc& ttab : tabstrucs)
    {
        ttab.l_date->setText(QString("%1").fromStdString(mjd2iso8601(cinfo->node.loc.pos.utc)));
        ttab.l_age->setText(QString("%1").arg(86400.*(currentmjd()-cinfo->node.loc.pos.utc), 0, 'g', 5));
        switch (ttab.jidx)
        {
        case JSON_TYPE_POS_BARYC:
            for (size_t i=0; i<3; ++i)
            {
                ttab.l_sname[i]->setText("meters");
                ttab.l_vname[i]->setText("m/s");
                ttab.l_aname[i]->setText("m/s/s");
                ttab.l_sval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.icrf.s.col[i], 0, 'g', 10));
                ttab.l_vval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.icrf.v.col[i], 0, 'g', 10));
                ttab.l_aval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.icrf.a.col[i], 0, 'g', 10));
            }
            break;
        case JSON_TYPE_POS_ECI:
            for (size_t i=0; i<3; ++i)
            {
                ttab.l_sname[i]->setText("meters");
                ttab.l_vname[i]->setText("m/s");
                ttab.l_aname[i]->setText("m/s/s");
                ttab.l_sval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.eci.s.col[i], 0, 'g', 10));
                ttab.l_vval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.eci.v.col[i], 0, 'g', 10));
                ttab.l_aval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.eci.a.col[i], 0, 'g', 10));
            }
            break;
        case JSON_TYPE_POS_GEOC:
            for (size_t i=0; i<3; ++i)
            {
                ttab.l_sname[i]->setText("meters");
                ttab.l_vname[i]->setText("m/s");
                ttab.l_aname[i]->setText("m/s/s");
                ttab.l_sval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.geoc.s.col[i], 0, 'g', 10));
                ttab.l_vval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.geoc.v.col[i], 0, 'g', 10));
                ttab.l_aval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.geoc.a.col[i], 0, 'g', 10));
            }
            break;
        case JSON_TYPE_POS_SCI:
            for (size_t i=0; i<3; ++i)
            {
                ttab.l_sname[i]->setText("meters");
                ttab.l_vname[i]->setText("m/s");
                ttab.l_aname[i]->setText("m/s/s");
                ttab.l_sval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.sci.s.col[i], 0, 'g', 10));
                ttab.l_vval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.sci.v.col[i], 0, 'g', 10));
                ttab.l_aval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.sci.a.col[i], 0, 'g', 10));
            }
            break;
        case JSON_TYPE_POS_SELC:
            for (size_t i=0; i<3; ++i)
            {
                ttab.l_sname[i]->setText("meters");
                ttab.l_vname[i]->setText("m/s");
                ttab.l_aname[i]->setText("m/s/s");
                ttab.l_sval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.selc.s.col[i], 0, 'g', 10));
                ttab.l_vval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.selc.v.col[i], 0, 'g', 10));
                ttab.l_aval[i]->setText(QString("%1").arg(cinfo->node.loc.pos.selc.a.col[i], 0, 'g', 10));
            }
            break;
        case JSON_TYPE_POS_GEOD:
            ttab.l_sname[0]->setText("degrees");
            ttab.l_vname[0]->setText("degrees/s");
            ttab.l_aname[0]->setText("degrees/s/s");
            ttab.l_sname[1]->setText("degrees");
            ttab.l_vname[1]->setText("degrees/s");
            ttab.l_aname[1]->setText("degrees/s/s");
            ttab.l_sname[2]->setText("meters");
            ttab.l_vname[2]->setText("m/s");
            ttab.l_aname[2]->setText("m/s/s");
            ttab.l_sval[0]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.geod.s.lat), 0, 'g', 10));
            ttab.l_sval[1]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.geod.s.lon), 0, 'g', 10));
            ttab.l_sval[2]->setText(QString("%1").arg(cinfo->node.loc.pos.geod.s.h, 0, 'g', 10));
            ttab.l_vval[0]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.geod.v.lat), 0, 'g', 10));
            ttab.l_vval[1]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.geod.v.lon), 0, 'g', 10));
            ttab.l_vval[2]->setText(QString("%1").arg(cinfo->node.loc.pos.geod.v.h, 0, 'g', 10));
            ttab.l_aval[0]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.geod.a.lat), 0, 'g', 10));
            ttab.l_aval[1]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.geod.a.lon), 0, 'g', 10));
            ttab.l_aval[2]->setText(QString("%1").arg(cinfo->node.loc.pos.geod.a.h, 0, 'g', 10));
            break;
        case JSON_TYPE_POS_SELG:
            ttab.l_sname[0]->setText("degrees");
            ttab.l_vname[0]->setText("degrees/s");
            ttab.l_aname[0]->setText("degrees/s/s");
            ttab.l_sname[1]->setText("degrees");
            ttab.l_vname[1]->setText("degrees/s");
            ttab.l_aname[1]->setText("degrees/s/s");
            ttab.l_sname[2]->setText("meters");
            ttab.l_vname[2]->setText("m/s");
            ttab.l_aname[2]->setText("m/s/s");
            ttab.l_sval[0]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.selg.s.lat), 0, 'g', 10));
            ttab.l_sval[1]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.selg.s.lon), 0, 'g', 10));
            ttab.l_sval[2]->setText(QString("%1").arg(cinfo->node.loc.pos.selg.s.h, 0, 'g', 10));
            ttab.l_vval[0]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.selg.v.lat), 0, 'g', 10));
            ttab.l_vval[1]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.selg.v.lon), 0, 'g', 10));
            ttab.l_vval[2]->setText(QString("%1").arg(cinfo->node.loc.pos.selg.v.h, 0, 'g', 10));
            ttab.l_aval[0]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.selg.a.lat), 0, 'g', 10));
            ttab.l_aval[1]->setText(QString("%1").arg(DEGOF(cinfo->node.loc.pos.selg.a.lon), 0, 'g', 10));
            ttab.l_aval[2]->setText(QString("%1").arg(cinfo->node.loc.pos.selg.a.h, 0, 'g', 10));
            break;
        }
    }


    update();
}
