/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#ifndef COSMOSWIDGETHANDLER_H
#define COSMOSWIDGETHANDLER_H

#include "support/configCosmos.h"
#include "cosmoswidget.h"
#include <QComboBox>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include "cosmosplot.h"
#include "cosmoslabel.h"
#include "cosmosspinbox.h"
#include "cosmosdoublespinbox.h"
#include "cosmospushbutton.h"
#include "cosmosslider.h"
#include "cosmosmap.h"
#include "cosmospayloads.h"
#include "cosmosdevice.h"
#include "cosmosposition.h"
#include "cosmoselements.h"

class CosmosWidgetHandler
{
public:
    // Constructors
    CosmosWidgetHandler();

    // Other functions
    enum class widgetType : std::uint16_t
        {
        ELEMENTS,  
        SPINBOX,
        DOUBLESPINBOX,
        LABEL,
        PLOT,
        PUSHBUTTON,
        SLIDER,
        MAP,
        PAYLOADS,
        DEVICE,
        POSITION
        };

    size_t newWidget(widgetType type, std::string name="", std::string label="", bool show_list=false);
    void setCdataIndex(size_t newcinfoindex);
    void setCinfoStack(cosmosinfostack newcinfostack);
    void updateCdata();
    void setCosmosName(size_t nindex, size_t windex);
    void setCosmosName(string name, size_t windex);
//    void setVisibility(size_t index, bool vis);
    QWidget *getWidget(std::string name);
    QWidget *getWidget(size_t index);
    QVBoxLayout *getWidgetLayout(std::string name);
    QVBoxLayout *getWidgetLayout(size_t index);
    QComboBox *getWidgetCombo(std::string name);
    QComboBox *getWidgetCombo(size_t index);
    QLabel *getWidgetLabel(size_t index);
    std::string getName(size_t index);
    size_t getIndex(std::string name);
    size_t size();
    widgetType getType(size_t index);

    struct widgetHandle
    {
        string name_value;
        string label_value;
        QWidget *widget = nullptr;
        QLabel *label = nullptr;
        QComboBox *combo = nullptr;
        widgetType type;
        QVBoxLayout *layout = nullptr;
    };


private:
    cosmosstruc *cinfo = nullptr;
    size_t cdataindex = -1;
    cosmosinfostack cinfostack;
    std::vector <widgetHandle> widgetStack;
    std::vector <std::string> name_list;
};

#endif // COSMOSWIDGET_H
