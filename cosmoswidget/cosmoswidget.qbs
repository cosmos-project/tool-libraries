import qbs.Environment

Product  { // could be DynamicLibrary but at this point loading a dll does not seem to work well
    type: "staticlibrary"
    name: "cosmoswidget"

    files: [
        "*.cpp",
        "*.h",
    ]

    Depends {
        name: "Qt";
        submodules: ["core", "gui", "widgets"]
    }

    Depends { name: "cpp" }
    Properties {
        condition: qbs.targetOS.contains("windows")
        cpp.minimumWindowsVersion: "7.0"
    }
    cpp.cxxLanguageVersion : "c++11"
    cpp.commonCompilerFlags : "-std=c++11"

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: ["../"]
    }

    property string Cosmos: Environment.getEnv("COSMOS")
    cpp.includePaths : ['.',  Cosmos+"/include", "../../../thirdparty/"]

//    Depends {
//        name: ["qcustomplot"]
//    }

}
