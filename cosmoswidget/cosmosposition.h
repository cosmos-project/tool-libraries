/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#ifndef COSMOSPOSITION_H
#define COSMOSPOSITION_H

#include "cosmoswidget.h"
#include "support/jsondef.h"
#include <QDoubleSpinBox>
#include <QLabel>
#include <QTabWidget>
#include <QString>
#include <QGridLayout>

class CosmosPosition : public QTabWidget
{
    Q_OBJECT
public:
    explicit CosmosPosition(QWidget *parent = 0);

    void setCinfoPtr(cosmosstruc* newcinfo);
    void updateCinfoValue();
    cosmosstruc *getCinfoPtr() const { return cinfo; }
    
    void setJsonType(size_t type);
    struct tabstruc
    {
        QWidget *w_me;
        QString s_name;
        QLabel *l_date;
        QLabel *l_age;
        QLabel *l_sval[3];
        QLabel *l_vval[3];
        QLabel *l_aval[3];
        QLabel *l_sname[3];
        QLabel *l_vname[3];
        QLabel *l_aname[3];
        size_t jidx;
    };

    size_t getTabstrucCount() const { return tabstrucs.size(); }
    tabstruc *getTabstruc(size_t index) { return (tabstrucs.size() > index ? &tabstrucs[index] : nullptr); }

signals:

public slots:

private:
    cosmosstruc *cinfo;
    nodestruc *cnode;
    

    vector <tabstruc> tabstrucs;
    size_t jtype;
    size_t rowcnt;
};

#endif // COSMOSPOSITION_H
