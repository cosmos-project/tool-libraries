/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#ifndef COSMOSPLOT_H
#define COSMOSPLOT_H
#include "cosmoswidget.h"
#include "math/mathlib.h"
#include <QGroupBox>
#include <QVBoxLayout>
#include <QWidget>

// C++ libs
#include <iostream>
#include "qcustomplot/qcustomplot.h" // the header file of QCustomPlot. Don't forget to add it to your project, if you use an IDE, so it gets compiled.

class CosmosPlot : public QCustomPlot
{
    Q_OBJECT

public:
    explicit CosmosPlot(QWidget *parent = 0);
    ~CosmosPlot();

    void setCinfoValue();
    void setCinfoStackPtr(cosmosstruc *newcinfo, cosmosinfostack newcinfostack, size_t newcinfoiter);
    void setCosmosName(std::string newname);
    void updateCinfoValue(size_t iter);
    void updateCinfoValues();

	cosmosinfostack getCinfoStackValue() const { return cinfostack; }
    std::string getCosmosName() const { return name; }

    jsonentry* getCosmosEntry();

    void setAxisTitles(QString x_axis_title, QString y_axis_title);
    void setAxisTitles(QString x_axis_title, QString y_axis_title, int fontSize);

//    void addData(QVector<double> x, QVector<double> y, QString name = "", QColor color = Qt::blue);
    void setFinal();
    void setAxesRange(float xmin = -1, float xmax = 1, float ymin = -1, float ymax = 1);

    void clear();
    void refresh();
//    void addDataFromStdVector(std::vector<double> x, std::vector<double> y, QString name, QColor color);
    void save();
    double getMinY(size_t index);
    double getMaxY(size_t index);
    double getMinX();
    double getMaxX();
    double getMinOf(std::vector<double> &v);
    double getMaxOf(std::vector<double> &v);
    void setAxesRangeAuto();

    std::vector < std::vector <double> > stackY;
    std::vector<double> stackX;

private:
    cosmosstruc *cinfo;
    
	cosmosinfostack cinfostack;
    int32_t cinfoiter = -1;
    jsonentry *entry = 0;
    std::string name;

    uvector cvalue;
    std::vector <QCPItemTracer*> tracer;

};

#endif // COSMOSPLOT_H
