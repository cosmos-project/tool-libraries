/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmospushbutton.h"

CosmosPushButton::CosmosPushButton(QWidget *parent) : QPushButton(parent)
{
    cinfo = nullptr;
    
    entry = nullptr;
}

void CosmosPushButton::getCinfoValue()
{
    if (cinfo != nullptr && entry != nullptr)
    {
        cvalue = json_get_string(*entry, cinfo);
        setText(QString::fromStdString(cvalue));
    }
}

void CosmosPushButton::setCinfoValue()
{
    std::string tvalue;
    if (cinfo != nullptr && entry != nullptr && cvalue !=  (tvalue = text().toStdString()))
    {
        cvalue = tvalue;
        json_set_string(cvalue, *entry, cinfo);
    }
}

void CosmosPushButton::setCinfoPtr(cosmosstruc* newcinfo)
{
    if (cinfo != newcinfo)
    {
        cinfo = newcinfo;
        if (cinfo != nullptr && !name.empty())
        {
            entry = json_entry_of(name, cinfo);
        }
    }

    getCinfoValue();
}

void CosmosPushButton::setCosmosName(std::string newname)
{
    if (name != newname)
    {
        name = newname;
        if (cinfo != nullptr && !name.empty())
        {
            entry = json_entry_of(name, cinfo);
        }
        getCinfoValue();
    }
}

jsonentry* CosmosPushButton::getCosmosEntry()
{
    return entry;
}

