/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmoselements.h"

CosmosElements::CosmosElements(QWidget *parent) :
    QTabWidget(parent)
{
//    cinfo = nullptr;
//    
//    jtype = JSON_TYPE_NONE;
}

void CosmosElements::addTabs()
{

    //initialize all of kep
    kep->e = 0.;
    kep->a = 500000. + REARTHM;
    kep->i = RADOF(94.);
    kep->raan = 0.;
    kep->ap = RADOF(90.);



    kep2eci(*kep, *eci);
//    QLabel *tlabel;
    QWidget *kepTab = new QWidget;
    QFormLayout *kepLayout = new QFormLayout;
    kepTab->setLayout(kepLayout);

    kepLayout->setVerticalSpacing(30);

//    tlabel = new QLabel("Eccentricity", kepTab);
     eccentricity = new CosmosDoubleSpinBox;
    eccentricity->setValue(kep->e);
    connect(eccentricity, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        kep->e = eccentricity->value();
    });
    kepLayout->addRow("Eccentricity", eccentricity);



     altitude = new CosmosDoubleSpinBox;
    altitude->setMaximum(300000.);
    altitude->setValue((kep->a - REARTHM) / 1000.);
    connect(altitude, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        kep->a = 1000. * altitude->value() + REARTHM;
    });
    kepLayout->addRow("Altitude", altitude);

     inclination = new CosmosDoubleSpinBox;
    inclination->setMaximum(180.);
    inclination->setValue(DEGOF(kep->i));
    connect(inclination, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        kep->i = RADOF(inclination->value());
    });
    kepLayout->addRow("Inclination", inclination);

     raan = new CosmosDoubleSpinBox;
    raan->setRange(-180., 180.);
    raan->setValue(kep->raan);
    raan->setToolTip("Right ascension of the ascending node");
    connect(raan, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        kep->raan = RADOF(raan->value());
    });
    kepLayout->addRow("RAAN", raan);

     ap = new CosmosDoubleSpinBox;
    ap->setMaximum(180.);
    ap->setToolTip("Argument of perigee");
    ap->setValue(DEGOF(kep->ap));
    connect(ap, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        kep->ap = RADOF(ap->value());
    });
    kepLayout->addRow("AP", ap);

    QPushButton *kepOrbit = new QPushButton;
    kepOrbit->setText("Update Orbit");

    connect(kepOrbit, &QPushButton::clicked, [=]() {
        kep2eci(*kep, *eci);
        updateOrbits();
    });
    kepLayout->addRow(kepOrbit);

    QPushButton *kepExport = new QPushButton;
    kepExport->setText("Export Data");
    kepExport->setToolTip("Export keplerian elements to a .txt file");
    connect(kepExport, &QPushButton::clicked, [=]() {
        eci2kep(*eci, *kep);
        //UPDATE SPINBOXES();
        exportingKepData();
    });
    kepLayout->addRow(kepExport);

    addTab(kepTab, "Keplerian");

    QWidget *eciTab = new QWidget;
    QFormLayout *eciLayout = new QFormLayout;
    eciTab->setLayout(eciLayout);

    eciLayout->setVerticalSpacing(23);


    xPos = new CosmosDoubleSpinBox;
    xPos->setMaximum(1e20);  //couldn't find a define for MAX_DOUBLE, for 64bit it is = 1.8e308? (CT:25/08/2017)
    xPos->setValue(eci->s.col[0]);
    connect(xPos, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        eci->s.col[0] = xPos->value();
    });
    eciLayout->addRow("X Position (m)", xPos);


    yPos = new CosmosDoubleSpinBox;
    yPos->setMaximum(1e20);
    yPos->setValue(eci->s.col[1]);
    connect(yPos, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        eci->s.col[1] = yPos->value();
    });
    eciLayout->addRow("Y Position (m)", yPos);



    zPos = new CosmosDoubleSpinBox;
    zPos->setMaximum(1e20);
    zPos->setValue(eci->s.col[2]);
    connect(zPos, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        eci->s.col[2] = zPos->value();
    });
    eciLayout->addRow("Z Position (m)", zPos);


    xVel = new CosmosDoubleSpinBox;
    xVel->setMaximum(3e8);
    xVel->setValue(eci->v.col[0]);
    connect(xVel, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        eci->v.col[0] = xVel->value();
    });
    eciLayout->addRow("X Velocity (m/s)", xVel);


    yVel = new CosmosDoubleSpinBox;
    yVel->setMaximum(3e8);
    yVel->setValue(eci->v.col[1]);
    connect(yVel, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        eci->v.col[1] = yVel->value();
    });
    eciLayout->addRow("Y Velocity (m/s)", yVel);


    zVel = new CosmosDoubleSpinBox;
    zVel->setMaximum(3e8);
    zVel->setValue(eci->v.col[2]);
    connect(zVel, &CosmosDoubleSpinBox::editingFinished, [&]()
    {
        eci->v.col[2] = zVel->value();
    });
    eciLayout->addRow("Z Velocity (m/s)", zVel);


    QPushButton *eciOrbit = new QPushButton;
    eciOrbit->setText("Update Orbit");
    connect(eciOrbit, &QPushButton::clicked, [=]() {
        eci2kep(*eci, *kep);
        updateOrbits();
    });
    eciLayout->addRow(eciOrbit);

    QPushButton *eciExport = new QPushButton;
    eciExport->setText("Export Data");
    eciExport->setToolTip("Export Earth-centered intertial coordinates to a .txt file");
    connect(eciExport, &QPushButton::clicked, [=]() {
        exportingEciData();
    });
    eciLayout->addRow(eciExport);

    addTab(eciTab, "     ECI     ");



}

void CosmosElements::updateEciSpinboxes(){

    //goes through each spinbox and updates the values
    xPos->setValue(eci->s.col[0]);
    yPos->setValue(eci->s.col[1]);
    zPos->setValue(eci->s.col[2]);
    xVel->setValue(eci->v.col[0]);
    yVel->setValue(eci->v.col[1]);
    zVel->setValue(eci->v.col[2]);

}

void CosmosElements::updateKepSpinboxes(){

    //goes through each spinbox and updates the values
    eccentricity->setValue(kep->e);
    altitude->setValue((kep->a  - REARTHM)/ 1000.);
    inclination->setValue(DEGOF(kep->i));
    raan->setValue(DEGOF(kep->raan));
    ap->setValue(DEGOF(kep->ap));

}
