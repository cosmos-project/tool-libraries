/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmosdevice.h"

CosmosDevice::CosmosDevice(QWidget *parent) :
    QTabWidget(parent)
{
    cinfo = nullptr;
    dtype = DEVICE_TYPE_NONE;
}

void CosmosDevice::setCinfoPtr(cosmosstruc *newcinfo)
{
    if (cdata != newcdata)
    {
        cdata = newcdata;
    }

    // Completely new node?
    if (cinfo != newcinfo)
    {
        cinfo = newcinfo;

        // Clear out old widget tabs if necessary
        clear();

        for (devicestruc dev : cdata->device)
        {
            if (dev.all.gen.type != dtype)
            {
                continue;
            }

            QLabel *tlabel;
            tabstruc ttab;
            QGridLayout *tlayout = new QGridLayout;
            size_t trow = 0;

            ttab.w_me = new QWidget;
            ttab.w_me->setLayout(tlayout);

            ttab.cidx = dev.all.gen.cidx;
            ttab.didx = dev.all.gen.didx;

            // Date
            tlabel = new QLabel("Date:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 0);
            ttab.l_date = new QLabel(ttab.w_me);
            tlayout->addWidget(ttab.l_date, trow, 1);

            // Age
            tlabel = new QLabel("Age:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 2);
            ttab.l_age = new QLabel(ttab.w_me);
            tlayout->addWidget(ttab.l_age, trow, 3);
            ++trow;

            // Flags
            tlabel = new QLabel("State:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 0);
            ttab.b_on = new QRadioButton("On", ttab.w_me);
            ttab.b_on->setCheckable(true);
            tlayout->addWidget(ttab.b_on, trow, 1);
            ttab.b_off = new QRadioButton("Off", ttab.w_me);
            ttab.b_off->setCheckable(true);
            tlayout->addWidget(ttab.b_off, trow, 2);
            ++trow;

            // Current, Voltage, Power
            tlabel = new QLabel("Current:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 0);
            ttab.l_current = new QLabel(ttab.w_me);
            tlayout->addWidget(ttab.l_current, trow, 1);

            tlabel = new QLabel("Voltage:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 2);
            ttab.l_voltage = new QLabel(ttab.w_me);
            tlayout->addWidget(ttab.l_voltage, trow, 3);

            tlabel = new QLabel("Power:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 4);
            ttab.l_power = new QLabel(ttab.w_me);
            tlayout->addWidget(ttab.l_power, trow, 5);
            ++trow;

            // Date Rate, Temperature
            tlabel = new QLabel("Data Rate:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 0);
            ttab.l_drate = new QLabel(ttab.w_me);
            tlayout->addWidget(ttab.l_drate, trow, 1);

            tlabel = new QLabel("Temperature:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 2);
            ttab.l_temp = new QLabel(ttab.w_me);
            tlayout->addWidget(ttab.l_temp, trow, 3);
            ++trow;

            // Device specific values
            switch (dtype)
            {
            case DEVICE_TYPE_CPU:
                {
                    // Date Rate, Temperature
                    tlabel = new QLabel("Uptime:", ttab.w_me);
                    tlayout->addWidget(tlabel, trow, 0);
                    ttab.l_uptime = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_uptime, trow, 1);

                    tlabel = new QLabel("Load %:", ttab.w_me);
                    tlayout->addWidget(tlabel, trow, 2);
                    ttab.l_load = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_load, trow, 3);

                    tlabel = new QLabel("Memory %:", ttab.w_me);
                    tlayout->addWidget(tlabel, trow, 4);
                    ttab.l_gib = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_gib, trow, 5);
                    ++trow;
                }
                break;
            case DEVICE_TYPE_DISK:
                {
                    tlabel = new QLabel("Disk %:", ttab.w_me);
                    tlayout->addWidget(tlabel, trow, 4);
                    ttab.l_gib = new QLabel(ttab.w_me);
                    tlayout->addWidget(ttab.l_gib, trow, 5);
                    ++trow;
                }
                break;
            }

            tlabel = new QLabel("Condition:", ttab.w_me);
            tlayout->addWidget(tlabel, trow, 0);
            ttab.l_condition = new QLineEdit(ttab.w_me);
            tlayout->addWidget(ttab.l_condition, trow, 1);
            ttab.b_add_condition = new QPushButton("Add", ttab.w_me);
            connect(ttab.b_add_condition, &QPushButton::pressed, [&]() { buttonAddCondition(ttab.didx); });
            tlayout->addWidget(ttab.b_add_condition, trow, 2);
            ++trow;

            rowcnt = trow;

            ttab.conditions.clear();

            tabstrucs.push_back(ttab);
            addTab(ttab.w_me, QString::fromStdString(cdata->piece[dev.all.gen.pidx].name));
        }
    }

    updateCdataValue();
}

void CosmosDevice::updateCdataValue()
{
    if (cinfo == nullptr || cdata == nullptr)
    {
        return;
    }


    // Update all tabs
    for (tabstruc& ttab : tabstrucs)
    {
        ttab.l_date->setText(QString("%1").fromStdString(mjd2iso8601(cdata->device[ttab.cidx].all.gen.utc)));
        ttab.l_age->setText(QString("%1").arg(86400.*(currentmjd()-cdata->device[ttab.cidx].all.gen.utc), 0, 'g', 5));
        switch (cdata->device[ttab.cidx].all.gen.flag & DEVICE_FLAG_ON)
        {
        case 0:
            {
                ttab.b_off->setChecked(true);
                ttab.b_on->setChecked(false);
            }
            break;
        default:
            {
                ttab.b_off->setChecked(false);
                ttab.b_on->setChecked(true);
            }
            break;
        }
        ttab.l_current->setText(QString("%1").arg(cdata->device[ttab.cidx].all.gen.amp, 0, 'g', 10));
        ttab.l_voltage->setText(QString("%1").arg(cdata->device[ttab.cidx].all.gen.volt, 0, 'g', 10));
        ttab.l_power->setText(QString("%1").arg(cdata->device[ttab.cidx].all.gen.power, 0, 'g', 10));
        ttab.l_drate->setText(QString("%1").arg(cdata->device[ttab.cidx].all.gen.drate, 0, 'g', 10));
        ttab.l_temp->setText(QString("%1").arg(cdata->device[ttab.cidx].all.gen.temp, 0, 'g', 10));
        for (size_t i=0; i<ttab.conditions.size(); ++i)
        {
            ttab.conditions[i].l_condition->setText(QString::fromStdString(ttab.conditions[i].condition));
        }
        switch (cdata->device[ttab.cidx].all.gen.type)
        {
        case DEVICE_TYPE_CPU:
            ttab.l_uptime->setText(QString("%1").arg(cdata->device[ttab.cidx].cpu.uptime, 0, 'g', 10));
            ttab.l_load->setText(QString("%1").arg(100.*cdata->device[ttab.cidx].cpu.load/cdata->device[ttab.cidx].cpu.maxload, 0, 'g', 5));
            ttab.l_gib->setText(QString("%1").arg(100.*cdata->device[ttab.cidx].cpu.gib/cdata->device[ttab.cidx].cpu.maxgib, 0, 'g', 3));
            break;
        case DEVICE_TYPE_DISK:
            ttab.l_gib->setText(QString("%1").arg(100.*cdata->device[ttab.cidx].disk.gib/cdata->device[ttab.cidx].disk.maxgib, 0, 'g', 3));
            break;
        }
    }


    update();
}

void CosmosDevice::buttonAddCondition(int index)
{
    size_t cindex;
    for (cindex = 0; cindex < tabstrucs[index].conditions.size(); ++cindex)
    {
        if (tabstrucs[index].conditions[cindex].condition.empty())
            break;
    }

    string condition = "(" + tabstrucs[index].l_condition->text().toStdString() + ")";
    if (cindex == tabstrucs[index].conditions.size())
    {
        conditionstruc tcondition;
        tcondition.condition = condition;
        QLabel *tlabel = new QLabel(QString::fromStdString(condition), tabstrucs[index].w_me);
        ((QGridLayout *)(tabstrucs[index].w_me->layout()))->addWidget(tlabel, tabstrucs[index].conditions.size()+5, 0);
        tcondition.l_condition = tlabel;
        QPushButton *tbutton = new QPushButton("Del", tabstrucs[index].w_me);
        ((QGridLayout *)(tabstrucs[index].w_me->layout()))->addWidget(tbutton, tabstrucs[index].conditions.size()+5, 1);
        connect(tbutton, &QPushButton::pressed, [&, index, cindex]() { buttonDelCondition(index, cindex); });
        tcondition.b_del_condition = tbutton;
        tabstrucs[index].conditions.push_back(tcondition);
    }
    else
    {
        tabstrucs[index].conditions[cindex].condition = condition;
        tabstrucs[index].conditions[cindex].l_condition->setText(QString::fromStdString(condition));
    }
}

void CosmosDevice::buttonDelCondition(int index, int cindex)
{
    tabstrucs[index].conditions[cindex].condition = "";
    tabstrucs[index].conditions[cindex].l_condition->setText("");
}

void CosmosDevice::setDeviceType(size_t type)
{
    if (type < DEVICE_TYPE_COUNT)
    {
        dtype = type;
    }
    else
    {
        dtype = DEVICE_TYPE_NONE;
    }
}
