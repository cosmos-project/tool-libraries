/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmosplot.h"

CosmosPlot::CosmosPlot(QWidget *parent) :
    QCustomPlot(parent)
{

    cinfostack.stack = nullptr;
    entry = nullptr;

    // set v
    legend->setVisible(true);
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

}

CosmosPlot::~CosmosPlot()
{
    //delete ui;
}

void CosmosPlot::setAxisTitles(QString x_axis_title, QString y_axis_title){
    QFont tfont = font();
    tfont.setPixelSize(20);

    xAxis->setLabelFont(tfont);
    yAxis->setLabelFont(tfont);

    xAxis->setLabel(x_axis_title);
    yAxis->setLabel(y_axis_title);
}


void CosmosPlot::setAxisTitles(QString x_axis_title, QString y_axis_title, int fontSize){
    QFont tfont = font();
    tfont.setPixelSize(fontSize);

    xAxis->setLabelFont(tfont);
    yAxis->setLabelFont(tfont);

    xAxis->setLabel(x_axis_title);
    yAxis->setLabel(y_axis_title);
}

void CosmosPlot::setCinfoStackPtr(cosmosstruc *newcinfo, cosmosinfostack newcinfostack, size_t newcinfoiter)
{
    cinfoiter = newcinfoiter;
    if (cinfo != newcinfo)
    {
        cinfo = newcinfo;
        cinfostack.stack = nullptr;
    }

    if (cinfostack.stack != newcinfostack.stack || cinfostack.timestamp != newcinfostack.timestamp)
    {
        cinfostack = newcinfostack;
        cinfoiter = 0;
        updateCinfoValues();
    }
    else
    {
        updateCinfoValue(cinfoiter);
    }

}

void CosmosPlot::setCosmosName(std::string newname)
{
    if (name != newname)
    {
        name = newname;
        updateCinfoValues();
    }
}

void CosmosPlot::updateCinfoValue(size_t iter)
{
    if (cinfo == nullptr || cinfostack.stack == nullptr || iter >= stackX.size())
    {
        return;
    }

    for (QCPItemTracer *ttracer : tracer)
    {
        ttracer->setGraphKey(stackX[iter]);
    }
    refresh();
}

void CosmosPlot::updateCinfoValues()
{
    if (cinfo == nullptr || cinfostack.stack == nullptr)
    {
        return;
    }

    for (size_t i=0; i<stackY.size(); ++i)
    {
        stackY[i].clear();
    }
    stackY.clear();
    stackX.clear();
    if (cinfo != nullptr && cinfostack.stack != nullptr && name.size())
    {
        for (size_t i=0; i<(*cinfostack.stack).size(); ++i)
        {
            cinfo = &(*cinfostack.stack)[i];
            stackX.push_back(cinfo->node.utc);
            entry = json_entry_of(name, cinfo);
            switch (entry->type)
            {
            case JSON_TYPE_RVECTOR:
                if (stackY.size() == 0)
                {
                    stackY.resize(3);
                }
                cvalue.r = json_get_rvector(*entry, cinfo);
                for (size_t j=0; j<3; ++j)
                {
                    stackY[j].push_back(cvalue.a4[j]);
                }
                break;
            case JSON_TYPE_QUATERNION:
                if (stackY.size() == 0)
                {
                    stackY.resize(4);
                }
                cvalue.q = json_get_quaternion(*entry, cinfo);
                for (size_t j=0; j<4; ++j)
                {
                    stackY[j].push_back(cvalue.a4[j]);
                }
                break;
            default:
                if (stackY.size() == 0)
                {
                    stackY.resize(1);
                }
                cvalue.a4[0] = json_get_double(*entry, cinfo);
                stackY[0].push_back(cvalue.a4[0]);
                break;
            }
        }
    }

    QColor color[4] = {Qt::green, Qt::blue, Qt::cyan, Qt::magenta};
    clearGraphs();
    clearItems();
    tracer.clear();
    setWindowTitle(QString::fromStdString(name+"_"));
    for (size_t i=0; i<stackY.size(); ++i)
    {
        if (stackY[i].size())
        {
            addGraph();
            graph(i)->setPen(QPen(color[i]));
            graph(i)->setData(QVector<double>::fromStdVector(stackX), QVector<double>::fromStdVector(stackY[i]));
            graph(i)->setName(QString::number(i));

            QCPItemTracer *ttracer = new QCPItemTracer(this);
            addItem(ttracer);
            ttracer->setGraph(graph(i));
            ttracer->setStyle(QCPItemTracer::tsCircle);
            ttracer->setPen(QPen(Qt::red));
            ttracer->setSize(7);
            ttracer->setGraphKey(stackX[cinfoiter]);
            tracer.push_back(ttracer);
        }
    }
    setAxesRangeAuto();
//    setMinimumWidth(800);
//    setMinimumHeight(160);
    refresh();
}

void CosmosPlot::setAxesRange(float xmin, float xmax, float ymin, float ymax){
    // set axes ranges, so we can see all the data:
    xAxis->setRange(xmin, xmax);
    yAxis->setRange(ymin, ymax);
}

void CosmosPlot::setAxesRangeAuto()
{

    float offsetPercentage = 0.1;
    float rangeX = getMaxX() - getMinX();
    float maxY = getMaxY(0);
    float minY = getMinY(0);
    for (size_t i=1; i<(size_t)graphCount(); ++i)
    {
        if (getMaxY(i) > maxY)
        {
            maxY = getMaxY(i);
        }
        if (getMinY(i) < minY)
        {
            minY = getMinY(i);
        }
    }
    float rangeY = maxY - minY;
    if (rangeY == 0.)
    {
        setAxesRange(getMinX() - rangeX*offsetPercentage,
                     getMaxX() + rangeX*offsetPercentage,
                     .9 * minY,
                     1.1 * maxY);
    }
    else
    {
    setAxesRange(getMinX() - rangeX*offsetPercentage,
                 getMaxX() + rangeX*offsetPercentage,
                 minY - rangeY*offsetPercentage,
                 maxY + rangeY*offsetPercentage);
    }
}



double CosmosPlot::getMaxY(size_t index)
{
    return *max_element(begin(stackY[index]), end(stackY[index]));
}

double CosmosPlot::getMinY(size_t index)
{
    return *min_element(begin(stackY[index]), end(stackY[index]));
}

double CosmosPlot::getMaxX()
{
    return *max_element(begin(stackX), end(stackX));
}

double CosmosPlot::getMinX()
{
    return *min_element(begin(stackX), end(stackX));
}


double CosmosPlot::getMaxOf(std::vector<double> &v){
    return *max_element(begin(v), end(v));
}

double CosmosPlot::getMinOf(std::vector<double> &v){
    return *min_element(begin(v), end(v));
}

void CosmosPlot::setFinal(){

    //    //replot();


    //    //QGroupBox *plotGroupBox = new QGroupBox;
    //    QVBoxLayout *plot_layout = new QVBoxLayout;

    //    plot_layout->addWidget(plot);
    //    //plotGroupBox->setLayout(vbox1);

    //    setLayout(plot_layout);
}

void CosmosPlot::clear(){
    //    clearItems();
    // delete the stacks so that when we clear the
    // auto axis can compute the new stack correctly
    stackX.clear();
    for (size_t i=0; i<4; ++i)
    {
        stackY[i].clear();
    }
    clearGraphs();
    clearItems();
    tracer.clear();
}

void CosmosPlot::refresh(){
    repaint();
    replot();
}

void CosmosPlot::save(){
    // save to pdf does not work very well in this version on QCustomPlot
    //savePdf("plot.pdf",false,500,1000);
    savePng("plot.png",1000,500);
}

jsonentry* CosmosPlot::getCosmosEntry()
{
    return entry;
}
