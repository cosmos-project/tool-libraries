/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#ifndef COSMOSDEVICE_H
#define COSMOSDEVICE_H

#include "cosmoswidget.h"
#include "support/timelib.h"
#include <QTabWidget>
#include <QLabel>
#include <QRadioButton>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>

class CosmosDevice : public QTabWidget
{
    Q_OBJECT
public:
    explicit CosmosDevice(QWidget *parent = 0);

    void setCinfoPtr(cosmosstruc* newcinfo);
    void updateCinfoValue();
    cosmosstruc *getCinfoPtr() const { return cinfo; }
    
    void buttonAddCondition(int index);
    void buttonDelCondition(int index, int cindex);
    void setDeviceType(size_t type);

    struct conditionstruc
    {
        QLabel *l_condition;
        QPushButton *b_del_condition;
        string condition;
    };

    struct tabstruc
    {
        QWidget *w_me;
        QLabel *l_date;
        QLabel *l_age;
        QLabel *l_current;
        QLabel *l_voltage;
        QLabel *l_power;
        QLabel *l_drate;
        QLabel *l_temp;
        QRadioButton *b_on;
        QRadioButton *b_off;
        QPushButton *b_add_condition;
        QLineEdit *l_condition;
        size_t cidx;
        size_t didx;
        vector <conditionstruc> conditions;
        // CPU
        QLabel *l_uptime;
        QLabel *l_load;
        QLabel *l_gib;
    };

    size_t getTabstrucCount() const { return tabstrucs.size(); }
    tabstruc *getTabstruc(size_t index) { return (tabstrucs.size() > index ? &tabstrucs[index] : nullptr); }

signals:

public slots:

private:
    cosmosstruc *cinfo;
    nodestruc *cnode;
    

    vector <tabstruc> tabstrucs;
    uint16_t dtype;
    size_t rowcnt;
};

#endif // COSMOSDEVICE_H
