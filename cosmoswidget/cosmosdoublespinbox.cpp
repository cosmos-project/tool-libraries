/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmosdoublespinbox.h"

CosmosDoubleSpinBox::CosmosDoubleSpinBox(QWidget *parent) :
    QDoubleSpinBox(parent)
{
    cinfo = nullptr;

    entry = nullptr;
    this->setDecimals(25);
    connect(this, SIGNAL (editingFinished()), this, SLOT (cosmosEditingFinished()));
}

void CosmosDoubleSpinBox::getCinfoValue()
{
    if (cinfo != nullptr && entry != nullptr)
    {
            switch ((entry->type))
            {
            case JSON_TYPE_UINT8:
                setRange(0, UCHAR_MAX);
                break;
            case JSON_TYPE_UINT16:
                setRange(0, USHRT_MAX);
                break;
            case JSON_TYPE_UINT32:
                setRange(0, ULONG_MAX);
                break;
            case JSON_TYPE_INT8:
                setRange(CHAR_MIN, CHAR_MAX);
                break;
            case JSON_TYPE_INT16:
                setRange(SHRT_MIN, SHRT_MAX);
                break;
            case JSON_TYPE_INT32:
            default:
                setRange(LONG_MIN, LONG_MAX);
                break;
            }
        cvalue = json_get_double(*entry, cinfo);
        setValue(cvalue);
    }
}

void CosmosDoubleSpinBox::setCinfoValue()
{
    if (cinfo != nullptr && entry != nullptr && cvalue != value())
    {
        cvalue = value();
        json_set_number(cvalue, *entry, cinfo);
    }
}

void CosmosDoubleSpinBox::setCinfoPtr(cosmosstruc* newcinfo)
{
    if (cinfo != newcinfo)
    {
        cinfo = newcinfo;
        if (cinfo != nullptr && !name.empty())
        {
            entry = json_entry_of(name, cinfo);
        }

    }

    getCinfoValue();
}

void CosmosDoubleSpinBox::setCosmosName(std::string newname)
{
    if (name != newname)
    {
        name = newname;
        if (cinfo != nullptr)
        {
            entry = json_entry_of(name, cinfo);
        }
        getCinfoValue();
    }
}

void CosmosDoubleSpinBox::cosmosEditingFinished()
{
    setCinfoValue();
}

QString CosmosDoubleSpinBox::textFromValue(double value) const
{
    if (entry != nullptr)
    {
        json_set_number((double)value, *entry, cinfo);
    }
    return QString::number(value);
}

double CosmosDoubleSpinBox::valueFromText(const QString &text) const
{
    bool ok;
    const char *ptr;

    QByteArray array = text.toLocal8Bit();
    ptr = array.data();
    if (ptr != nullptr && entry != nullptr)
    {
        json_parse_value(ptr, *entry, cinfo);
    }

    return text.toDouble(&ok);
}

jsonentry* CosmosDoubleSpinBox::getCosmosEntry()
{
    return entry;
}
