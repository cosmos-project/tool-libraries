/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmospayloads.h"

CosmosPayloads::CosmosPayloads(QWidget *parent) :
    QTabWidget(parent)
{
    cinfo = nullptr;
    
}

void CosmosPayloads::setCinfoPtr(cosmosstruc* newcinfo)
{
    if (cinfo != newcinfo)
    {
        cinfo = newcinfo;
        // Completely new node?
        if (cinfo != nullptr && cnode != &cinfo->node)
        {
            cnode = &cinfo->node;

            // Clear out old widget tabs if necessary
            clear();

            dtype = (uint16_t)DeviceType::PLOAD;
            for (size_t i=0; i<cinfo->devspec.pload_cnt; ++i)
            {
                ploadstruc *pload = cinfo->devspec.pload[i];
                tabstruc ttab;
                ttab.w_me = new QWidget;
                QGridLayout *tlayout = new QGridLayout;
                ttab.w_me->setLayout(tlayout);

                ttab.cidx = pload->cidx;
                ttab.didx = i;

                QLabel *tlabel = new QLabel("State:", ttab.w_me);
                tlayout->addWidget(tlabel, 0, 0);
                ttab.b_on = new QRadioButton("On", ttab.w_me);
                ttab.b_on->setCheckable(true);
                tlayout->addWidget(ttab.b_on, 0, 1);
                ttab.b_off = new QRadioButton("Off", ttab.w_me);
                ttab.b_off->setCheckable(true);
                tlayout->addWidget(ttab.b_off, 0, 2);

                tlabel = new QLabel("Power:", ttab.w_me);
                tlayout->addWidget(tlabel, 1, 0);
                ttab.l_power = new QLabel(ttab.w_me);
                tlayout->addWidget(ttab.l_power, 1, 1);

                tlabel = new QLabel("Data Rate:", ttab.w_me);
                tlayout->addWidget(tlabel, 2, 0);
                ttab.l_drate = new QLabel(ttab.w_me);
                tlayout->addWidget(ttab.l_drate, 2, 1);

                tlabel = new QLabel("Temperature:", ttab.w_me);
                tlayout->addWidget(tlabel, 3, 0);
                ttab.l_temp = new QLabel(ttab.w_me);
                tlayout->addWidget(ttab.l_temp, 3, 1);

                tlabel = new QLabel("Condition:", ttab.w_me);
                tlayout->addWidget(tlabel, 4, 0);
                ttab.l_condition = new QLineEdit(ttab.w_me);
                tlayout->addWidget(ttab.l_condition, 4, 1);
                ttab.b_add_condition = new QPushButton("Add", ttab.w_me);
                tlayout->addWidget(ttab.b_add_condition, 4, 2);
                connect(ttab.b_add_condition, &QPushButton::pressed, [=]() { buttonAddCondition(i); });
                ttab.conditions.clear();

                tabstrucs.push_back(ttab);
                addTab(ttab.w_me, QString::fromStdString(cinfo->pieces[pload->pidx].name));
            }
        }
    }

    updateCinfoValue();
}

void CosmosPayloads::updateCinfoValue()
{
    if (cinfo == nullptr)
    {
        return;
    }


    // Update all tabs
    for (tabstruc& ttab : tabstrucs)
    {
        switch (cinfo->device[ttab.cidx].all.flag & DEVICE_FLAG_ON)
        {
        case 0:
            {
                ttab.b_off->setChecked(true);
                ttab.b_on->setChecked(false);
            }
            break;
        default:
            {
                ttab.b_off->setChecked(false);
                ttab.b_on->setChecked(true);
            }
            break;
        }
        ttab.l_power->setText(QString("%1").arg(cinfo->device[ttab.cidx].all.power, 0, 'g', 10));
        ttab.l_drate->setText(QString("%1").arg(cinfo->device[ttab.cidx].all.drate, 0, 'g', 10));
        ttab.l_temp->setText(QString("%1").arg(cinfo->device[ttab.cidx].all.temp, 0, 'g', 10));
        for (size_t i=0; i<ttab.conditions.size(); ++i)
        {
            ttab.conditions[i].l_condition->setText(QString::fromStdString(ttab.conditions[i].condition));
        }
    }


    update();
}

void CosmosPayloads::buttonAddCondition(int index)
{
    size_t cindex;
    for (cindex = 0; cindex < tabstrucs[index].conditions.size(); ++cindex)
    {
        if (tabstrucs[index].conditions[cindex].condition.empty())
            break;
    }

    string condition = "(" + tabstrucs[index].l_condition->text().toStdString() + ")";
    if (cindex == tabstrucs[index].conditions.size())
    {
        conditionstruc tcondition;
        tcondition.condition = condition;
        QLabel *tlabel = new QLabel(QString::fromStdString(condition), tabstrucs[index].w_me);
        ((QGridLayout *)(tabstrucs[index].w_me->layout()))->addWidget(tlabel, tabstrucs[index].conditions.size()+5, 0);
        tcondition.l_condition = tlabel;
        QPushButton *tbutton = new QPushButton("Del", tabstrucs[index].w_me);
        ((QGridLayout *)(tabstrucs[index].w_me->layout()))->addWidget(tbutton, tabstrucs[index].conditions.size()+5, 1);
        connect(tbutton, &QPushButton::pressed, [&, index, cindex]() { buttonDelCondition(index, cindex); });
        tcondition.b_del_condition = tbutton;
        tabstrucs[index].conditions.push_back(tcondition);
    }
    else
    {
        tabstrucs[index].conditions[cindex].condition = condition;
        tabstrucs[index].conditions[cindex].l_condition->setText(QString::fromStdString(condition));
    }
}

void CosmosPayloads::buttonDelCondition(int index, int cindex)
{
    tabstrucs[index].conditions[cindex].condition = "";
    tabstrucs[index].conditions[cindex].l_condition->setText("");
}
