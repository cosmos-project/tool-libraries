/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmosmap.h"

CosmosMap::CosmosMap(QWidget *parent) :
    QLabel(parent)
{
    cinfostack.stack = nullptr;
    cinfo = nullptr;
    
}

void CosmosMap::setImage(std::string newname, double upperleftlon, double upperleftlat, double lowerrightlon, double lowerrightlat)
{
    if (imagename != newname && pixmap.load(QString::fromStdString(get_cosmosresources()+"/"+newname)))
    {
        imagename = newname;
        //        pixmap.convertFromImage(image);
        setPixmap(pixmap);
        setScaledContents(true);
        width = pixmap.width();
        height = pixmap.height();
        ullon = upperleftlon;
        lrlon = lowerrightlon;
        dxdlon = width / (lrlon - ullon);
        ullat = upperleftlat;
        lrlat = lowerrightlat;
        dydlat = height / (lrlat - ullat);
        updateCinfoValues();
    }
}

void CosmosMap::setCinfoStackPtr(cosmosstruc *newcinfo, cosmosinfostack newcinfostack, size_t newcinfoiter)
{
    cinfoiter = newcinfoiter;
    if (cinfo != newcinfo)
    {
        cinfo = newcinfo;
        cinfostack.stack = nullptr;
    }
    if (cinfostack.stack != newcinfostack.stack || cinfostack.timestamp != newcinfostack.timestamp)
    {
        cinfostack = newcinfostack;
        cinfoiter = 0;
        updateCinfoValues();
    }
    else
    {
        if (cinfo != &(*cinfostack.stack)[cinfoiter])
        {
            cinfo = &(*cinfostack.stack)[cinfoiter];
        }
        updateCinfoValue(cinfoiter);
    }

}

void CosmosMap::updateCinfoValue(size_t iter)
{
    if (cinfo == nullptr || cinfostack.stack == nullptr || iter >= cstackY.size())
    {
        return;
    }

    // Set circle at new point
    cvalueX = cstackX[iter];
    cvalueY = cstackY[iter];


    update();
}

void CosmosMap::updateCinfoValues()
{
    if (cinfo == nullptr || cinfostack.stack == nullptr)
    {
        return;
    }

    // Get new points
    if (cinfo != nullptr && cinfostack.stack != nullptr && imagename.size())
    {
        cstackY.clear();
        cstackX.clear();
        for (size_t i=0; i<(*cinfostack.stack).size(); ++i)
        {
            cinfo = &(*cinfostack.stack)[i];
            cvalueX = dxdlon * (cinfo->node.loc.pos.geod.s.lon - ullon);
            cstackX.push_back(cvalueX);
            cvalueY = dydlat * (cinfo->node.loc.pos.geod.s.lat - ullat);
            cstackY.push_back(cvalueY);
        }
        cvalueX = cstackX[0];
        cvalueY = cstackY[0];
        ptimestamp = ctimestamp;
        ctimestamp = currentmjd();
    }

    update();
}

void CosmosMap::paintEvent(QPaintEvent *event)
{
    bool update_flag = false;
    QLabel::paintEvent(event);

    // Get ready to draw
    QPainter painter;
    painter.begin(&pixmap);

    if (pvalueX != cvalueX || pvalueY != cvalueY)
    {
        if (pvalueX > 0 && pvalueY > 0)
        {
            // Erase old circle
            painter.setCompositionMode(QPainter::RasterOp_SourceXorDestination);
            QPen pen(Qt::yellow, width/500, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
            painter.setPen(pen);
            painter.drawEllipse(pvalueX-width/200, pvalueY-width/200, width/100, width/100);
        }

        if (cvalueX > 0 && cvalueY > 0)
        {
            // Draw new circle
            painter.setCompositionMode(QPainter::RasterOp_SourceXorDestination);
            QPen pen(Qt::yellow, width/500, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
            painter.setPen(pen);
            painter.drawEllipse(cvalueX-width/200, cvalueY-width/200, width/100, width/100);

            pvalueX = cvalueX;
            pvalueY = cvalueY;
            update_flag = true;
        }
    }


    if (ctimestamp > ptimestamp)
    {
        // Erase old line
        painter.setCompositionMode(QPainter::RasterOp_SourceXorDestination);
        QPen pen(Qt::red, width/200, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
//        pen.setWidthF(width/20.);
        painter.setPen(pen);
        for (size_t i=1; i<pstackY.size(); ++i)
        {
            if ((size_t)abs(pstackX[i-1]-pstackX[i]) < width/10)
            {
                painter.drawLine(pstackX[i-1], pstackY[i-1], pstackX[i], pstackY[i]);
            }
        }
        pstackX = cstackX;
        pstackY = cstackY;

        // Draw new line
        for (size_t i=1; i<cstackY.size(); ++i)
        {
            if ((size_t)abs(cstackX[i-1]-cstackX[i]) < width/10)
            {
                painter.drawLine(cstackX[i-1], cstackY[i-1], cstackX[i], cstackY[i]);
            }
        }
        ptimestamp = ctimestamp;
        update_flag = true;
    }

    // Clean up
    painter.end();
    if (update_flag)
    {
        setPixmap(pixmap);
    }
}
