/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#ifndef COSMOSSLIDER_H
#define COSMOSSLIDER_H


#include "cosmoswidget.h"
#include <QSlider>

class CosmosSlider : public QSlider
{
    Q_OBJECT

public:
    explicit CosmosSlider(QWidget *parent = 0);

    void getCinfoValue();
    void setCinfoValue();
    void setCinfoPtr(cosmosstruc *newcinfo);
    void setCosmosName(std::string newname);
    std::string getCosmosName() const { return name; }
    jsonentry* getCosmosEntry();

private:
    cosmosstruc *cinfo;
    
    jsonentry *entry = 0;
    std::string name;

    int cvalue;
    int digits;
};

#endif // COSMOSSLIDER_H
