#ifndef COSMOSELEMENTS_H
#define COSMOSELEMENTS_H

#include "cosmoswidget.h"
#include "support/jsondef.h"
#include "support/convertlib.h"
#include "cosmosdoublespinbox.h"
#include <QPushButton>
#include <QLabel>
#include <QTabWidget>
#include <QString>
#include <QFormLayout>
#include "agent/agentclass.h"
#include "physics/physicslib.h"

class CosmosElements: public QTabWidget
{
    Q_OBJECT
public:
    explicit CosmosElements(QWidget *parent = 0);

//    void setCinfoPtr(cosmosstruc* newcinfo);
//    cosmosstruc *getCinfoPtr() const { return cinfo; }
//    
//    void setJsonType(size_t type);
    void addTabs();
  // void setAgent(Agent *agent) {cagent = agent;}
    void setkep(kepstruc *mkep) {kep = mkep;}
    void seteci(cartpos *meci) {eci = meci;}


    struct tabstruc
    {
        QWidget *w_me;
        QString s_name;
        QLabel *l_date;
        QLabel *l_age;
        QLabel *l_sval[3];
        QLabel *l_vval[3];
        QLabel *l_aval[3];
        QLabel *l_sname[3];
        QLabel *l_vname[3];
        QLabel *l_aname[3];
        size_t jidx;
    };

    size_t getTabstrucCount() const { return tabstrucs.size(); }
    tabstruc *getTabstruc(size_t index) { return (tabstrucs.size() > index ? &tabstrucs[index] : nullptr); }

signals:
    void updatedOrbit();
    void exportKep();
    void exportEci();


public slots:

private slots:
//    void buttonUpdateKepOrbit();
//    void buttonUpdateEciOrbit();
    void updateOrbits() {emit updatedOrbit();}
    void exportingKepData() {emit exportKep();}
    void exportingEciData() {emit exportEci();}
    void updateKepSpinboxes();
    void updateEciSpinboxes();

private:
    CosmosDoubleSpinBox *eccentricity;
    CosmosDoubleSpinBox *altitude;
    CosmosDoubleSpinBox *inclination;
    CosmosDoubleSpinBox *raan;
    CosmosDoubleSpinBox *ap;
    CosmosDoubleSpinBox *xPos;
    CosmosDoubleSpinBox *yPos;
    CosmosDoubleSpinBox *zPos;
    CosmosDoubleSpinBox *xVel;
    CosmosDoubleSpinBox *yVel;
    CosmosDoubleSpinBox *zVel;

    cosmosstruc *cinfo;
    
  //  Agent *cagent;
    kepstruc *kep;
    cartpos *eci;

    vector <tabstruc> tabstrucs;
    size_t jtype;
    size_t rowcnt;
};

#endif // COSMOSELEMENTS_H
