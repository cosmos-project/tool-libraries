/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#ifndef COSMOSMAP_H
#define COSMOSMAP_H

#include "cosmoswidget.h"
#include <QLabel>
#include <QImage>
#include <QPixmap>
#include <QPainter>


class CosmosMap : public QLabel
{
    Q_OBJECT

public:
    explicit CosmosMap(QWidget *parent = 0);
    void paintEvent(QPaintEvent *event);

    void setCinfoStackPtr(cosmosstruc *newcinfo, cosmosinfostack newcinfostack, size_t newcinfoiter);
    void updateCinfoValue(size_t iter);
    void updateCinfoValues();

    cosmosinfostack getCinfoStackValue() const { return cinfostack; }

    void setImage(std::string newname, double upperleftlon=-DPI, double upperleftlat=DPI2, double lowerrightlon=DPI, double lowerrightlat=-DPI2);

private:
    size_t width = 0;
    size_t height = 0;
    QPixmap pixmap;

    cosmosstruc *cinfo;
    
    cosmosinfostack cinfostack;
    int32_t cinfoiter = -1;

    std::string imagename;
    double ullon;
    double ullat;
    double lrlon;
    double lrlat;
    double dxdlon = 0.;
    double dydlat = 0.;
    std::vector<int> cstackX;
    std::vector<int> cstackY;
    std::vector<int> pstackX;
    std::vector<int> pstackY;
    int cvalueX = -1;
    int cvalueY = -1;
    int pvalueX = -1;
    int pvalueY = -1;
    double ctimestamp;
    double ptimestamp;

};

#endif // COSMOSMAP_H
