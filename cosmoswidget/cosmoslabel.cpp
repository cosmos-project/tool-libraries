/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmoslabel.h"

CosmosLabel::CosmosLabel(QWidget *parent) :
    QLabel(parent)
{
    cinfo = nullptr;
    
    entry = nullptr;
}

void CosmosLabel::getCinfoValue()
{
    if (cinfo != nullptr && entry != nullptr)
    {
        switch ((entry->type))
        {
        case JSON_TYPE_UINT8:
            digits = 3;
            break;
        case JSON_TYPE_UINT16:
            digits = 5;
            break;
        case JSON_TYPE_UINT32:
            digits = 10;
            break;
        case JSON_TYPE_INT8:
            digits = 4;
            break;
        case JSON_TYPE_INT16:
            digits = 6;
            break;
        default:
            digits = 11;
            break;
        }
        cvalue = json_get_string(*entry, cinfo);
        setText(QString::fromStdString(cvalue));
    }
}

void CosmosLabel::setCinfoValue()
{
    std::string tvalue;
    if (cinfo != nullptr && entry != nullptr && cvalue !=  (tvalue = text().toStdString()))
    {
        cvalue = tvalue;
        json_set_string(cvalue, *entry, cinfo);
    }
}

void CosmosLabel::setCinfoPtr(cosmosstruc* newcinfo)
{
    if (cinfo != newcinfo)
    {
        cinfo = newcinfo;
        if (cinfo != nullptr && !name.empty())
        {
            entry = json_entry_of(name, cinfo);
        }
        
    }

    getCinfoValue();
}

void CosmosLabel::setCosmosName(std::string newname)
{
    if (name != newname)
    {
        name = newname;
        if (cinfo != nullptr)
        {
            entry = json_entry_of(name, cinfo);
        }
        getCinfoValue();
    }
}

void CosmosLabel::cosmosEditingFinished()
{
    setCinfoValue();
}

QString CosmosLabel::textFromValue(int value) const
{
    if (entry != nullptr)
    {
        json_set_number((double)value, *entry, cinfo);
    }
    return QString::number(value);
}

int CosmosLabel::valueFromText(const QString &text) const
{
    bool ok;
    const char *ptr;

    QByteArray array = text.toLocal8Bit();
    ptr = array.data();
    if (ptr != nullptr && entry != nullptr)
    {
        json_parse_value(ptr, *entry, cinfo);
    }

    return text.toInt(&ok, digits);
}

jsonentry* CosmosLabel::getCosmosEntry()
{
    return entry;
}
