/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

#include "cosmoswidgethandler.h"

CosmosWidgetHandler::CosmosWidgetHandler()
{
    cinfo = nullptr;
    
    cinfostack.stack = nullptr;
    cdataindex = -1;
}

size_t CosmosWidgetHandler::newWidget(widgetType type, std::string name, std::string label, bool show_list)
{
    size_t windex = -1;
    widgetHandle handle;
    handle.widget = nullptr;
    handle.layout = nullptr;
    handle.type = type;
    handle.name_value = name;
    handle.label_value = label;

    switch (type)
    {
    case widgetType::ELEMENTS:
        handle.widget = new CosmosElements;
        break;
    case widgetType::LABEL:
        handle.widget = new CosmosLabel;
        break;
    case widgetType::SPINBOX:
        handle.widget = new CosmosSpinBox;
        break;
    case widgetType::DOUBLESPINBOX:
        handle.widget = new CosmosDoubleSpinBox;
        break;
    case widgetType::PLOT:
        handle.widget = new CosmosPlot;
        break;
    case widgetType::PUSHBUTTON:
        handle.widget = new CosmosPushButton;
        break;
    case widgetType::SLIDER:
        handle.widget = new CosmosSlider;
        break;
    case widgetType::MAP:
        handle.widget = new CosmosMap;
        ((CosmosMap *)(handle.widget))->setImage(name);
        break;
    case widgetType::PAYLOADS:
        handle.widget = new CosmosPayloads;
        break;
    case widgetType::DEVICE:
        handle.widget = new CosmosDevice;
        break;
    case widgetType::POSITION:
        handle.widget = new CosmosPosition;
        break;
    }

    if (handle.widget != nullptr)
    {
        windex = widgetStack.size();
        handle.layout = new QVBoxLayout;
        QHBoxLayout *hlayout1 = new QHBoxLayout;
        handle.label = new QLabel(label.c_str());
        hlayout1->addWidget(handle.label);
        if (show_list)
        {
            handle.combo = new QComboBox;
            handle.combo->connect(handle.combo, static_cast<void(QComboBox::*)(int)>(&QComboBox::activated), [&, windex](int nindex) { setCosmosName(nindex, windex); });
            hlayout1->addWidget(handle.combo);
        }
        handle.layout->addLayout(hlayout1);
        QHBoxLayout *hlayout2 = new QHBoxLayout;
        hlayout2->addWidget(handle.widget);
        handle.layout->addLayout(hlayout2);
        widgetStack.push_back(handle);
        setCosmosName(handle.name_value, windex);
    }

    return windex;
}

void CosmosWidgetHandler::setCdataIndex(size_t newcinfoindex)
{
    if (cinfo != nullptr && cinfostack.stack != nullptr && cdataindex != newcinfoindex && newcinfoindex < cinfostack.stack->size())
    {
        cinfo = &(*cinfostack.stack)[newcinfoindex];
        cdataindex = newcinfoindex;
        updateCdata();
    }
}

void CosmosWidgetHandler::setCinfoStack(cosmosinfostack newcinfostack)
{
    if (newcinfostack.stack != nullptr)
    {
        name_list.clear();
        for (std::vector<jsonentry> jrow : (*newcinfostack.stack)[0].jmap)
        {
            for (jsonentry jentry : jrow)
            {
                if (jentry.name[0] == 'n' || jentry.name[0] == 'e' || jentry.name[0] == 'd' || jentry.name[0] == 'c' || jentry.name[0] == 't')
                {
                    name_list.push_back(jentry.name);
                    for (size_t i=name_list.size()-1; i>0; --i)
                    {
                        if (name_list[i] < name_list[i-1])
                        {
                            std::string tname = name_list[i-1];
                            name_list[i-1] = name_list[i];
                            name_list[i] = tname;
                        }
                    }
                }
            }
        }

        for (size_t i=0; i<widgetStack.size(); ++i)
        {
            if (widgetStack[i].combo)
            {
                widgetStack[i].combo->clear();
            }
        }
        cinfostack.stack = nullptr;
    }

    if (cinfostack.stack != newcinfostack.stack || cinfostack.timestamp != newcinfostack.timestamp)
    {
        cinfostack = newcinfostack;
        cdataindex = 0;
        updateCdata();
    }
}

void CosmosWidgetHandler::updateCdata()
{
    if (cinfo == nullptr || cinfostack.stack == nullptr)
    {
        return;
    }

    if (cdataindex >= (*cinfostack.stack).size())
    {
        return;
    }

    cinfo = &(*cinfostack.stack)[cdataindex];

    for (size_t i=0; i<widgetStack.size(); ++i)
    {
        if (widgetStack[i].combo && (size_t)widgetStack[i].combo->count() != name_list.size())
        {
            for ( std::string name : name_list)
            {
                widgetStack[i].combo->addItem(QString::fromStdString(name));
            }
        }

        switch (widgetStack[i].type)
        {
        case widgetType::LABEL:
            ((CosmosLabel *)(widgetStack[i].widget))->setCinfoPtr(cinfo);
            break;
        case widgetType::SPINBOX:
            ((CosmosSpinBox *)(widgetStack[i].widget))->setCinfoPtr(cinfo);
            break;
        case widgetType::DOUBLESPINBOX:
            ((CosmosDoubleSpinBox *)(widgetStack[i].widget))->setCinfoPtr(cinfo);
            break;
        case widgetType::PLOT:
            ((CosmosPlot *)(widgetStack[i].widget))->setCinfoStackPtr(cinfo, cinfostack, cdataindex);
            break;
        case widgetType::PUSHBUTTON:
            ((CosmosPushButton *)(widgetStack[i].widget))->setCinfoPtr(cinfo);
            break;
        case widgetType::SLIDER:
            ((CosmosSlider *)(widgetStack[i].widget))->setCinfoPtr(cinfo);
            break;
        case widgetType::ELEMENTS:

            break;
        case widgetType::MAP:
            ((CosmosMap *)(widgetStack[i].widget))->setCinfoStackPtr(cinfo, cinfostack, cdataindex);
            break;
        case widgetType::PAYLOADS:
            ((CosmosPayloads *)(widgetStack[i].widget))->setCinfoPtr(cinfo);
            break;
        case widgetType::DEVICE:
            ((CosmosDevice *)(widgetStack[i].widget))->setCinfoPtr(cinfo);
            break;
        case widgetType::POSITION:
            ((CosmosPosition *)(widgetStack[i].widget))->setCinfoPtr(cinfo);
            break;
        }
    }
}

void CosmosWidgetHandler::setCosmosName(size_t nindex, size_t windex)
{
    setCosmosName(name_list[nindex], windex);
}

void CosmosWidgetHandler::setCosmosName(string name, size_t windex)
{
    jsonentry *entry = nullptr;
    switch (widgetStack[windex].type)
    {

    case widgetType::LABEL:
        ((CosmosLabel *)(widgetStack[windex].widget))->setCosmosName(name);
        entry = ((CosmosLabel *)(widgetStack[windex].widget))->getCosmosEntry();
        break;
    case widgetType::SPINBOX:
        ((CosmosSpinBox *)(widgetStack[windex].widget))->setCosmosName(name);
        entry = ((CosmosSpinBox *)(widgetStack[windex].widget))->getCosmosEntry();
        break;
    case widgetType::DOUBLESPINBOX:
        ((CosmosDoubleSpinBox *)(widgetStack[windex].widget))->setCosmosName(name);
        entry = ((CosmosDoubleSpinBox *)(widgetStack[windex].widget))->getCosmosEntry();
        break;
    case widgetType::PLOT:
        ((CosmosPlot *)(widgetStack[windex].widget))->setCosmosName(name);
        entry = ((CosmosPlot *)(widgetStack[windex].widget))->getCosmosEntry();
        break;
    case widgetType::PUSHBUTTON:
        ((CosmosPushButton *)(widgetStack[windex].widget))->setCosmosName(name);
        entry = ((CosmosPushButton *)(widgetStack[windex].widget))->getCosmosEntry();
        break;
    case widgetType::SLIDER:
        ((CosmosSlider *)(widgetStack[windex].widget))->setCosmosName(name);
        entry = ((CosmosSlider *)(widgetStack[windex].widget))->getCosmosEntry();
        break;
    case widgetType::ELEMENTS:
    case widgetType::MAP:
    case widgetType::PAYLOADS:
    case widgetType::DEVICE:
    case widgetType::POSITION:
        break;
    }

    if (widgetStack[windex].label_value != "")
    {
        widgetStack[windex].label->setText(QString::fromStdString(widgetStack[windex].label_value));
    }
    else if (entry)
    {
        string label_value = entry->name + "(" + cinfo->unit[entry->unit_index][0].name + ")";
        widgetStack[windex].label->setText(QString::fromStdString(label_value));
    }
}

//void CosmosWidgetHandler::setVisibility(size_t index, bool vis)
//{

//    if (index < widgetStack.size())
//    {
//        widgetStack[index].widget->setVisible(vis);
//        if (widgetStack[index].combo)
//        {
//            widgetStack[index].combo->setVisible(vis);
//        }
//        if (widgetStack[index].label)
//        {
//            widgetStack[index].label->setVisible(vis);
//        }
//    }
//}

std::string CosmosWidgetHandler::getName(size_t index)
{
    if (index < widgetStack.size())
    {
        switch (widgetStack[index].type)
        {
        case widgetType::LABEL:
            return ((CosmosLabel *)(widgetStack[index].widget))->getCosmosName();
            break;
        case widgetType::SPINBOX:
            return ((CosmosSpinBox *)(widgetStack[index].widget))->getCosmosName();
            break;
        case widgetType::DOUBLESPINBOX:
            return ((CosmosDoubleSpinBox *)(widgetStack[index].widget))->getCosmosName();
            break;
        case widgetType::PLOT:
            return ((CosmosPlot *)(widgetStack[index].widget))->getCosmosName();
            break;
        case widgetType::PUSHBUTTON:
            return ((CosmosPushButton *)(widgetStack[index].widget))->getCosmosName();
            break;
        case widgetType::SLIDER:
            return ((CosmosSlider *)(widgetStack[index].widget))->getCosmosName();
            break;
        case widgetType::ELEMENTS:
        case widgetType::MAP:
        case widgetType::PAYLOADS:
        case widgetType::DEVICE:
        case widgetType::POSITION:
            return "";
            break;
        }
    }
    return "";
}

size_t CosmosWidgetHandler::getIndex(std::string name)
{
    size_t i;
    for (i=0; i<widgetStack.size(); i++) //does this exit after it finds the target?
    {
        switch (widgetStack[i].type)
        {
        case widgetType::LABEL:
            if (name == ((CosmosLabel *)(widgetStack[i].widget))->getCosmosName())
            {
                return i;
            }
            break;
        case widgetType::SPINBOX:
            if (name == ((CosmosSpinBox *)(widgetStack[i].widget))->getCosmosName())
            {
                return i;
            }
            break;
        case widgetType::DOUBLESPINBOX:
            if (name == ((CosmosDoubleSpinBox *)(widgetStack[i].widget))->getCosmosName())
            {
                return i;
            }
            break;
        case widgetType::PLOT:
            if (name == ((CosmosPlot *)(widgetStack[i].widget))->getCosmosName())
            {
                return i;
            }
            break;
        case widgetType::PUSHBUTTON:
            if (name == ((CosmosPushButton *)(widgetStack[i].widget))->getCosmosName())
            {
                return i;
            }
            break;
        case widgetType::SLIDER:
            if (name == ((CosmosSlider *)(widgetStack[i].widget))->getCosmosName())
            {
                return i;
            }
            break;
        case widgetType::ELEMENTS:
        case widgetType::MAP:
        case widgetType::PAYLOADS:
        case widgetType::DEVICE:
        case widgetType::POSITION:
                    //will this need a return? if so, how will the arguement work?
            break;
        }
    }
    return i;    //will return a value > stack size. should properly break following functions.
}

//does not work for ELEMENTS, MAP, PAYLOADS, DEVICE, POSTION. (as of 8/25/2017)
QWidget * CosmosWidgetHandler::getWidget(std::string name)
{
    size_t index = getIndex(name);
    return getWidget(index);
}

QWidget * CosmosWidgetHandler::getWidget(size_t index)
{
    if (index < widgetStack.size())
    {
        return widgetStack[index].widget;
    }
    return nullptr;
}

QComboBox * CosmosWidgetHandler::getWidgetCombo(std::string name)
{
    size_t index = getIndex(name);
    return getWidgetCombo(index);
}

QComboBox * CosmosWidgetHandler::getWidgetCombo(size_t index)
{
    if (index < widgetStack.size())
    {
        if (widgetStack[index].combo)
        {
            return widgetStack[index].combo;
        }
    }
    return nullptr;
}

QLabel * CosmosWidgetHandler::getWidgetLabel(size_t index)
{
    if (index < widgetStack.size())
    {
        if (widgetStack[index].label)
        {
            return widgetStack[index].label;
        }
    }
    return nullptr;
}

QVBoxLayout *CosmosWidgetHandler::getWidgetLayout(std::string name)
{
    size_t index = getIndex(name);
    return getWidgetLayout(index);
}

QVBoxLayout * CosmosWidgetHandler::getWidgetLayout(size_t index)
{
    if (index < widgetStack.size())
    {
        return widgetStack[index].layout;
    }
    return nullptr;
}

size_t CosmosWidgetHandler::size()
{
    return widgetStack.size();
}

CosmosWidgetHandler::widgetType CosmosWidgetHandler::getType(size_t index)
{
    return widgetStack[index].type;
}
