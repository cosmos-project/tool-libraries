#include "miscqmlsupport.h"

MiscQmlSupport::MiscQmlSupport(QObject *parent) :
    QObject(parent)
{
}

QColor MiscQmlSupport::alterAlpha(QColor baseColor, double newAlpha) {
    baseColor.setAlpha(newAlpha*255);
    return baseColor;
}

QObject* MiscQmlSupport::getQObjectParent(QObject* obj) {
    return obj->parent();
}

void MiscQmlSupport::changeQObjectParent(QObject* obj, QObject* prnt) {
    obj->setParent(prnt);
}
