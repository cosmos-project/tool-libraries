#include "sharedobjectlist.h"

#include <QDebug>

SharedObjectList::SharedObjectList(QObject *parent) :
    QObject(parent)
{
    defaultParent = NULL;
}

//Only my very favorite QList functions:

QObject* SharedObjectList::at(int i) {
    if (i>=0 && i<m_list.length()) return m_list.at(i);
    else return NULL;
}

void SharedObjectList::append(QObject* obj) {
    if (defaultParent!=NULL&&obj->parent()==NULL) obj->setParent(defaultParent);
    m_list.append(obj);
    emit itemAdded(obj);
    emit listChanged(this); //when the list is lengthened, it is best to emit lengthChanged last.
    emit lengthChanged(m_list.length());
}

void SharedObjectList::prepend(QObject* obj) {
    if (defaultParent!=NULL&&obj->parent()==NULL) obj->setParent(defaultParent);
    m_list.prepend(obj);
    emit itemAdded(obj);
    emit listChanged(this);
    emit lengthChanged(m_list.length());
}

void SharedObjectList::insert(int i, QObject* obj) {
    if (defaultParent!=NULL&&obj->parent()==NULL) obj->setParent(defaultParent);
    m_list.insert(i, obj);
    emit itemAdded(obj);
    emit listChanged(this);
    emit lengthChanged(m_list.length());
}

void SharedObjectList::replace(int i, QObject* obj) {
    if (defaultParent!=NULL&&obj->parent()==NULL) obj->setParent(defaultParent);
    m_list.replace(i, obj); //(replace doesn't change the length)
    emit itemAdded(obj);
    emit itemRemoved();
    emit listChanged(this);
}

void SharedObjectList::removeAt(int i) {
    m_list.removeAt(i);
    emit lengthChanged(m_list.length());
    emit itemRemoved();
    emit listChanged(this); //when the list is shortened, it is best to emit lengthChanged first.
}

void SharedObjectList::removeFirst() {
    m_list.removeFirst();
    emit lengthChanged(m_list.length());
    emit itemRemoved();
    emit listChanged(this);
}

void SharedObjectList::removeLast() {
    m_list.removeLast();
    emit lengthChanged(m_list.length());
    emit itemRemoved();
    emit listChanged(this);
}

void SharedObjectList::clear() {
    m_list.clear();
    emit lengthChanged(0);
    emit listChanged(this);
}

int SharedObjectList::length() const {
    return m_list.length();
}

QObjectList* SharedObjectList::list() {
    return &m_list;
}
