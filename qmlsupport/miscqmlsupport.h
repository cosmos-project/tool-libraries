#ifndef MISCQMLSUPPORT_H
#define MISCQMLSUPPORT_H

#include <QObject>
#include <QColor>

class MiscQmlSupport : public QObject
{
    Q_OBJECT
public:
    explicit MiscQmlSupport(QObject *parent = 0);

    Q_INVOKABLE static QColor alterAlpha(QColor baseColor, double newAlpha);
    Q_INVOKABLE static QObject* getQObjectParent(QObject* obj);
    Q_INVOKABLE static void changeQObjectParent(QObject* obj, QObject* prnt);

signals:

public slots:

};

#endif // MISCQMLSUPPORT_H
