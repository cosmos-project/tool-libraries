#ifndef SHAREDOBJECTLIST_H
#define SHAREDOBJECTLIST_H

#include <QObject>

class SharedObjectList : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int length READ length NOTIFY lengthChanged)

public:
    explicit SharedObjectList(QObject *parent = 0);

    Q_INVOKABLE QObject* at(int i);
    Q_INVOKABLE void append(QObject* obj);
    Q_INVOKABLE void prepend(QObject* obj);
    Q_INVOKABLE void insert(int i, QObject* obj);
    Q_INVOKABLE void replace(int i, QObject* obj);
    Q_INVOKABLE void removeAt(int i);
    Q_INVOKABLE void removeFirst();
    Q_INVOKABLE void removeLast();
    Q_INVOKABLE void clear();

    int length() const;

    QObjectList* list();

    QObject* defaultParent; //all objects with no parent are parented to this unless it's set to null (default)

private:
    QObjectList m_list;

signals:
    void lengthChanged(int length);

    void listChanged(QObject* lst);
    void itemAdded(QObject* newItem);
    void itemRemoved();


public slots:

};

#endif // SHAREDOBJECTLIST_H
